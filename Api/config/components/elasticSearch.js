
'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
    ELASTIC_SEARCH_URL: joi.string().required()
}).unknown()
    .required()

const envVars = joi.attempt(process.env, envVarsSchema)

const config = {
    elasticSearch: {
        url: envVars.ELASTIC_SEARCH_URL
    }
}

module.exports = config;