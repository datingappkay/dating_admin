'use strict';


'use strict'

const joi = require('joi')
const mailgunx = require('mailgun.js');

const config = require('./config') 

const mg = mailgunx.client({key:config.MAILGUN_AUTH_KEY });


const envVarsSchema = joi.object({
    to: joi.string().required(),
    subject: joi.string().required(),
    subject: joi.string().required(),
    from: joi.string().defaut(config.MAILGUN_FROM_NAME)
}).unknown();


function sendMail(params, callback) {
    const mailGunConf = joi.attempt(params, envVarsSchema)
    mg.messages.create(config.MAILGUN_DOMAIN_NAME, mailGunConf).then(msg => {
        cb(null, msg);
    }).catch(err => {
        
        return cb(err)
    });
}

module.exports = { sendMail };


