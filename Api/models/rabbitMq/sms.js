const path = require('path');
const ipc = require('node-ipc');
const fork = require('child_process').fork;
let logger = require('winston');
let amqp = require('./rabbitMq');
const cluster = require('cluster');
var cpus = {};

ipc.config.id = 'rabbitmqserverSMS';
ipc.config.silent = true;


function InsertQueue(channel, queue, data, callback) {

    logger.info("step 2: inserting message into queue");

    if (channel) {
        channel.assertQueue(queue, { durable: false }, function (err, queueList) {

            let messageCount = queueList.messageCount;
            let consumerCount = queueList.consumerCount;

            logger.info("Message count", messageCount);
            logger.info("Consumer count", consumerCount);

            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
            logger.info("cluster.worker.id : ", cluster.worker.id)
            logger.info("cpus : ", cpus);
            if (!cpus[cluster.worker.id]) {
                logger.info("in IF");
                cpus[cluster.worker.id] = true;
                startIPCServerForRabbitMQWorker(data);
            } else {
                logger.info("in ELSE");
            }

        });
    }
    else {
        logger.error("channal not found...");
    }
}


function startIPCServerForRabbitMQWorker(data) {

    logger.info("step 3: forking chaild process");

    var file = path.join(__dirname, '../../worker/sms/worker.js');
    var child_process = fork(file);

    logger.info("cpus : ", cpus);
    logger.info("pid forked ........ ", child_process.pid);

    ipc.serve(() => {
        ipc.server.on('consumer.connected', (data, socket) => {
            logger.info('RabbitMQ consumer is connected and ready to execute', data);
        });

        ipc.server.on('message.consumed', (data, socket) => {
            logger.info('RabbitMQ message consumed callback', data);
            smsLog.Insert(data, (err, response) => {
            });//insert  log in database

        });

        ipc.server.on('consumer.exiting', (data, socket) => {
            logger.info('RabbitMQ message consumer exiting', data);
            ipc.server.stop();
        });

    });

    try {
        ipc.server.start();
    } catch (err) {
        logger.error(err);

    }

}


module.exports = { InsertQueue };