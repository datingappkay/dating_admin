'use strict'

const config = require('../../config/index');
const logger = require('winston');

var elasticsearch = require('elasticsearch');
var elasticClient;

var state = {
  connection: null,
}

exports.connect = function (done) {
  try {
    logger.info("elasticsearch 000");
    if (state.connection) return done()
    elasticClient = new elasticsearch.Client({
      host: config.elasticSearch.url,
      log: 'info'
    });

    state.connection = elasticClient;
    logger.info("elasticsearch connected on url : ", config.elasticSearch.url);

    done()

  } catch (e) {
    logger.info("elasticsearch connect exception ", e)
  }

}

exports.get = function () {
  return state.connection
}