'use strict'

const logger = require('winston');
let db = require('../mongodb');
const ObjectID = require('mongodb').ObjectID;

let tablename = 'userPrefrances';


function Select(data, callback) {
    db.get().collection(tablename)
        .find(data)
        .sort({ Priority: -1 })
        .toArray((err, result) => {
            return callback(err, result);
        });
}
function SortByPriority(data, callback) {
    db.get().collection(tablename)
        .find(data)
        .sort({ Priority: 1 })
        .toArray((err, result) => {
            return callback(err, result);
        });
}
function Reorder(_id, data, callback) {
    db.get().collection(tablename)
        .update({ Priority: _id }, { $set: { Priority: data } }, (err, result) => {
            // return callback(err, result);
        });
    db.get().collection(tablename)
        .update({ Priority: data }, { $set: { Priority: _id } }, (err, result) => {
            return callback(err, result);
        });
}
function SelectOne(data, callback) {
    db.get().collection(tablename)
        .findOne(data, ((err, result) => {
            return callback(err, result);
        }));
}


function SelectById(condition, requiredFeild, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .findOne(condition, requiredFeild, ((err, result) => {
            return callback(err, result);
        }));
}


function Insert(data, callback) {
    db.get().collection(tablename)
        .insert(data, (err, result) => {
            return callback(err, result);
        });
}


function Update(condition, data, callback) {
    db.get().collection(tablename)
        .update(condition, { $set: data }, (err, result) => {
            return callback(err, result);
        });
}


function UpdateById(_id, data, callback) {
    db.get().collection(tablename)
        .update({ _id: ObjectID(_id) }, { $set: data }, { upsert: true }, (err, result) => {
            return callback(err, result);
        });
}



function Delete(condition, callback) {
    db.get().collection(tablename)
        .remove(condition, (err, result) => {
            return callback(err, result);
        });
}


function Aggregate(condition, callback) {
    db.get().collection(tablename)
        .aggregate(condition, (err, result) => {
            return callback(err, result);
        });
}


module.exports = { Aggregate, Select, Reorder, SortByPriority, SelectOne, Insert, Update, SelectById, UpdateById, Delete };