'use strict';
const Joi = require("joi");
const logger = require('winston');
const blockedReports = require("../../../models/blockedReports");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");


const APIHandler = (req, res) => {

    let userId = req.params.userId;

    let condition = [
        { "$match": { "targetUserId": ObjectID(userId) } },
        {
            "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "blockedUserName" }
        }, { "$unwind": "$blockedUserName" },
        {
            "$project": {
                "firstName": "$blockedUserName.firstName", "gender": "$blockedUserName.gender", "registeredTimestamp": "$blockedUserName.registeredTimestamp", "total": 1, "timestamp": 1, "profileVideo": "blockedUserName.profileVideo", "city": "$blockedUserName.address.city", "country": "$blockedUserName.address.country",
                "contactNumber": "$blockedUserName.contactNumber", "email": "$blockedUserName.email", "profilePic": "$blockedUserName.profilePic", "profileVideo": "$blockedUserName.profileVideo", "dob": "$blockedUserName.dob", "height": "$blockedUserName.height", "blockedUserName.country": 1, "lastLogin": "$blockedUserName.lastLogin",
                "myPreferences":"$blockedUserName.myPreferences","otherImages":"$blockedUserName.otherImages"
            }
        },
        { "$sort": { "timestamp": -1 } }
    ];

    blockedReports.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {

            return res({ message: req.i18n.__('BlockUserById')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('BlockUserById')['204'] }).code(204);
        }
    });
};

const payloadValidator = Joi.object({
    userId: Joi.string().required().description('iserId'),

}).options({ allowUnknown: true });

const response = {
    status: {
        200: {
            message: Joi.any().default(local['BlockUserById']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['BlockUserById']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }