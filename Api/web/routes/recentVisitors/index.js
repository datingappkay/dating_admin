
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let GetByIdAPI = require('./GetById');
let mostviewed = require('./mostviewed');


module.exports = [
    {
        method: 'GET',
        path: '/recentVisitors',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'recentVisitors'],
            description: 'This API is used to login an user in the app.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response,

        }
    },
    {
        method: 'GET',
        path: '/recentVisitors/{_id}',
        handler: GetByIdAPI.APIHandler,
        config: {
            tags: ['api', 'recentVisitors'],
            description: 'This API is used to login an user in the app.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByIdAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response,

        }
    },
    {
        method: 'GET',
        path: '/mostviewed',
        handler: mostviewed.APIHandler,
        config: {
            tags: ['api', 'recentVisitors'],
            description: 'This API is used to get mostviewed.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];