'use strict';
const Joi = require("joi");
const logger = require('winston');
const recentVisitors = require("../../../models/recentVisitors");
const ObjectID = require('mongodb').ObjectID;
const local  = require("../../../locales");

const APIHandler = (req, res) => {


    let userId = req.params._id;
    let condition = [
        { "$match": { "targetUserId": ObjectID(userId) } },
        {
            "$group": {
                "_id": "$userId",
                "LastViewedOn": { "$last": "$createdTimestamp" },
                "TotalViews": { "$sum": 1 }
            }
        },
        { "$lookup": { "from": "userList", "localField": "_id", "foreignField": "_id", "as": "Data" } },
        { "$unwind": "$Data" },
        {
            "$project": {
                "LastViewedOn": 1,
                "TotalViews": 1,
                "ViewedBy": "$Data.firstName",
                "PhoneNo": "$Data.contactNumber",
                "Email": "$Data.email"
            }
        },
        { "$sort": { "LastViewedOn": -1 } }

    ]
    console.log("--?>>>>>>>>>>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX", JSON.stringify(condition));
    recentVisitors.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message:  req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
        result.forEach(element => {
            if (element.Email !== null && element.Email !== undefined && element.Email !== '') {
                var maskid = "";
                var myemailId = element.Email;
                var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                var postfix = myemailId.substring(myemailId.lastIndexOf("@"));
                for (var i = 0; i < prefix.length; i++) {
                    if (i == 0 || i == prefix.length - 1) {   ////////
                        maskid = maskid + prefix[i].toString();
                    }
                    else {
                        maskid = maskid + "*";
                    }
                }
                maskid = maskid + postfix;
                element.Email = maskid;
            }
            if (element.PhoneNo !== null && element.PhoneNo !== undefined && element.PhoneNo !== '') {
                var maskid = "";
                var myemailId = element.PhoneNo;
                var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                for (var i = 0; i < prefix.length; i++) {
                    if (i == 0 || i == prefix.length - 1) {   ////////
                        maskid = maskid + prefix[i].toString();
                    }
                    else {
                        maskid = maskid + "*";
                    }
                }
                maskid = maskid + postfix;
                element.PhoneNo = maskid;
            }

        });
        return res({  message:  req.i18n.__('GetRecentVisitors')['200'], data: result }).code(200);
    });
};

const payloadValidator = Joi.object({
    _id: Joi.string().required().min(24).max(24).description('iserId'),
}).options({ allowUnknown: true });

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetRecentVisitors']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetRecentVisitors']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator,response }