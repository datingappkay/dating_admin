"use strict";
const Joi = require("joi");
const logger = require('winston');
const coins = require("../../../models/coins");
const local = require("../../../locales");

const payloadValidator = Joi.object({
    text: Joi.string().description('search coin By coinTitle '),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    var user = req.params.text.trim();
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;

    // let condition = {limit,offset};
    var user = req.params.text.trim();
    if (user == "") {

    } else {

        condition =
            {
                "statusCode": { "$ne": 0 },
                "$or": [
                    { "coinTitle": new RegExp(user, "i") },
                ],

            }
        console.log("======>>>", condition);
    }

    coins.Select(condition, (err, result) => {
        if (err) {
            logger.silly(err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            coins.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('GetcoinByTxt')['200'], data: result, totalCount: ress }).code(200);
            });

           // return res({ message: req.i18n.__('GetcoinByTxt')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetcoinByTxt')['204'] }).code(204);
        }
    });
}
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetcoinByTxt']['200']), data: Joi.any(),totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['GetcoinByTxt']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }