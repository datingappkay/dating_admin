'use strict'
const Joi = require("joi");
const logger = require('winston');
const coin = require('../../../models/coins');
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");


const payloadValidator = Joi.object({
    coinId: Joi.array().required().description('coinIds'),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    let array = [];
    req.payload.coinId.forEach(element => {
        array.push(new ObjectID(element));
    });
    let condition = {
        '_id': { '$in': array }
    };
    let datatoUpdate = {
        statusCode: 0,
        status: "deleted",
        deleteTimestamp: new Date().getTime(),
    };

    /* we will use this code for delete image from server */
    // if (req.body.image) {
    //     // console.log(req.body.activeimage);
    //     var deleteImage = req.body.image.split('/');
    //     var ImageUrl = '/var/www/html/datum_2.0-admin/imageFile/' + active[5];
    //     if (fs.existsSync(ImageUrl)) {

    //         fs.unlinkSync(ImageUrl, function (err, del) {
    //             if (err1) {
    //                 console.log('could not delete  image', err);
    //             } else {
    //                 console.log(' image deleted', del);
    //             }
    //         });
    //     }
    // }

    coin.Delete(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('DeletePlans')['200'] }).code(200);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['Deletecoins']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, payloadValidator }