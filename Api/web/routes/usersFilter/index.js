let headerValidator = require('../../middleware/validator');
let PostAPI = require('./Post');
let GetAPI = require('./Get');
module.exports = [
    {
        method: 'POST',
        path: '/filterBy',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'usersFilter'],
            description: 'This API is used to get Most like on top.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response,

        }
    },

    {
        method: 'GET',
        path: '/usersFilters',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'usersFilter'],
            description: 'This API is use to  get From to To Date  vise  Active and Bann User selected with userType record details.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },
    

];