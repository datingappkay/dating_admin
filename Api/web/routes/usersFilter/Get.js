'use strict';
const Joi = require("joi");
const logger = require('winston');
var userListCollection = require("../../../models/userList");
const local = require("../../../locales");
const moment = require('moment');


const payloadValidator = Joi.object({
    dateFrom: Joi.number().description('Enter From Date(milliseconds) For Ex.946684800000'),
    dateTo: Joi.number().description('Enter To Date(milliseconds) for Ex. 33765897600000'),
    userType: Joi.string().required().allow(["1", "2", "3", "4", "5", "6"]).description('Configuration change userType'),
    text: Joi.string().description('enter You want to search'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).options({ allowUnknown: true });


const APIHandler = (req, res) => {

    const limit = parseInt(req.query.limit) || 100;
    const offset = parseInt(req.query.offset) || 0;

    let date = parseInt(req.query.dateTo)
    let dateTo = moment(date).add(24, 'hours').valueOf()
    let condition = []

    /*
       add condition for Active to set userType = 1
    */
    switch (req.query.userType) {
        case "1": {
            var statusCode = { deleteStatus: { $ne: 1 } }
            userListCollection = require("../../../models/userList");
            if (req.query.dateFrom) {
                condition.push({ "$match": { "registeredTimestamp": { '$gte': parseInt(req.query.dateFrom) } } });

            }
            if (dateTo) {
                condition.push({ "$match": { "registeredTimestamp": { '$lte': dateTo } } });
            }

            condition.push(

                {
                    "$match": {


                        profileStatus: { "$ne": 1 },
                        deleteStatus: { "$ne": 1 },

                    },

                },

                {
                    "$project": {
                        firstName: 1, about: 1, searchPreferences: 1, email: 1, lastOnline: 1, creationDate: 1, contactNumber: 1,
                        registeredTimestamp: 1, lastOnlineStatus: 1, city: "$address.city",
                        country: "$address.country", "dob": 1, "gender": 1, "height": 1,
                        otherImages: 1, "heightInFeet": 1, "profilePic": 1, "profileVideo": 1,
                        myPreferences: 1, "registeredTimestamp": 1, userType: 1,
                        myLikes: { "$size": { "$ifNull": ["$myLikes", []] } },
                        likedBy: { "$size": { "$ifNull": ["$likedBy", []] } },
                        myunlikes: { "$size": { "$ifNull": ["$myunlikes", []] } },
                        mySupperLike: { "$size": { "$ifNull": ["$mySupperLike", []] } },
                        supperLikeBy: { "$size": { "$ifNull": ["$supperLikeBy", []] } },
                        mySupperLike: { "$size": { "$ifNull": ["$mySupperLike", []] } },
                        disLikedUSers: { "$size": { "$ifNull": ["$disLikedUSers", []] } },
                        matchedWith: { "$size": { "$ifNull": ["$matchedWith", []] } },
                        recentVisitors: { "$size": { "$ifNull": ["$recentVisitors", []] } },

                    }
                },
                { "$sort": { "_id": -1 } },
            );
            break;
        }

        /*
           add condition for ban to set userType = 2
        */
        case "2": {
            var statusCode = { profileStatus: { $eq: 1 } }
            userListCollection = require("../../../models/userList");
            if (req.query.dateFrom) {
                condition.push({ "$match": { "banndTimeStamp": { "$gte": req.query.dateFrom } } });

            }
            if (dateTo) {
                condition.push({ "$match": { "banndTimeStamp": { "$lte": dateTo } } });
            }
            condition.push(
                {
                    "$match": {

                        profileStatus: { "$eq": 1 },
                    }
                },
                {
                    "$project": {
                        firstName: 1,
                        about: 1,
                        email: 1,
                        contactNumber: 1,
                        registeredTimestamp: 1,
                        lastOnline: 1,
                        myPreferences: 1,
                        lastOnlineStatus: 1,
                        city: "$address.city",
                        country: "$address.country",
                        "dob": 1,
                        "gender": 1,
                        "height": 1,
                        otherImages: 1,
                        "heightInFeet": 1,
                        "profilePic": 1,
                        "profileVideo": 1,
                        "registeredTimestamp": 1,
                        banndTimeStamp: 1
                    }

                },
                { "$sort": { "banndTimeStamp": -1 } },
            );
            break;
        }
        /*
      add condition for DeActive to set userType = 3
   */
        case "3": {
            var statusCode = { isDeactive: true }

            userListCollection = require("../../../models/userList");
            if (req.query.dateFrom) {
                condition.push({ "$match": { "isDeactiveTimestamp": { "$gte": req.query.dateFrom } } });

            }
            if (dateTo) {
                condition.push({ "$match": { "isDeactiveTimestamp": { "$lte": dateTo } } });
            }
            condition.push(
                {
                    "$match":
                    {

                        isDeactive: true

                    }
                },
                {
                    "$project": {
                        firstName: 1,
                        email: 1,
                        contactNumber: 1,
                        about: 1,
                        registeredTimestamp: 1,
                        lastOnlineStatus: 1,
                        city: "$address.city",
                        country: "$address.country",
                        lastOnline: 1,
                        myPreferences: 1,
                        otherImages: 1,
                        "dob": 1,
                        "gender": 1,
                        "height": 1,
                        "heightInFeet": 1,
                        "profilePic": 1,
                        "profileVideo": 1,
                        "registeredTimestamp": 1,
                        isDeactiveTimestamp: 1
                    }

                },
                { "$sort": { "isDeactiveTimestamp": -1 } },


            );
            break;
        }
        /*
              add condition for reporetedUsers to set userType = 4
           */
        case "4": {
            var statusCode = {}

            userListCollection = require("../../../models/userReports");
            if (req.query.dateFrom) {
                condition.push({ "$match": { "creation": { "$gte": req.query.dateFrom } } });

            }
            if (dateTo) {
                condition.push({ "$match": { "creation": { "$lte": dateTo } } });
            }
            condition.push(

                {
                    "$group": {
                        _id: "$targetUserId",
                        "creation": { "$last": "$creation" },
                        "NoOfReports": { "$sum": 1 }
                    }
                },
                { "$lookup": { "from": "userList", "localField": "_id", "foreignField": "_id", "as": "Data" } },
                { "$unwind": "$Data" },
                {
                    "$project": {
                        "creation": 1, "reportedUser": "$Data.firstName", "NoOfReports": 1,
                        "reportedPhoneNo": "$Data.contactNumber", "reportEdEmail": "$Data.email",
                        "dob": "$Data.dob", "gender": "$Data.gender", "height": "$Data.height",
                        "city": "$Data.address.city", "country": "$Data.address.country", "profilepic": "$Data.profilePic",
                        "otherImages": "$Data.otherImages", "myPreferences": "$Data.myPreferences", "about": "$Data.about"

                    }
                },
                { "$sort": { "creation": -1 } },


            );
            break;
        }
        /*
      add condition for Blocked to set userType = 5
   */
        case "5": {
            var statusCode = {}

            userListCollection = require("../../../models/userBlocks");
            if (req.query.dateFrom) {
                condition.push({ "$match": { "timestamp": { "$gte": req.query.dateFrom } } });

            }
            if (dateTo) {
                condition.push({ "$match": { "timestamp": { "$lte": dateTo } } });
            }
            condition.push(

                {
                    "$group": {
                        _id: "$targetUserId",
                        "timestamp": { "$last": "$timestamp" },
                        "NoOfBlocked": { "$sum": 1 }
                    }
                },
                { "$lookup": { "from": "userList", "localField": "_id", "foreignField": "_id", "as": "Data" } },
                { "$unwind": "$Data" },
                {
                    "$project": {
                        "timestamp": 1, "blockedUser": "$Data.firstName", "NoOfBlocked": 1,
                        "blockedPhoneNo": "$Data.contactNumber", "blockedEdEmail": "$Data.email",
                        "dob": "$Data.dob", "gender": "$Data.gender", "height": "$Data.height",
                        "city": "$Data.address.city", "country": "$Data.address.country", "profilepic": "$Data.profilePic",
                        "otherImages": "$Data.otherImages", "myPreferences": "$Data.myPreferences", "about": "$Data.about"
                    }
                },
                { "$sort": { "timestamp": -1 } },



            );
            break;
        }
        /*
      add condition for DeleteUser to set userType = 6
   */
        case "6": {
            var statusCode = { deleteStatus: 1 }

            userListCollection = require("../../../models/userList");
            if (req.query.dateFrom) {
                condition.push({ "$match": { "deleteTimeStamp": { "$gte": req.query.dateFrom } } });

            }
            if (dateTo) {
                condition.push({ "$match": { "deleteTimeStamp": { "$lte": dateTo } } });
            }
            condition.push(
                {
                    "$match":
                    {
                        // "deleteTimeStamp": {
                        //     '$gte': parseInt(req.query.dateFrom),
                        //     '$lte': parseInt(req.query.dateTo)

                        // },
                        deleteStatus: 1,

                    }
                },
                {
                    "$project": {
                        firstName: 1,
                        email: 1,
                        about: 1,
                        contactNumber: 1,
                        registeredTimestamp: 1,
                        otherImages: 1,
                        lastOnline: 1,
                        "myPreferences": 1,
                        city: "$address.city",
                        country: "$address.country",
                        "dob": 1,
                        "gender": 1,
                        "height": 1,
                        "heightInFeet": 1,
                        "profilePic": 1,
                        "profileVideo": 1,
                        "registeredTimestamp": 1,
                        deleteTimeStamp: 1
                    }

                },
                { "$sort": { "deleteTimeStamp": -1 } },


            );
            break;
        }

    }


    if (req.query.text != "") {
        condition.push({
            "$match": {
                "$or": [
                    { "firstName": new RegExp(req.query.text, 'i') },
                    { "email": new RegExp(req.query.text, 'i') },
                    { "contactNumber": new RegExp(req.query.text, 'i') },
                    { "ReportedUser": new RegExp(req.query.text, 'i') },
                    { "reportedPhoneNo": new RegExp(req.query.text, 'i') },
                    { "reportEdEmail": new RegExp(req.query.text, 'i') },
                    { "blockedUser": new RegExp(req.query.text, 'i') },
                    { "blockedPhoneNo": new RegExp(req.query.text, 'i') },
                    { "blockedEdEmail": new RegExp(req.query.text, 'i') },
                    { "PhoneNo": new RegExp(req.query.text, 'i') },
                    { "contactNumber": new RegExp(req.query.text, 'i') }
                ]
            }
        },
            { "$skip": offset * limit }, { "$limit": limit }
        );


    }
    else {
        condition.push({ "$skip": offset * limit }, { "$limit": limit });
    }
    // console.log("condition ", JSON.stringify(condition))
    userListCollection.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
        else if (result) {

            result.forEach(element => {
                if (element.email !== null && element.email !== undefined && element.email !== '') {
                    var maskid = "";
                    var myemailId = element.email;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                    var postfix = myemailId.substring(myemailId.lastIndexOf("@"));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.email = maskid;
                }
                if (element.contactNumber !== null && element.contactNumber !== undefined && element.contactNumber !== '') {
                    var maskid = "";
                    var myemailId = element.contactNumber;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                    var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.contactNumber = maskid;
                }

            });
            userListCollection.TotalCount(statusCode, (err, ress) => {
                return res({ message: req.i18n.__('GetDateFromToto')['200'], data: result, totalCount: ress }).code(200);
            });


        } else {
            return res({ message: req.i18n.__('GetDateFromToto')['204'] }).code(204);
        }
    })
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetDateFromToto']['200']), data: Joi.any(), totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['GetDateFromToto']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }