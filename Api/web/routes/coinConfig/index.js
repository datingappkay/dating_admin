let headerValidator = require('../../middleware/validator');
let PostAPI = require('./Post');
let GetAPI = require('./Get');
let DeleteAPI = require('./Delete');
let PutAPI = require('./Put');

module.exports = [


    {
        method: 'PUT',
        path: '/coinConfig',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'coinConfig'],
            description: 'This API is used to update coinConfig.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
    {
        method: 'GET',
        path: '/coinConfigs',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'coinConfig'],
            description: 'This API is used to get coinPlan.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,

                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },
    {
        method: 'PUT',
        path: '/coinConfigDelete',
        handler: DeleteAPI.APIHandler,
        config: {
            tags: ['api', 'coinConfig'],
            description: 'This API is used to Delete coinConfig.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: DeleteAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
    {
        method: 'PUT',
        path: '/coinConfigUpdate',
        handler: PutAPI.APIHandler,
        config: {
            tags: ['api', 'coinConfig'],
            description: 'This API is used to update coinConfig.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    }


];