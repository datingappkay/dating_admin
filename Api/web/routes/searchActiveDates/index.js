let headerValidator = require('../../middleware/validator');
let PostAPI = require("./Post");
let GetByIdAPI = require("./GetbyId");
module.exports = [
    
    {
        method: 'POST',
        path: '/searchActiveDates',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'dates'],
            description: 'This API is used to login an user in the app.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response,

        }
    },
    {
        method: 'GET',
        path: '/activeDate/{_id}',
        handler: GetByIdAPI.APIHandler,
        config: {
            tags: ['api', 'dates'],
            description: 'This API is used to login an user in the app.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByIdAPI.payloadValidator,
                query: GetByIdAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByIdAPI.response,

        }
    }
];