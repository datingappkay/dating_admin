'use strict'
const Joi = require("joi");
const logger = require('winston');
const otpLogs = require("../../../models/otpLogs");
const local = require("../../../locales");


const APIHandler = (req, res) => {
    var condition = []

    //console.log("====>", req.query)
    var user = req.query.text;
    if (req.query.status == 1) {
        condition.push(
            {
                "$lookup": { "from": "userList", "localField": "phone", "foreignField": "contactNumber", "as": "optLogs" }
            },
            { "$unwind": "$optLogs" },
            {
                "$project": {
                    userName: "$optLogs.firstName",
                    phone: 1,
                    otp: 1,
                    time: 1,
                    deviceId: 1,
                    creationTs: 1,
                    creationDate: 1,
                }
            }, {
                "$match": {
                    "$or": [
                        { "userName": new RegExp(user, 'i') },
                        { "phone": new RegExp(user, 'i') }
                    ]
                }
            },
            { "$sort": { "_id": -1 } },


        )
    }
    else {

        condition.push(
            {
                "$lookup": { "from": "userList", "localField": "phone", "foreignField": "contactNumber", "as": "optLogs" }
            }, { "$unwind": "$optLogs" },
            {
                "$project": {
                    userName: "$optLogs.firstName",
                    phone: 1,
                    otp: 1,
                    time: 1,
                    deviceId: 1,
                    creationTs: 1,
                    creationDate: 1,


                }
            },

            { "$sort": { "_id": -1 } },

        )
    }

    //console.log("=============",condition)
    otpLogs.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
           // console.log("ressssss", result)
            result.forEach(element => {
                if (element.phone !== null && element.phone !== undefined && element.phone !== '') {
                    var maskid = "";
                    var myemailId = element.phone;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                    var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.phone = maskid;
                }
                if (element.otp !== null && element.otp !== undefined && element.otp !== '') {
                    var maskid = "";
                    element.otp = element.otp.toString().charAt(0) + "****" + element.otp.toString().charAt(5);
                    //console.log("myemailId", myemailId)


                }
            });
            return res({ message: req.i18n.__('TwillioOTP')['200'], data: result }).code(200);

        } else {
            return res({ message: req.i18n.__('TwillioOTP')['204'], data: [] }).code(204);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['TwillioOTP']['200']), data: Joi.any()
        },
        422: { message: Joi.any().default(local['TwillioOTP']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response }