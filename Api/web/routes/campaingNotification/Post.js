'use strict';
const Joi = require("joi");
const logger = require('winston');
const Cryptr = require('cryptr');
const cryptr = new Cryptr("3Embed1008");
const campainDetail = require("../../../models/campaign");
const fcm = require("../../../library/fcm/")
const local = require("../../../locales");

const payloadValidator = Joi.object({

    usersId: Joi.array().required().description('usersIds for valid user for campin'),
    message: Joi.string().description("message,Eg.for august sale").error(new Error('message is missing')),
    campainTitle: Joi.string().description("title,Eg.for  sale").error(new Error('message is missing')),
    campainImageUrl: Joi.string().description("campainImageUrl").error(new Error('campainImageUrl is missing')),
    url: Joi.string().description("for E.x www.daum.com").error(new Error('Url is missing')),




}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    var title = req.payload.pushTitle
    var msg = req.payload.message
    var url = req.payload.url
    var image = req.payload.campainImageUrl
    //  console.log("-------------->", req.payload.usersId)
    req.payload.usersId.forEach(element => {

        let request = {
            data: {
                type: "30", url: url, image: image, title: title, message: msg, deviceType: element.deviceType
            },
            notification: { "title": title, "message": msg }
        };
          console.log("=======================>", request)
        fcm.sendPushToTopic(`/topics/${element._id}`, request, () => { })
    })
    return res({ message: req.i18n.__('PostCampingNotification')['200'] }).code(200);

};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostCampingNotification']['200']),
        },
        422: { message: Joi.any().default(local['PostCampingNotification']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator, response }