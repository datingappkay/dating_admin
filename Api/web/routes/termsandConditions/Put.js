'use strict';
const adminConfig = require("../../../models/adminConfig");
var fs = require('fs');
const Joi = require("joi");
const local = require("../../../locales");
const moment = require('moment');


const payloadValidator = Joi.object({

    configData: Joi.any().required().description('data'),
    configType: Joi.any().required().description('configType'),
    title: Joi.any().required().description('t and c')

}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    // fs.writeFile("/var/www/html/datum_2.0-admin/TermsAndConditions.html", req.payload.data, function (err) {
    //     if (err) {
    //         return console.log(err);
    //     }
    //     return res({ message: req.i18n.__('PutTermsandConditions')['200']}).code(200);
    // });
    var configType = parseInt(req.payload.configType);
    var configData = req.payload.configData.data.trim();

    var updateData = {
        configType: configType,
        configData: configData,
        title: req.payload.title,
        createdOn: moment().valueOf()
    };
    adminConfig.Update(
        { configType: configType },
        updateData,
        function (err, data) {
            if (err) {
                return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
            } else {
                return res({ message: req.i18n.__('PutTermsandConditions')['200'], data: data }).code(200);
            }
        }
    );


};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['PutTermsandConditions']['200']), data: Joi.any(),
        },
        422: { message: Joi.any().default(local['PutTermsandConditions']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator, response }