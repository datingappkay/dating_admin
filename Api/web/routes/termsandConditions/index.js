
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PutAPI = require("./Put");
module.exports = [
    {
        method: 'GET',
        path: '/termsAndConditions',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'TermsAndConditions'],
            description: 'This API is used to login an user in the app.',
            auth: false,
            validate: {
               // headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },
    {
        method: 'PUT',
        path: '/termsAndConditions',
        handler: PutAPI.APIHandler,
        config: {
            tags: ['api', 'termsAndConditions'],
            description: 'This API is used to login an user in the app.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response

        }
    }
];