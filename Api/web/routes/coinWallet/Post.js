'use strict'
const Joi = require("joi");
const moment = require("moment");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const walletCustomer = require('../../../models/walletCustomer');
const coinWalletCollections = require('../../../models/coinWallet');
const local = require("../../../locales");
const mqtt = require("../../../library/mqtt");


/**
 * @method POST walletCustomer
 * @description This API use to  POST walletCustomer.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId
 
 * @returns  200 : User walletCustomer successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  204 : targetUserId  doesnot exist in the database,try with the different ID.
 * @returns  500 : An unknown error has occurred.
 */
let APIHandler = (req, res) => {

    let _id = req.payload.userId;
    let timestamp = new Date().getTime();
    let transctionTime = timestamp,
        transctionDate = timestamp;

    function getCoinWallet() {
        return new Promise((resolve, reject) => {
            /* in coinWallet _id is userId, so we find only _id */
            coinWalletCollections.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
                if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                if (!result) {
                    coinWalletCollections.Insert({ _id: ObjectID(_id), "coins": { "Coin": 0 } }, () => { });
                    return resolve({ "Coin": 0 });
                } else {
                    var coins = result.coins.Coin
                    return resolve(coins);

                }

            });
        });
    }
    getCoinWallet()
        .then(function (value) { return insertInWalletCustomer(value) })
        .then(function (value) { return res({ message: req.i18n.__('PostwalletCustomer')['200'] }).code(200); })
        .catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });

    function insertInWalletCustomer(coinWallet) {
        return new Promise((resolve, reject) => {

            let insertData = [];
            let txnId = "TXN-ID-" + Math.floor(Math.random() * 10000000000);
            let coinBalance = coinWallet;

            insertData.push({
                "txnId": txnId,
                "userId": ObjectID(_id),
                "txnType": req.payload.txnType,
                "txnTypeCode": (req.payload.txnType == "CREDIT") ? 1 : (req.payload.txnType == "DEBIT") ? 2 : 3,
                "trigger": (req.payload.txnType == "DEBIT") ? `${process.env.APPNAME} admin removed ${req.payload.coinAmount} coins from your wallet` : `${process.env.APPNAME} admin added ${req.payload.coinAmount} coins to your wallet`,
                "docTypeCode": (req.payload.txnType == "DEBIT") ? 3 : 2,
                "docType": (req.payload.txnType == "DEBIT") ? "on admin debit" : "on admin credit",
                "userPurchaseTime": new Date().getTime() + new Date().getTimezoneOffset(),
                "note": req.payload.note,
                "currencySymbol": "$",
                "currency": "USD",
                "coinType": "Coin",
                "coinOpeingBalance": coinBalance,
                "cost": 0,
                "coinAmount": req.payload.coinAmount,
                "coinClosingBalance": (req.payload.txnType == "DEBIT") ? coinBalance - req.payload.coinAmount : coinBalance + req.payload.coinAmount,
                // "coinClosingBalance": coinBalance + req.payload.coinAmount,
                "paymentType": "Admin",
                "timestamp": timestamp,
                "transctionTime": transctionTime,
                "transctionDate": transctionDate,
                "paymentTxnId": "N/A",
                "initatedBy": "Admin",

            });
            var update = (req.payload.txnType == "DEBIT") ? coinBalance - req.payload.coinAmount : coinBalance + req.payload.coinAmount;
            walletCustomer.Insert(insertData, () => { });
            coinWalletCollections.Update({ _id: ObjectID(_id) }, { "coins": { "Coin": update } }, () => { });
            mqtt.publish(_id, JSON.stringify({ "messageType": "AdminCoin",  }), { qos: 2 }, (e, r) => { if (e) logger.error("/boost/match", e) });

            return resolve("---");
        });
    }

};

let response = {
    status: {
        200: { message: local['PostwalletCustomer']['200'], data: Joi.any() },
        204: { message: local['PostwalletCustomer']['204'] },
        400: { message: local['genericErrMsg']['400'] },
        500: { message: local['genericErrMsg']['500'] },
    }
}

const payloadValidator = Joi.object({
    userId: Joi.string().required().description('5b056ef1281de02df59bf15a'),
    txnType: Joi.string().required().description('txnType'),
    note: Joi.string().required().description('note'),
    //currency: Joi.string().required().description('currency'),
    // currencySymbole: Joi.string().required().description('currencySymbole'),
    // coinType: Joi.string().required().description('coinType'),
    // cost: Joi.number().required().description('cost'),
    coinAmount: Joi.number().required().description('coinAmount'),
    // paymentType: Joi.string().required().description('paymentType'),


}).options({ allowUnknown: true });


module.exports = { APIHandler, payloadValidator }