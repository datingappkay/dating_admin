'use strict';

const Joi = require("joi");
const logger = require('winston');
const walletCustomer = require("../../../models/walletCustomer");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");



const APIHandler = (req, res) => {
    // const limit = parseInt(req.query.limit) || 20;
    // const offset = parseInt(req.query.offset) || 0;
    let match = { userId: ObjectID(req.params._id) }

    let project = {
        txnType: 1, trigger: 1, currency: 1,
        currencySymbol: 1, coinType: 1,
        coinOpeingBalance: 1, cost: 1, txnId: 1, note: 1,
        "coinAmount": 1, "coinClosingBalance": 1, "paymentType": 1,
        "timestamp": 1, "transctionDate": 1,
        "paymentTxnId": 1,
        "initatedBy": 1, "transctionTime": 1,

    }

    walletCustomer.selectWithProject(match, { "_id": -1 }, project, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
        if (result) {
            return res({ message: req.i18n.__('GetcoinWalletById')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetcoinWalletById')['204'] }).code(204);
        }





    });


};

const payloadValidator = Joi.object({
    _id: Joi.string().required().description('_id'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).options({ allowUnknown: true });

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetcoinWalletById']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetcoinWalletById']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }