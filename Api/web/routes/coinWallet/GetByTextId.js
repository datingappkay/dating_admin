"use strict";
const Joi = require("joi");
const logger = require('winston');
const coinWallet = require("../../../models/walletCustomer");
const local = require("../../../locales");

const payloadValidator = Joi.object({
    text: Joi.string().description('search coinWallet UserName Or Email'),
    dateFrom: Joi.string().description('Enter From Date(milliseconds) For Ex.1527580357728'),
    dateTo: Joi.string().description('Enter dateTo(milliseconds) For Ex.1527580357728'),
    txnType: Joi.string().allow(["All", "DEBIT", "PURCHSE"]).description('Configuration change txnType'),
    coinType: Joi.string().allow(["All", "Gold", "Silver"]).description('Configuration change coinType'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).options({ allowUnknown: true });
const APIHandler = (req, res) => {


    // const limit = parseInt(req.query.limit) || 20;
    // const offset = parseInt(req.query.offset) || 0;

    let condition = []
    // { "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "Data" } },
    // { "$unwind": "$Data" },
    // {
    //     "$project": {
    //         // "userName": "$Data.firstName",
    //         // "email": "$Data.email",
    //         // "phone": "$Data.contactNumber",
    //         txnType: 1, trigger: 1, currency: 1,
    //         currencySymbol: 1, coinType: 1,
    //         coinOpeingBalance: 1, cost: 1,
    //         "coinAmount": 1, "coinClosingBalance": 1, "paymentType": 1,
    //         "timestamp": 1, "transctionDate": 1,
    //         "paymentTxnId": 1,
    //         "initatedBy": 1, "transctionTime": 1,
    //     }
    // },
    // { "$skip": offset },
    // { "$limit": limit },


    /*
add condition for txnType
*/
    switch (req.query.txnType) {
        case "All": {
            //no add any condition 
            break;
        }
        case "CREDIT": {
            condition.push({ "$match": { "txnType": "CREDIT" } });
            break;
        }
        case "DEBIT": {
            condition.push({ "$match": { "txnType": "DEBIT" } });
            break;
        }

    }

    /*
add condition for coinType
*/

    switch (req.query.coinType) {
        case "All": {
            //no add any condition 
            break;
        }
        case "Coin": {
            condition.push({ "$match": { "coinType": "Coin" } });
            break;
        }
        // case "Silver": {
        //     condition.push({ "$match": { "coinType": "Silver" } });
        //     break;
        // }

    }


    /*
add condition for from Date
*/
    if (req.query.dateFrom) {
        condition.push({ "$match": { "timestamp": { "$gte": parseInt(req.query.dateFrom) } } });

    }
    /*
   add condition for to Date
   */
    if (req.query.dateTo) {
        condition.push({ "$match": { "timestamp": { "$lte": parseInt(req.query.dateTo) } } });
    }


    /*
     add condition for text search
     */

    // if (req.query.text) {
    //     condition.push(
    //         {
    //             "$match": {
    //                 "$or": [
    //                     { "userName": new RegExp(req.query.text, 'i') },
    //                     { "email": new RegExp(req.query.text, 'i') },

    //                 ]
    //             }
    //         },
    //     );

    // }


    console.log("======for search>++++++++++++++++>>", JSON.stringify(condition));



    coinWallet.Aggregate(condition, (err, result) => {
        if (err) {
            logger.silly(err)
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            return res({ message: req.i18n.__('GetcoinWalletTxt')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetcoinWalletTxt')['204'] }).code(204);
        }
    });
}
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetcoinWalletTxt']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetcoinWalletTxt']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }