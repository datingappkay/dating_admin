
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PostAPI = require("./Post");
let DeleteAPI = require("./Delete");
let PUTAPI = require('./Put');

module.exports = [
    {
        method: 'GET',
        path: '/question',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'question'],
            description: 'This API is used to get an user prefrances.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response,

        }
    },
    {
        method: 'POST',
        path: '/question',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'question'],
            description: 'This API is used to post an prefrances.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response,

        }
    },
    {
        method: 'POST',
        path: '/questions',
        handler: DeleteAPI.APIHandler,
        config: {
            tags: ['api', 'question'],
            description: 'This API is used to delete preference.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: DeleteAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response

        }
    },
    {
        method: 'PUT',
        path: '/questions',
        handler: PUTAPI.APIHandler,
        config: {
            tags: ['api', 'question'],
            description: 'This API is used to update user preferance.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PUTAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PUTAPI.response
        }
    },
];