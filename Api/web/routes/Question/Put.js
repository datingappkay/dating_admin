'use strict'
const Joi = require("joi");
const logger = require('winston');
const preferenceTable = require('../../../models/searchPreferences');
const local = require("../../../locales");
const Timestamp = require('mongodb').Timestamp;


const payloadValidator = Joi.object({
    filterId: Joi.string().required().description('filterId'),
    newtitledropdown: Joi.string().required().description('newtitledropdown').error(new Error("Titile type  is Missing")),
    newisMandatory: Joi.boolean().required().description('new is Mandatory').error(new Error("is Mandatory is Missing")),
    newtype: Joi.number().required().description('newtype is Mandatory').error(new Error("Type is Missing")),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    console.log("edittttttttt data============>", req.payload)

    var title = {};
    var values = {};
    var lable = {};
    var types = req.payload.newvalues
    req.payload.otherName.forEach(e => {

        values[e.langCode] = e.values.split(',');
        lable[e.langCode] = e.fieldName;
        (req.payload.newtitledropdown == "My Interests") ? title[e.langCode] = "Sở thích của tôi" : (req.payload.newtitledropdown == "More about yourself") ? title[e.langCode] = "Tìm hiểu thêm về bản thân" : (req.payload.newtitledropdown == "My Vitals") ? title[e.langCode] = "Sức sống của tôi" : ""

    });

    let dataToSend =
    {
        //"titleType": req.payload.newtitledropdown,
        "title": req.payload.newtitledropdown,
        "otherTitle": title,
        "label": req.payload.newFieldName,
        "options": (typeof types == "object") ? req.payload.newvalues : req.payload.newvalues.split(','),
        "otherLabel": lable,
        "otherOptions": values,
        "type": parseInt(req.payload.newtype),
        "mandatory": req.payload.newisMandatory,
        "status": 1,
        "timestamp": new Date().getTime(),
        "Priority": req.payload.Priority

    }

    let id = req.payload.filterId


    console.log("===========data to update", dataToSend)

    preferenceTable.UpdateById(id, dataToSend, (err, result) => {
        if (err) {
            logger.silly(err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('PutUsers')['200'], data: dataToSend.titleType }).code(200);
        }
    });
};

const response = {
    // status: {
    //     200: {
    //         message: Joi.any().default(local['PutUsers']['200']), data: Joi.any()
    //     },
    //     204: { message: Joi.any().default(local['genericErrMsg']['204']) },
    //     400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    //     500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    // }
}

module.exports = { APIHandler, response, payloadValidator }