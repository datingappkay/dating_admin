'use strict';

const Joi = require("joi");
const logger = require('winston');
const preferenceTable = require("../../../models/preferenceTable");
const searchPreferencesCollection = require("../../../models/searchPreferences");
const local = require("../../../locales");
const Promise = require('promise');

const payloadValidator = Joi.object({
    status: Joi.number().required().description('status'),
}).options({ allowUnknown: true });


const APIHandler = (req, res) => {
    var data;
 //   console.log("=======================>", req.query.status)


    if (req.query.status == 1) {
        data = {}
    }
    if (req.query.status == 2) {
        data = {
            "title": req.query.title
        }
    }

    let Mypreference = [];
    let searchPreferences = [];

    let getMyPreference = () => {
        return new Promise((resolve, reject) => {
            preferenceTable.Select({}, (err, result) => {
                if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

                if (!result) {
                    return res({ message: req.i18n.__('GetPrefrances')['412'] }).code(412);
                } else {
                    return resolve(Mypreference.push(result));
                }

            });
        });
    }

    let getSearchPreferences = () => {
        return new Promise((resolve, reject) => {
            searchPreferencesCollection.Select(data, (err, res) => {
                if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

                if (!res) {
                    return res({ message: req.i18n.__('GetPrefrances')['412'] }).code(412);
                } else {
                    return resolve(searchPreferences.push(res));
                }

            });
        });
    }
    getMyPreference()
        .then(function () { return getSearchPreferences() })
        .then(function () { return res({ message: req.i18n.__('GetPrefrances')['200'], data: { Mypreference: Mypreference, searchPreferences: searchPreferences } }).code(200); })
        .catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetPrefrances']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetPrefrances']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, response, payloadValidator }