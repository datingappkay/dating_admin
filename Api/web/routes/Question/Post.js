'use strict';

const Joi = require("joi");
const logger = require('winston');
const preferenceTable = require('../../../models/searchPreferences');
const local = require("../../../locales");


const payloadValidator = Joi.object({
    titledropdown: Joi.string().required().description('titleType').error(new Error("Titile type  is Missing")),
    isMandatory: Joi.boolean().required().description('mandatory').error(new Error("is Mandatory is Missing")),
    type: Joi.number().required().description('type is Mandatory').error(new Error("Type is Missing")),

}).options({ allowUnknown: true });
const APIHandler = (req, res) => {

 console.log("getting data============>", req.payload)

    let condition = [
        {
            "$group": {
                "_id": null,
                "maxProirity": { "$max": "$Priority" }
            }
        }
    ]
    var title = {};
    var values = {};
    var lable = {};
    req.payload.otherName.forEach(e => {

        values[e.langCode] = e.values.split(',');
        lable[e.langCode] = e.fieldName;
        (req.payload.titledropdown =="My Interests")?title[e.langCode] = "Sở thích của tôi":(req.payload.titledropdown =="More about yourself")?title[e.langCode]="Tìm hiểu thêm về bản thân":(req.payload.titledropdown =="My Vitals")?title[e.langCode]="Sức sống của tôi":""

    });

    //  console.log("values", values)

    let data =
    {
        //"titleType": req.payload.titledropdown,
        "title": req.payload.titledropdown,
        "otherTitle": title,
        "label": req.payload.fieldName,
        "options": req.payload.values.split(','),
        "otherLabel": lable,
        "otherOptions": values,
        "type": parseInt(req.payload.type),
        "mandatory": req.payload.isMandatory,
        "status": 1,
        "timestamp": new Date().getTime(),

    }


    preferenceTable.Aggregate(condition, (err, result) => {

        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
        req.payload["Priority"] = (result.length) ? result[0]["maxProirity"] + 1 : 1;
        data.Priority = req.payload.Priority
        console.log("data to insert===========>", data)
        preferenceTable.Insert(data, (err, result) => {
            if (err) {
                return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
            } else {

                return res({ message: req.i18n.__('PostPrefrances')['200'], data: data.titleType }).code(200);
            }

        })
    })


};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostPrefrances']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['PostPrefrances']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator, response }