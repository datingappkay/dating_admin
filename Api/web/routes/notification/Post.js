'use strict';
const Joi = require("joi");
const logger = require('winston');
const Cryptr = require('cryptr');
const fcm = require("../../../library/fcm")
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");
const userList = require("../../../models/userList")

const payloadValidator = Joi.object({

    usersId: Joi.array().required().description('usersIds for valid user for campin'),
    message: Joi.string().description("message,Eg.for august sale").error(new Error('message is missing')),
    campainTitle: Joi.string().description("title,Eg.for  sale").error(new Error('message is missing')),





}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    var title = req.payload.campainTitle
    var msg = req.payload.message

    let array = [];
    req.payload.usersId.forEach(element => {
        array.push(new ObjectID(element));
    });
    let condition = { '_id': { '$in': array } };
    let project = { firstName: 1, currentLoggedInDevices: 1 }

    userList.selectWithProject(condition, { "_id": -1 }, project, (err, result) => {
        if (err) {
            logger.silly(err);

        } else {
            result.forEach(e => {
                var deviceType = e.currentLoggedInDevices.deviceType


                let request = {
                    data: {
                        type: "40", title: title, message: msg, deviceType: deviceType
                    },
                    notification: { "title": title, "message": msg }
                };
                fcm.sendPushToTopic(`/topics/${e._id}`, request, () => { })


                // if (deviceType == 1) {
                //     let request = {
                //         data: {
                //             type: "40", title: title || "", messag: msg || "", deviceType: deviceType, url: "xyz.com", image: "image.pgp"
                //         },
                //         notification: { "title": title, "message": msg }
                //     };
                //     console.log("Iosssssssssssssssssssss", request)
                //     fcm.sendPushToTopic(`/topics/${e._id}`, request, () => { })
                // } else {
                //     let request = {
                //         data: {
                //             type: "40", title: title || "", messag: msg || "", deviceType: deviceType, url: "xyz.com", image: "image.jpg"
                //         },
                //     };
                //     console.log("Androiddddddddddddddddddd", request)
                //     fcm.sendPushToTopic(`/topics/${e._id}`, request, () => { })
                // }

            });
            return res({ message: "succses..!", data: result })
        }
    });

    //     req.payload.usersId.forEach(element => {

    //         let request = {
    //             data: {
    //                 type:"40",title:title,msg:msg
    //             },
    //             notification: { "title":title, "message": msg }
    //         };
    // console.log("=++++++++++++>",request)
    //      fcm.sendPushToTopic(`/topics/${element.trim()}`, request, () => { })
    //     })
    //     return res({ message: req.i18n.__('PushNotification')['200'] }).code(200);

};
const response = {
    // status: {
    //     200: {
    //         message: Joi.any().default(local['PushNotification']['200']),
    //     },
    //     204: { message: Joi.any().default(local['genericErrMsg']['204']) },
    //     400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    //     500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    // }
}
module.exports = { APIHandler, payloadValidator, response }