'use strict';

const Joi = require("joi");
const logger = require('winston');
const pushNotificationCollection = require("../../../models/pushNotification");
const local  = require("../../../locales");


const APIHandler = (req, res) => {
    let condition = [
        { "$sort": { "_id": -1 } }
    ];

    pushNotificationCollection.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message:req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            return res({  message:req.i18n.__('GetNotification')['200'], data: result }).code(200);
        } else {
            return res({ message:req.i18n.__('GetNotification')['204'] }).code(204);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetNotification']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetNotification']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler,response }