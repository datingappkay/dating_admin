
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PostAPI = require('./Post');

module.exports = [
    {
        method: 'GET',
        path: '/notifications',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'notifications'],
            description: 'This API is used to Get Notification .',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },
    {
        method: 'POST',
        path: '/notification',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'notifications'],
            description: 'This API is used to Post Notification.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response

        }
    }
];