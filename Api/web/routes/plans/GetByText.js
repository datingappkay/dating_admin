"use strict";
const Joi = require("joi");
const logger = require('winston');
const plans = require("../../../models/plans");
const local = require("../../../locales");

const payloadValidator = Joi.object({
    text: Joi.string().required().description('enter You want to search'),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    var user = req.params.text.trim();


    let condition =
        {
            "$or": [
                { "planName": new RegExp(user, "i") },
                { "status": new RegExp(user, "i") },
                { "cost": new RegExp(user, "i") },
                { "descriptions": new RegExp(user, "i") },
            ]
        }
    console.log("======for search>>>", condition);



    plans.Select(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            plans.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('GetPlanByText')['200'], data: result, totalCount: ress }).code(200);
            });

            //return res({ message: req.i18n.__('GetPlanByText')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetPlanByText')['204'] }).code(204);
        }
    });
}
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetPlanByText']['200']), data: Joi.any(),totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['GetPlanByText']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }