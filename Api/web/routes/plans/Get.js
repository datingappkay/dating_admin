'use strict'
const Joi = require("joi");
const logger = require('winston');
const plans = require("../../../models/plans");
const local = require("../../../locales");

const queryValidator = Joi.object({
    planType: Joi.string().required().allow(["Active", "Deleted"]).description('Configuration change by planType '),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();
const APIHandler = (req, res) => {
    let condition = [];

    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;

    switch (req.query.planType) {
        case "Active": {
            condition.push(
                { "$match": { "statusCode": { "$ne": 0 } } },
                {
                    "$project": {
                        cost: 1,
                        planName: 1,
                        status: 1,
                        description: 1,
                        durationInMonths:1,
                        currencySymbol:1,
                        currencyCode:1,
                        likeCount:1,
                        rewindCount:1,
                        superLikeCount:1


                    }
                },
                { "$sort": { "_id": -1 } },{ "$skip": offset  }, { "$limit": limit }
            )
            break;
        }
        case "Deleted": {
            condition.push(
                { "$match": { "statusCode": { "$eq": 0 } } },
                {
                    "$project": {
                        cost: 1,
                        planName: 1,
                        status: 1,
                        description: 1,
                        currencySymbol:1,
                    }
                },

                { "$sort": { "_id": -1 } },{ "$skip": offset  }, { "$limit": limit }
            )
            break;
        }

    }




    plans.Aggregate(condition, (err, result) => {
        if (err) {

            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            plans.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('GetPlans')['200'], data: result, totalCount: ress }).code(200);
            });

            //return res({ message: req.i18n.__('GetPlans')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetPlans')['204'] }).code(204);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetPlans']['200']), data: Joi.any(),totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['GetPlans']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, queryValidator }