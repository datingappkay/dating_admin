
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PostAPI = require('./Post');
let DeleteAPI = require('./Delete');
let GetByText = require('./GetByText');
let GetById = require("./GetById");
let PUTAPI =require('./Put')

module.exports = [

    {
        method: 'GET',
        path: '/plan',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'plan'],
            description: 'This API is used to Get User All plan.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
    {
        method: 'PUT',
        path: '/plans',
        handler: DeleteAPI.APIHandler,
        config: {
            tags: ['api', 'plan'],
            description: 'This API is used to Delete Id wise plan.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: DeleteAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
    {
        method: 'POST',
        path: '/plan',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'plan'],
            description: 'This API is used to Post plan.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response

        }
    },
    {
        method: 'GET',
        path: '/Plan/{text}',
        handler: GetByText.APIHandler,
        config: {
            tags: ['api', 'plan'],
            description: 'This API is used to search plan.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByText.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByText.response

        }
    },

    {
        method: 'GET',
        path: '/plan/{_id}',
        handler: GetById.APIHandler,
        config: {
            tags: ['api','plan'],
            description: 'This API is used to get plan by Id.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetById.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetById.response,

        }
    },
    {
        method: 'PUT',
        path: '/plan',
        handler: PUTAPI.APIHandler,
        config: {
            tags: ['api', 'users'],
            description: 'This API is used to update user profile.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PUTAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PUTAPI.response
        }
    },

];