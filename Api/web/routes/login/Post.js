"use strict";
const Joi = require("joi");
const async = require("async");
const Cryptr = require('cryptr');
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const adminCollection = require('../../../models/admin');
const Auth = require("../../middleware/authentication.js");
const local = require("../../../locales");
const Config = process.env;
const cryptr = new Cryptr("3Embed1008");

const payloadValidator = Joi.object({
    userName: Joi.string().required().description('user name'),
    password: Joi.string().required().description('password'),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {

   // logger.silly("===?>", req.headers.authorization);
    let condition = { userName: req.payload.userName, password: req.payload.password };
    adminCollection.SelectOne(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            var data = {
                token: Auth.SignJWT({ _id: result._id.toString() }, 'admin', 3600),
                userName: result.userName
            };
            return res({ message: req.i18n.__('PostLogin')['200'], data: data }).code(200);
        } else {
            return res({ message: req.i18n.__('PostLogin')['422'] }).code(422);
        }
    })
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostLogin']['200']), data: Joi.any()
        },
        422: { message: Joi.any().default(local['PostLogin']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }