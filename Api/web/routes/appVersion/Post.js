'use strict';

const Joi = require("joi");
const logger = require('winston');
const AppversionCollection = require('../../../models/appversion');
const local = require("../../../locales");



const payloadValidator = Joi.object({
    appversion: Joi.string().required().description('appversion'),
    isMandatory: Joi.any().required().description('isMandatory'),
    type: Joi.any().required().description('type'),

}).options({ allowUnknown: true });

APIHandler = (req, res) => {


    let data = {
        "appversion": req.payload.appversion,
        "isMandatory": req.payload.isMandatory,
        "timeStamp": new Date().getTime(),
        "type": parseInt(req.payload.type)

    };

    AppversionCollection.Insert(data, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('Postappversion')['200'] }).code(200);
        }
    });
};
const response = {
    // status: {
    //     200: {
    //         message: Joi.any().default(local['Postappversion']['200']),
    //     },
    //     422: { message: Joi.any().default(local['Postappversion']['204']) },
    //     400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    //     500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    // }
}
module.exports = { APIHandler, payloadValidator, response }