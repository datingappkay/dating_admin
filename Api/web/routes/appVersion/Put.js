'use strict';

const Joi = require("joi");
const logger = require('winston');
const AppversionCollection = require('../../../models/appversion');
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");



const payloadValidator = Joi.object({
    _id: Joi.string().required().min(24).max(24).description('ID'),
    isMandatory: Joi.any().description('enter  isMandatory'),
    appversion: Joi.string().description('enter new appversion'),

}).options({ allowUnknown: true });

const APIHandler = (req, res) => {


    let data = {
        "appversion": req.payload.appversion,
        "isMandatory": req.payload.isMandatory,
        "timeStamp": new Date().getTime(),
        "type": parseInt(req.payload.type)

    };

    AppversionCollection.UpdateById(ObjectID(req.payload._id), data, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('Putappversion')['200'] }).code(200);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['Putappversion']['200']),
        },
        422: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }