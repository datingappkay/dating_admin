'use strict';

const Joi = require("joi");
const logger = require('winston');
const AppversionCollection = require('../../../models/appversion');
const local = require("../../../locales");
const userListCollection = require('../../../models/userList');

// const queryValidator = Joi.object({
//     type: Joi.any().required().description('type'),

// }).required();

const APIHandler = (req, res) => {

    const getAppVersion = () => {

        return new Promise((resolve, reject) => {
            let type = { type: parseInt(req.params.type) }
            //console.log("type", type, req.payload)
            AppversionCollection.Select(type, { "timeStamp": -1 }, (err, result) => {
                if (err) {
                    // return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                    reject()
                } else if (result) {
                    resolve(result)
                    // return res({ message: req.i18n.__('Getappversion')['200'], data: result }).code(200);
                } else {
                    // return res({ message: req.i18n.__('Getappversion')['204'] }).code(204);
                    resolve([])
                }
            });


        })





    }
    const getUsers = (data) => {

        new Promise((resolve, reject) => {
            let device = "1"
            if (parseInt(req.params.type) === 1) {
                device = "2"
            } else {
                device = "1"
            }
            let condition = [
                { "$match": { "currentLoggedInDevices.deviceType": device } },
                { "$group": { _id: "$currentLoggedInDevices.appVersion", count: { $sum: 1 } } }
            ]
            userListCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    // return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                } else if (result) {
                    // console.log("result", data)
                    return res({ message: req.i18n.__('Getappversion')['200'], version: data, userCount: result }).code(200)
                } else {
                    // return res({ message: req.i18n.__('Getappversion')['204'] }).code(204);
                }
            });
        })
    }

    getAppVersion()
        .then(getUsers)
        .catch((err) => {

            logger.error(err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        })


};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['Getappversion']['200']), version: Joi.any(), userCount: Joi.any()
        },
        204: { message: Joi.any().default(local['Getappversion']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, response }