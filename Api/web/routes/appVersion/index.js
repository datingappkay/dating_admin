let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PostAPI = require('./Post');
let PutAPI = require('./Put');
let DeleteAPI = require('./Delete');

module.exports = [
    {
        method: 'GET',
        path: '/appVersion/{type}',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'reportReasons'],
            description: 'This API is used to get  app version data.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },

    {
        method: 'POST',
        path: '/appVersion',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'reportReasons'],
            description: 'This API is used to insert app version data.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response

        }
    },
    {
        method: 'PUT',
        path: '/appVersion',
        handler: PutAPI.APIHandler,
        config: {
            tags: ['api', 'reportReasons'],
            description: 'This API is used to update app verdion.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response

        }
    },
    {
        method: 'Delete',
        path: '/appVersion/{appVersionId}',
        handler: DeleteAPI.APIHandler,
        config: {
            tags: ['api', 'reportReasons'],
            description: 'This API is used to delete app version.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: DeleteAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response

        }
    }


];