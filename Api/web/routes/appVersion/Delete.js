'use strict';

const Joi = require("joi");
const logger = require('winston');
const AppversionCollection = require('../../../models/appversion');
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");


const payloadValidator = Joi.object({
    appVersionId: Joi.string().required().description('appVersionId'),
}).options({ allowUnknown: true });


const APIHandler = (req, res) => {

    let data = {
        _id: ObjectID(req.params.appVersionId)
    };
    AppversionCollection.Delete(data, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('Deleteappversion')['200'] }).code(200);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['Deleteappversion']['200']),
        },
        422: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator, response }