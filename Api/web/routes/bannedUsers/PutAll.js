'use strict';
const Joi = require("joi");
const logger = require('winston');
const userListCollection = require("../../../models/userList");
const userTypeCollection = require("../../../models/userListType");
const sendMail = require("../../../library/mailgun");
const mqtt = require("../../../library/mqtt");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");


const APIHandler = (req, res) => {
    let datatoUpdate = {
        profileStatus: 1,
        banndTimeStamp: new Date().getTime(),
    }
    let array = [];
    req.payload._id.forEach(element => {
        array.push(new ObjectID(element));
        console.log("=============>",element)
        // mqtt.publish(`bannedUser/${element}`, JSON.stringify({ "message": "Hey You are banned by admin" }), { qos: 1 }, (err, result) => {
        //     userTypeCollection.Update(element, datatoUpdate, (err, result) => { logger.error((err) ? err : "") })
        // });
        userTypeCollection.Update(element, datatoUpdate, (err, result) => { if (err) logger.error(err) })
        mqtt.publish(element, JSON.stringify({ "messageType": "bannedUser", "message": "Hey You are banned by admin" }), { qos: 1 }, (e, r) => {

        });

    });

    let condition = {
        '_id': { '$in': array }
    };

    userListCollection.Update(condition, datatoUpdate, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
        return res({ message: req.i18n.__('PutBanAll')['200'] }).code(200);

    });
};

const payloadValidator = Joi.object({
    _id: Joi.array().required().description('_id'),

}).options({ allowUnknown: true });

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PutBanAll']['200']),
        },
        422: { message: Joi.any().default(local['PutBanAll']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }