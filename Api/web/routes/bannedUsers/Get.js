'use strict'
const Joi = require("joi");
const logger = require('winston');
const local  = require("../../../locales");
const userListCollection = require("../../../models/userList");

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required(); 
const APIHandler = (req, res) => {
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    let condition = [
        { "$match": { profileStatus: 1 } },
        { "$project": { "firstName": 1, "contactNumber": 1, "email": 1, "banndReson": 1, "banndTimeStamp": 1 } },
        { "$sort": { "banndTimeStamp": -1 } },
        { "$skip": offset },
        { "$limit": limit },
    ]
    userListCollection.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            return res({ message: req.i18n.__('GetAllBannUser')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetAllBannUser')['204'] }).code(204);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetAllBannUser']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetAllBannUser']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler,response,response }