'use strict';
/*  will remove thi API*/
const Joi = require("joi");
const logger = require('winston');
const userListCollection = require("../../../models/userList");
const sendMail = require("../../../library/mailgun");
const mqtt = require("../../../library/mqtt");


const APIHandler = (req, res) => {


    var data = {
        from: 'noreply@appscrip.com',
        to: req.payload.banndemail,
        subject: 'Profile banned on Sync 1 to 1',
        text: `Hello “` + req.payload.banndUserName + `”,
      
      Your profile has been banned on Sync 1 to 1. The reason is “`+ req.payload.BannduserReson + `”. Please contact our support team for any queries.
      
      Cheers,
      Team Sync 1 to 1
      `
    };
    sendMail.sendMail(data, () => { })


    let datatoUpdate = {
        profileStatus: req.payload.status,
        banndTimeStamp: new Date().getTime(),
        banndReson: req.payload.BannduserReson || "--"
    };


    let message = { data: "you are banned by admin" };
    mqtt.publish("bannedUser/" + req.payload._id, JSON.stringify(message), { qos: 2 }, () => { });

    userListCollection.UpdateById(req.payload._id, datatoUpdate, (err, result) => {
        if (err) {
            return res({ message: 'internal server error' }).code(500);
        }
        return res({ message: 'sucess' }).code(200);

    });
};

const payloadValidator = Joi.object({
    _id: Joi.string().required().min(24).max(24).description('_id'),
    status: Joi.number().required().description('status true or false'),
    BannduserReson: Joi.string().description('BannduserReson'),
    banndUserName: Joi.string().required().description('banndUserName'),
    banndemail: Joi.string().required().description('banndemail'),

}).options({ allowUnknown: true });


module.exports = { APIHandler, payloadValidator }