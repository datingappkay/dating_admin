const Joi = require("joi");
const logger = require('winston');
const preferenceTable = require('../../../models/preferenceTable');
const ObjectID = require('mongodb').ObjectID;


payloadValidator = Joi.object({
    prefId: Joi.string().required().description('enter user ids'),
}).options({ allowUnknown: true });

APIHandler = (req, res) => {
    let data = {
        _id: ObjectID(req.params.prefId)
    };

    preferenceTable.Delete(data, (err, result) => {
        if (err) {
            return res({ message: 'internal server error' }).code(500);
        } else {
            return res({ code: 200, message: 'success' }).code(200);
        }
    });
};



module.exports = { APIHandler, payloadValidator }