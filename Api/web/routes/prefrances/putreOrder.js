'use strict';
const Joi = require("joi");
const logger = require('winston');
const preferenceTable = require('../../../models/preferenceTable');
const local = require("../../../locales");
const searchPreferencesCollection = require("../../../models/searchPreferences");

const payloadValidator = Joi.object({
    currId: Joi.number().required().description('type'),
    otherId: Joi.number().description('Unit'),
    type: Joi.number().description('type')
}).options({ allowUnknown: true });
const APIHandler = (req, res) => {
    console.log("==========", req.payload)

    switch (req.payload.type) {

        case 1: {
            preferenceTable.Reorder(req.payload.currId, req.payload.otherId, (err, result) => {
                if (err) {
                    return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                } else {
                    return res({ message: req.i18n.__('PutPrefrances')['200'] }).code(200);
                }

            })
            break;
        }
        case 2: {
            searchPreferencesCollection.Reorder(req.payload.currId, req.payload.otherId, (err, result) => {
                if (err) {
                    return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                } else {
                    return res({ message: req.i18n.__('PutPrefrances')['200'] }).code(200);
                }

            })
            break;
        }
        default: {
            console.log("wrrong condition");
            break;
        }
    }


};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['PutPrefrances']['200']),
        },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator, response }