
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PostAPI = require("./Post");
let DeleteAPI = require("./Delete");
let PUTAPI = require('./put');
let ReOrderAPI = require('./putreOrder')
module.exports = [
    {
        method: 'GET',
        path: '/preferences',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'prefrances'],
            description: 'This API is used to get an user prefrances.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response,

        }
    },
    {
        method: 'POST',
        path: '/preference',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'prefrances'],
            description: 'This API is used to post an prefrances.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response,

        }
    },
    {
        method: 'Delete',
        path: '/preference/{prefId}',
        handler: DeleteAPI.APIHandler,
        config: {
            tags: ['api', 'preference'],
            description: 'This API is used to delete preference.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: DeleteAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response

        }
    },
    {
        method: 'PUT',
        path: '/preference',
        handler: PUTAPI.APIHandler,
        config: {
            tags: ['api', 'preference'],
            description: 'This API is used to update user preference.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PUTAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PUTAPI.response
        }
    },
    {
        method: 'PUT',
        path: '/reorderpreference',
        handler: ReOrderAPI.APIHandler,
        config: {
            tags: ['api', 'preference'],
            description: 'This API is used to reorder user preference.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: ReOrderAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: ReOrderAPI.response
        }
    },
];