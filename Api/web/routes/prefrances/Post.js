'use strict';

const Joi = require("joi");
const logger = require('winston');
const preferenceTable = require('../../../models/preferenceTable');
const local = require("../../../locales");


const payloadValidator = Joi.object({
    type: Joi.any().required().description('type'),
    Unit: Joi.string().description('Unit'),

}).options({ allowUnknown: true });
const APIHandler = (req, res) => {
    // console.log("============>", req.payload)
    var unit = [];
    const unitObj = {
        "ftcm": ["ft", "cm"],
        "kmmi": ["km", "mi"],
        "ft": ["ft"],
        "cm": ["cm"],
        "km": ["km"],
        "mi": ["mi"],
        "year": ["year"],
        "kg": ["kg"],
        "pound": ["pound"],
        "years": ["years"]

    }
    for (const k in unitObj) {
        if (k == req.payload.unit) {
            unit = unitObj[k];
        }

    }

    let condition = [
        {
            "$group": {
                "_id": null,
                "maxProirity": { "$max": "$Priority" }
            }
        }
    ]
    var title = {};
    var values = {};
    var lable = {};
    req.payload.otherName.forEach(e => {

        // values[e.langCode] = e.values.split(',').map(Number);
        values[e.langCode] = (req.payload.type != "1") ? e.values.split(',').map(Number) : e.values.split(',');

        lable[e.langCode] = e.fieldName;
        title[e.langCode] = e.addtitle;

    });


    let data =
    {
        PreferenceTitle: req.payload.titileLable,
        otherPreferenceTitle: title,
        TypeOfPreference: req.payload.type,
        OptionsValue: (req.payload.type != "1") ? req.payload.values.split(',').map(Number) : req.payload.values.split(','),
        otherOptionsValue: values,
        optionsUnits: unit,
        forAdminUnit: req.payload.unit,
        mandatory: req.payload.isMandatory,
        status: 1,
        timestamp: new Date().getTime(),
    }
    if (parseInt(req.payload.type) == 1) {

        data.optionsUnits = [],
            delete data.forAdminUnit

    }


    //console.log("==datadatadatadatadata=======>", data)

    preferenceTable.Aggregate(condition, (err, result) => {

        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
        console.log("============", result)
        req.payload["Priority"] = (result.length) ? result[0]["maxProirity"] + 1 : 1;
        data.Priority = req.payload.Priority
        //console.log("===========>", data)
        preferenceTable.Insert(data, (err, result) => {
            if (err) {
                return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
            } else {

                return res({ message: req.i18n.__('PostPrefrances')['200'] }).code(200);
            }

        })
    })


};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostPrefrances']['200']),
        },
        204: { message: Joi.any().default(local['PostPrefrances']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator, response }