'use strict';

const Joi = require("joi");
const logger = require('winston');
const preferenceTable = require('../../../models/preferenceTable');
const local = require("../../../locales");


const payloadValidator = Joi.object({
    //newunit: Joi.any().required().description('type'),
    newtype: Joi.any().description('1,2,3'),

}).options({ allowUnknown: true });
const APIHandler = (req, res) => {
    var unit
     //console.log("==================>", req.payload,req.payload.newvalues)
    const unitObj = {
        "ftcm": ["ft", "cm"],
        "kmmi": ["km", "mi"],
        "ft": ["ft"],
        "cm": ["cm"],
        "km": ["km"],
        "mi": ["mi"],
        "year": ["year"],
        "kg": ["kg"],
        "pound": ["pound"],
        "years": ["years"]
    }

    for (const k in unitObj) {
        if (k == req.payload.newunit) {
            unit = unitObj[k];
        }

    }
    let condition = [
        {
            "$group": {
                "_id": null,
                "maxProirity": { "$max": "$priority" }
            }
        }
    ]
    var title = {};
    var values = {};
    var lable = {};
    req.payload.otherName.forEach(e => {

        //values[e.langCode] = e.values.split(',').map(Number);
        values[e.langCode] = (typeof(req.payload.newvalues) == "string") ? e.values.split(',').map(String) : e.values.split(',');

        lable[e.langCode] = e.fieldName;
        title[e.langCode] = e.editTitle;

    });



    if (req.payload.newtype == 1) {

        var data =
        {
            PreferenceTitle: req.payload.newtitileLable,
            otherPreferenceTitle: title,
            TypeOfPreference: req.payload.newtype,
            // OptionsValue: req.payload.newvalues.map(Number),
            OptionsValue: (typeof(req.payload.newvalues) == "string") ? req.payload.newvalues.split(',').map(String) : req.payload.newvalues,
            otherOptionsValue: values,
            optionsUnits: [],
            forAdminUnit: "",
            mandatory: req.payload.newisMandatory,
            status: 1,
            Priority: req.payload.Priority,
            timestamp: new Date().getTime(),
        }
    } else {
        var data =
        {
            PreferenceTitle: req.payload.newtitileLable,
            otherPreferenceTitle: title,
            TypeOfPreference: req.payload.newtype,
            OptionsValue: (typeof(req.payload.newvalues) == "string") ? req.payload.newvalues.split(',').map(String) : req.payload.newvalues,
            otherOptionsValue: values,
            optionsUnits: unit,
            forAdminUnit: req.payload.newunit,
            mandatory: req.payload.newisMandatory,
            status: 1,
            Priority: req.payload.Priority,
            timestamp: new Date().getTime(),
        }
    }


    //  console.log("==datadatadatadatadata=======>", data)

    preferenceTable.UpdateById(req.payload.filterId, data, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('PutPrefrances')['200'] }).code(200);
        }

    })



};


const response = {
    // status: {
    //     200: {
    //         message: Joi.any().default(local['PutPrefrances']['200']),
    //     },
    //     400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    //     500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    // }
}
module.exports = { APIHandler, payloadValidator, response }