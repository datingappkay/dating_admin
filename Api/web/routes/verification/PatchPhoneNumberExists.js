'use strict';

const Joi = require("joi");
const async = require("async");
const Cryptr = require('cryptr');
const logger = require('winston');
const Promise = require('promise');
const Auth = require("../../middleware/authentication.js");
const userListCollection = require('../../../models/userList');
const local  = require("../../../locales");

let payloadValidator = Joi.object({
    phoneNumber: Joi.string().required().description("enter phoneNumber with country code").example("+919620826142").error(new Error('phoneNumber is missing')),
}).required();

/**
 * @method PATCH verificationPhoneNumberExists
 * @description This API use to PATCH verificationPhoneNumberExists.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} phoneNumber enter phoneNumber with country code
 
 * @returns  200 : Mobile number already exist in the database..
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : Mobile number doesnot exist in the database.
 * @returns  500 : An unknown error has occurred.
 */
let APIHandler = (req, res) => {

    let phoneNumber = req.payload.phoneNumber;
    let condition = { contactNumber: phoneNumber };

    userListCollection.Select(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result[0]) {
            return res({ message:req.i18n.__('PatchPhoneNumberExists')['200']}).code(200);
        } else {
            return res({ message:req.i18n.__('PatchPhoneNumberExists')['422'] }).code(422);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PatchPhoneNumberExists']['200']), 
        },
        422: { message: Joi.any().default(local['PatchPhoneNumberExists']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator,response }