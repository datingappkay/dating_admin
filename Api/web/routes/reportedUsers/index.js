
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let GetByIdAPI = require('./GetById');

module.exports = [
    {
        method: 'GET',
        path: '/reportedUsers',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'reported'],
            description: 'This API is used to login an user in the app.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response,

        }
    },
    {
        method: 'GET',
        path: '/reportedUsers/{userId}',
        handler: GetByIdAPI.APIHandler,
        config: {
            tags: ['api', 'reported'],
            description: 'This API is used to login an user in the app.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByIdAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByIdAPI.response,

        }
    }
];