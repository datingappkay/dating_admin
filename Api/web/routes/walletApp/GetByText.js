"use strict";
const Joi = require("joi");
const logger = require('winston');
const walletApp = require("../../../models/walletApp");
const local = require("../../../locales");

const payloadValidator = Joi.object({
    text: Joi.string().description('search walletApp By txnType,paymentType.initatedBy'),
    dateFrom: Joi.number().description('Enter From Date(milliseconds) For Ex.946684800000'),
    dateTo: Joi.number().description('Enter To Date(milliseconds) for Ex. 33765897600000'),
    txnType: Joi.string().required().allow(["All", "DEBIT", "CREDIT"]).description('Configuration change txnType'),
    // offset: Joi.number().description("offset"),
    // limit: Joi.number().description("limit"),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    // const limit = parseInt(req.query.limit) || 20;
    // const offset = parseInt(req.query.offset) || 0;

    var user = req.query.text;
    let condition = [];


    if (req.query.dateFrom) {
        condition.push({ "$match": { "timestamp": { '$gte': req.query.dateFrom } } });

    }
    if (req.query.dateTo) {
        condition.push({ "$match": { "timestamp": { '$lte': req.query.dateTo } } });
    }
    /*
  add condition for txnType
  */
    switch (req.query.txnType) {
        case "All": {
            //no add any condition 
            break;
        }
        case "CREDIT": {
            condition.push({ "$match": { "txnType": "CREDIT" } });
            break;
        }
        case "DEBIT": {
            condition.push({ "$match": { "txnType": "DEBIT" } });
            break;
        }

    }

    if (user) {
        condition.push({
            "$match": {
                "$or": [
                    { "txnType": new RegExp(user, "i") },
                    { "paymentType": new RegExp(user, "i") },
                    { "initatedBy": new RegExp(user, "i") },

                ]
            }
        })
    }
    
       // condition.push({ "$skip": offset }, { "$limit": limit });
    

    console.log("======for search>>>", JSON.stringify(condition));



    walletApp.Aggregate(condition, (err, result) => {
        if (err) {
            logger.silly(err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            walletApp.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('GetWalletAppTxt')['200'], data: result, totalCount: ress }).code(200);
            });
        } else {
            return res({ message: req.i18n.__('GetWalletAppTxt')['204'] }).code(204);
        }
    });
}
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetWalletAppTxt']['200']), data: Joi.any(),totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['GetWalletAppTxt']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }