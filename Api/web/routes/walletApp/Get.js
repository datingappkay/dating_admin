'use strict'
const Joi = require("joi");
const logger = require('winston');
const walletApp = require("../../../models/walletApp");
const local = require("../../../locales");

const payloadValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;



    walletApp.selectWithProject({}, { "_id": -1 }, offset * limit, limit, {}, (err, result) => {
        if (err) {
            logger.silly(err);

            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            result.forEach(e => {
                e.opeingBalance = parseFloat(e.opeingBalance).toFixed(2)
                e.closingBalance = parseFloat(e.closingBalance).toFixed(2)

            });
            walletApp.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('GetWalletApp')['200'], data: result, totalCount: ress }).code(200);
            });


        } else {
            return res({ message: req.i18n.__('GetWalletApp')['204'] }).code(204);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetWalletApp']['200']), data: Joi.any(), totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['GetWalletApp']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, payloadValidator }