"use strict";
const Joi = require("joi");
const logger = require('winston');
const UserBoost = require("../../../models/userBoost");
const local = require("../../../locales");
const moment = require('moment');

const payloadValidator = Joi.object({
    text: Joi.string().required().description('enter You want to search by subscriptionUserName or planName '),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),

}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    let date = parseInt(req.query.dateTo)
    let dateTo = moment(date).add(24, 'hours').valueOf()
    const limit = parseInt(req.query.limit) || 10;
    const offset = parseInt(req.query.offset) || 0;


    let condition = [

        {
            "$project": {
                isActive: 1,
                start: 1,
                expire: 1,
                userId: 1,
                likes: "$count.likes",
                disLikes: "$count.disLikes",
                supperLikes: "$count.supperLikes",
                match: "$count.match",
                unmatch: "$count.unmatch",
                views: "$count.views"

            }
        },
        { "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "Data" } },
        { "$unwind": "$Data" },
        {
            "$project": {
                boostUserName: "$Data.firstName",
                isActive: 1,
                start: 1,
                expire: 1,
                userId: 1,
                likes: 1,
                disLikes: 1,
                supperLikes: 1,
                match: 1,
                unmatch: 1,
                views: 1
            }

        }, {
            "$sort": {
                start: -1
            }
        },
        { "$skip": offset * limit }, { "$limit": limit }

    ]
    if (req.query.dateFrom) {
        condition.push({ "$match": { "start": { "$gte": parseInt(req.query.dateFrom) } } });

    }
    /*
   add condition for to Date
   */
    if (dateTo) {
        condition.push({ "$match": { "expire": { "$lte": dateTo } } });
    }
    /*
   add condition for text search
   */

    if (req.query.text != "") {
        condition.push(

            {
                "$match": {
                    "$or": [
                        { "boostUserName": new RegExp(req.query.text, 'i') }


                    ]
                }

            },
            { "$skip": offset * limit }, { "$limit": limit }
        );


    }

    else {
        condition.push({ "$skip": offset * limit }, { "$limit": limit });
    }

    switch (req.query.boostType) {
        case "All": {
            //no add any condition 
            break;
        }
        case "EXPIRE": {
            condition.push({ "$match": { "isActive": 0 } });
            break;
        }
        case "ACTIVE": {
            condition.push({ "$match": { "isActive": 1 } });
            break;
        }

    }


    //  console.log("======for search>>>", condition);


    UserBoost.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            UserBoost.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('UserBoostGet')['200'], data: result, totalCount: ress }).code(200);
            });

        } else {
            return res({ message: req.i18n.__('UserBoostGet')['204'] }).code(204);
        }
    });
}
const response = {
    status: {
        200: {
            message: Joi.any().default(local['UserBoostGet']['200']), data: Joi.any(), totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['UserBoostGet']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }