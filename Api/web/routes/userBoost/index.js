
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
//let GetByText = require('./GetByText');


module.exports = [

    {
        method: 'GET',
        path: '/userBoost',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'userBoost'],
            description: 'This API is used to Get User Boost.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },




];