'use strict';

const Joi = require("joi");
const logger = require('winston');
const userMatch = require("../../../models/userMatch");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");

const APIHandler = (req, res) => {



    let userId = req.params._id;
    let condition = [
        {
            "$match": {
                "$or": [
                    { "initiatedBy": ObjectID(userId) },
                    { "opponentId": ObjectID(userId) }
                ],
                "unMatchedTimestamp": { "$exists": false }
            }
        },
        {
            "$project": {
                "MatchedUserId": {
                    "$cond": {
                        "if": { "$eq": ["$initiatedBy", ObjectID(userId)] },
                        "then": "$opponentId", "else": "$initiatedBy"
                    }
                }, "initiatedBy": 1, "opponentId": 1, "createdTimestamp": 1
            }
        },
        { "$lookup": { "from": "userList", "localField": "MatchedUserId", "foreignField": "_id", "as": "MatchUserData" } },
        { "$lookup": { "from": "userList", "localField": "initiatedBy", "foreignField": "_id", "as": "FirstLikedByData" } },
        { "$lookup": { "from": "userList", "localField": "opponentId", "foreignField": "_id", "as": "SecondLikedByData" } },
        { "$unwind": "$MatchUserData" },
        { "$unwind": "$FirstLikedByData" },
        { "$unwind": "$SecondLikedByData" },
        {
            "$project": {
                "_id": 0,
                "MatcheUser": "$MatchUserData.firstName",
                "PhoneNo": "$MatchUserData.contactNumber",
                "Email": "$MatchUserData.email",
                "dob": "$MatchUserData.dob",
                "profilePic": "$MatchUserData.profilePic",
                "gender": "$MatchUserData.gender",
                "height": "$MatchUserData.height",
                "profileVideo": "$MatchUserData.profileVideo",
                "heightInFeet": "$MatchUserData.heightInFeet",
                "city": "$MatchUserData.address.city",
                "country": "$MatchUserData.address.country",
                "otherImages": "$MatchUserData.otherImages",
                "myPreferences": "$MatchUserData.myPreferences",
                "about": "$MatchUserData.about",
                "FirstLikedBy": "$FirstLikedByData.firstName",
                "SecondLikedBy": "$SecondLikedByData.firstName",
                "MatcedOn": "$createdTimestamp",
                "about": "$MatchUserData.about"

            }
        }
    ];


    userMatch.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
        result.forEach(element => {
            if (element.Email !== null && element.Email !== undefined && element.Email !== '') {
                var maskid = "";
                var myemailId = element.Email;
                var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                var postfix = myemailId.substring(myemailId.lastIndexOf("@"));
                for (var i = 0; i < prefix.length; i++) {
                    if (i == 0 || i == prefix.length - 1) {   ////////
                        maskid = maskid + prefix[i].toString();
                    }
                    else {
                        maskid = maskid + "*";
                    }
                }
                maskid = maskid + postfix;
                element.Email = maskid;
            }
            if (element.PhoneNo !== null && element.PhoneNo !== undefined && element.PhoneNo !== '') {
                var maskid = "";
                var myemailId = element.PhoneNo;
                var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                for (var i = 0; i < prefix.length; i++) {
                    if (i == 0 || i == prefix.length - 1) {   ////////
                        maskid = maskid + prefix[i].toString();
                    }
                    else {
                        maskid = maskid + "*";
                    }
                }
                maskid = maskid + postfix;
                element.PhoneNo = maskid;
            }

        });
        return res({ message: req.i18n.__('GetMatchById')['200'], data: result }).code(200);
    });
};

const payloadValidator = Joi.object({
    _id: Joi.string().required().min(24).max(24).description('iserId'),
}).options({ allowUnknown: true });


const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetMatchById']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['GetMatchById']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }