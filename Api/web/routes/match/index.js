
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let GetByIdAPI = require('./GetById');
let MostMatchAPI = require('./mostMatch');

module.exports = [
    {
        method: 'GET',
        path: '/match',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'match'],
            description: 'This API is used to Gte All Match.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },
    {
        method: 'GET',
        path: '/match/{_id}',
        handler: GetByIdAPI.APIHandler,
        config: {
            tags: ['api', 'match'],
            description: 'This API is used to Get Match By Id.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByIdAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByIdAPI.response

        }
    },  {
        method: 'GET',
        path: '/mostMatch',
        handler: MostMatchAPI.APIHandler,
        config: {
            tags: ['api', 'match'],
            description: 'This API is used to get most match user.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: MostMatchAPI.response

        }
    }
    
];