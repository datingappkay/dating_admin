const Joi = require("joi");
const logger = require('winston');
const userMatchCollection = require("../../../models/userMatch");


APIHandler = (req, res) => {
    let condition = [
      {  "$group":
        {
            "_id": "$initiatedBy",
            "totalMatch": { "$sum": 1 },
            "timestamp": { "$last": "$createdTimestamp" }
        }
    },
        { "$sort": { "totalMatch": -1 } },
        { "$lookup": { "from": "userList", "localField": "_id", "foreignField": "_id", "as": "matchUser" } },
        { "$unwind": "$matchUser" },
        {
            "$project": {
                "firstName": "$matchUser.firstName", "gender": "$matchUser.gender", "registerd": "$matchUser.registeredTimestamp", "totalMatch": 1, "timestamp": 1, "profileVideo": "matchUser.profileVideo", "city": "$matchUser.address.city", "country": "$matchUser.address.country",
                "contactNumber": "$matchUser.contactNumber", "email": "$matchUser.email", "profilePic": "$matchUser.profilePic", "profileVideo": "$matchUser.profileVideo", "dob": "$matchUser.dob", "height": "$matchUser.height", "matchUser.country": 1, "lastLogin": "$matchUser.lastLogin"
            }
        }
    ];

    userMatchCollection.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: 'internal server error' }).code(500);
        } else if (result) {
            return res({  message: 'success', data: result }).code(200);
        } else {
            return res({ message: 'incorrect password' }).code(204);
        }
    });
};

module.exports = { APIHandler }