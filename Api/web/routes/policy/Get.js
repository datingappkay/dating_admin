'use strict';
const Joi = require("joi");

var fs = require('fs');
const local  = require("../../../locales");

const APIHandler = (req, res) => {
    fs.readFile("/var/www/html/datum_2.0-admin/policy.html", "utf8", function (err, data) {
        if (err) {
            return console.log(err);
        }
        return res({ message:  req.i18n.__('GetPolicy')['200'], data: data }).code(200);
    });

};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetPolicy']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler,response }