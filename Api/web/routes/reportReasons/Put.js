'use strict';

const Joi = require("joi");
const logger = require('winston');
const reportReasonsCollection = require('../../../models/reportReasons');
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");



const payloadValidator = Joi.object({
    reasonId: Joi.string().required().min(24).max(24).description('reasonId'),
    reportReason: Joi.string().required().description('reportReason'),
    reason: Joi.array().required().description('reason'),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {


    var reasons = {}
    let data = {
        "reportReason": req.payload.reportReason,

    };
    console.log("dataaaaaaaaaaa", req.payload.reason.length );
    if (req.payload.reason.length  > 0) {


        req.payload.reason.forEach(e => {
            reasons[e.lanuageCode] = e.languageName

        });
        data.reason = reasons;
    }
    console.log("dataaaaaaaaaaa", data)
    reportReasonsCollection.UpdateById(ObjectID(req.payload.reasonId), data, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('PutReportReasonse')['200'] }).code(200);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['PutReportReasonse']['200']),
        },
        422: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }