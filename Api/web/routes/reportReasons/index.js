let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PostAPI = require('./Post');
let PutAPI = require('./Put');
let DeleteAPI=require('./Delete');

module.exports = [
    {
        method: 'GET',
        path: '/reportReasons',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'reportReasons'],
            description: 'This API is used to login an user in the app.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },
   
    {
        method: 'POST',
        path: '/reportReasons',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'reportReasons'],
            description: 'This API is used to login an user in the app.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response

        }
    },
    {
        method: 'PUT',
        path: '/reportReasons',
        handler: PutAPI.APIHandler,
        config: {
            tags: ['api', 'reportReasons'],
            description: 'This API is used to login an user in the app.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response

        }
    },
    {
        method: 'Delete',
        path: '/reportReasons/{reasonId}',
        handler: DeleteAPI.APIHandler,
        config: {
            tags: ['api', 'reportReasons'],
            description: 'This API is used to login an user in the app.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: DeleteAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response

        }
    }


];