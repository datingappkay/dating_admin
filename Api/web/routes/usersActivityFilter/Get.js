'use strict';
const Joi = require("joi");
const logger = require('winston');
var userListCollection = require("../../../models/userUnlikes");
const local = require("../../../locales");
const moment = require('moment');


const payloadValidator = Joi.object({
    dateFrom: Joi.string().default().description('Enter From Date(milliseconds) For Ex.1462060800000'),
    dateTo: Joi.string().default().description('Enter To Date(milliseconds) for Ex. 1556668800000'),
    userType: Joi.string().required().default().allow(["1", "2", "3"]).description('Configuration change userType'),
    text: Joi.string().description('enter You want to search'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).options({ allowUnknown: true });


const APIHandler = (req, res) => {
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    // req.query.dateFrom = parseInt(req.query.dateFrom) || 946684800000;
    // req.query.dateTo = parseInt(req.query.dateTo) || 33765897600000;
    let condition = []
    let date = parseInt(req.query.dateTo)
    let dateTo = moment(date).add(24, 'hours').valueOf()
    /*
      add condition for Liked to set userType = 1
   */
    switch (req.query.userType) {


        case "1": {
            userListCollection = require("../../../models/userLikes");
            if (req.query.dateFrom) {
                condition.push({ "$match": { "timestamp": { '$gte': parseInt(req.query.dateFrom) } } });

            }
            if (dateTo) {
                condition.push({ "$match": { "timestamp": { '$lte': dateTo } } });
            }
            condition.push(
                // {
                //     '$match': {
                //         "timestamp": {
                //             '$gte': parseInt(req.query.dateFrom),
                //             '$lte': parseInt(req.query.dateTo)
                //         },
                //     },

                // },
                { '$lookup': { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "userData" } },
                { "$lookup": { "from": "userList", "localField": "targetUserId", "foreignField": "_id", "as": "targetUserData" } },
                { "$unwind": "$userData" },
                { "$match": { "userData.profileStatus": { "$ne": 1 } } },
                { "$unwind": "$targetUserData" },
                {
                    "$project": {
                        "likedBy": "$userData.firstName",
                        "likedByMyPreferences": "$userData.myPreferences",
                        "likedByEmail": "$userData.email",
                        "likedByContactNumber": "$userData.contactNumber",
                        "likedBydob": "$userData.dob",
                        "likedBygender": "$userData.gender",
                        "likedByheight": "$userData.height",
                        "likedBycity": "$userData.address.city",
                        "likedBycountry": "$userData.address.country",
                        "likedByprofilePic": "$userData.profilePic",
                        "likedByprofileVideo": "$userData.profileVideo",
                        "likedByabout": "$userData.about",
                        "likeByOtherImages": "$userData.otherImages",
                        "opponentUser": "$targetUserData.firstName",
                        "opponentUserEmail": "$targetUserData.email",
                        "opponentUserContactNumber": "$targetUserData.contactNumber",
                        "opponentUserdob": "$targetUserData.dob",
                        "opponentUsergender": "$targetUserData.gender",
                        "opponentUserheight": "$targetUserData.height",
                        "opponentUsercity": "$targetUserData.address.city",
                        "opponentUserOtherImages": "$targetUserData.otherImages",
                        "opponentUsercountry": "$targetUserData.address.country",
                        "opponentUserabout": "$targetUserData.about",
                        "opponentUserprofilePic": "$targetUserData.profilePic",
                        "opponentUserprofileVideo": "$targetUserData.profileVideo",
                        "opponentUserMyPreferences": "$targetUserData.myPreferences",

                        "likedOn": "$timestamp"
                    }
                },
                { "$sort": { "_id": -1 } }
            );
            break;
        }
        /*
      add condition for UnLike to set userType = 2
   */
        case "2": {
            userListCollection = require("../../../models/userUnlikes");
            if (req.query.dateFrom) {
                condition.push({ "$match": { "timestamp": { '$gte': parseInt(req.query.dateFrom) } } });

            }
            if (dateTo) {
                condition.push({ "$match": { "timestamp": { '$lte': dateTo } } });
            }
            condition.push(
                // {
                //     "$match": {
                //         "timestamp": {
                //             '$gte': parseInt(req.query.dateFrom),
                //             '$lte': parseInt(req.query.dateTo)
                //         },
                //     },

                // },
                { "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                { "$lookup": { "from": "userList", "localField": "targetUserId", "foreignField": "_id", "as": "targetUserData" } },
                { "$unwind": "$targetUserData" },
                { "$match": { "userData.profileStatus": { "$ne": 1 } } },
                {
                    "$project": {
                        "unLikedBy": "$userData.firstName",
                        "unLikedByMyPreferences": "$userData.myPreferences",
                        "unLikedByEmail": "$userData.email",
                        "unLikedByContactNumber": "$userData.contactNumber",
                        "unlikedBydob": "$userData.dob",
                        "unlikedBygender": "$userData.gender",
                        "unlikedByheight": "$userData.height",
                        "unlikedByabout": "$userData.about",
                        "unlikedByOtherImages": "$userData.otherImages",
                        "unlikedBycity": "$userData.address.city",
                        "unlikeByprofileVideo": "$userData.profileVideo",
                        "unlikedBycountry": "$userData.address.country",
                        "unlikedByprofilePic": "$userData.profilePic",
                        "unLikedOpponentUser": "$targetUserData.firstName",
                        "unLikedOpponentUserEmail": "$targetUserData.email",
                        "unLikedOpponentUserContactNumber": "$targetUserData.contactNumber",
                        "unLikedOpponentUserdob": "$targetUserData.dob",
                        "unLikedOpponentUsergender": "$targetUserData.gender",
                        "unLikedOpponentUserOtherImages": "$targetUserData.otherImages",
                        "unLikedOpponentUserheight": "$targetUserData.height",
                        "unLikedOpponentUsercity": "$targetUserData.address.city",
                        "unLikedOpponentUserabout": "$targetUserData.about",
                        "unLikedOpponentUserprofileVideo": "$targetUserData.profileVideo",
                        "unLikedOpponentUsercountry": "$targetUserData.address.country",
                        "unLikedOpponentUserprofilePic": "$targetUserData.profilePic",
                        "unLikedOpponentUserMyPreferences": "$targetUserData.myPreferences",

                        "unLikedOn": "$timestamp"
                    }
                },
                { "$sort": { "_id": -1 } });
            break;
        }
        /*
      add condition for SupperLike to set userType = 3
   */
        case "3": {
            userListCollection = require("../../../models/supperLikes");
            if (req.query.dateFrom) {
                condition.push({ "$match": { "timestamp": { '$gte': parseInt(req.query.dateFrom) } } });

            }
            if (dateTo) {
                condition.push({ "$match": { "timestamp": { '$lte': dateTo } } });
            }
            condition.push(
                // {
                //     "$match": {
                //         "timestamp": {
                //             '$gte': parseInt(req.query.dateFrom),
                //             '$lte': parseInt(req.query.dateTo)
                //         },
                //     },

                // },
                { "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                { "$lookup": { "from": "userList", "localField": "targetUserId", "foreignField": "_id", "as": "targetUserData" } },
                { "$unwind": "$targetUserData" },
                { "$match": { "userData.profileStatus": { "$ne": 1 } } },
                {
                    "$project": {
                        "superLikedBy": "$userData.firstName",
                        "superLikedByEmail": "$userData.email",
                        "superLikedByMyPreferences": "$userData.myPreferences",
                        "superLikedByContactNumber": "$userData.contactNumber",
                        "superlikedBydob": "$userData.dob",
                        "superlikedByabout": "$userData.about",
                        "superlikedBygender": "$userData.gender",
                        "superlikedByheight": "$userData.height",
                        "superlikedByOtherImages": "$userData.otherImages",
                        "superlikedBycity": "$userData.address.city",
                        "superlikedByprofileVideo": "$userData.profileVideo",
                        "superlikedBycountry": "$userData.address.country",
                        "superlikedByprofilePic": "$userData.profilePic",
                        "superLikedOpponentUser": "$targetUserData.firstName",
                        "superLikedOpponentUserEmail": "$targetUserData.email",
                        "superLikedOpponentUserContactNumber": "$targetUserData.contactNumber",
                        "superLikedOpponentUserdob": "$targetUserData.dob",
                        "superLikedOpponentUserabout": "$targetUserData.about",
                        "superLikedOpponentUsergender": "$targetUserData.gender",
                        "superLikedOpponentUserOtherImages": "$targetUserData.otherImages",
                        "superLikedOpponentUserheight": "$targetUserData.height",
                        "superLikedOpponentUsercity": "$targetUserData.address.city",
                        "superLikedOpponentUsercountry": "$targetUserData.address.country",
                        "superLikedOpponentUserprofileVideo": "$targetUserData.profileVideo",
                        "superLikedOpponentUserprofilePic": "$targetUserData.profilePic",
                        "superLikedOpponentUserMyPreferences": "$targetUserData.myPreferences",
                        "superLikedOn": "$timestamp"
                    }
                },
                { "$sort": { "_id": -1 } });
            break;
        }
    }
    if (req.query.text != "") {
        condition.push({
            "$match": {
                "$or": [
                    { "likedBy": new RegExp(req.query.text, 'i') },
                    { "likedByEmail": new RegExp(req.query.text, 'i') },
                    { "likedByContactNumber": new RegExp(req.query.text, 'i') },
                    { "opponentUser": new RegExp(req.query.text, 'i') },
                    { "opponentUserEmail": new RegExp(req.query.text, 'i') },
                    { "opponentUserContactNumber": new RegExp(req.query.text, 'i') },
                    { "unLikedBy": new RegExp(req.query.text, 'i') },
                    { "unLikedByEmail": new RegExp(req.query.text, 'i') },
                    { "unLikedByContactNumber": new RegExp(req.query.text, 'i') },
                    { "unLikedOpponentUser": new RegExp(req.query.text, 'i') },
                    { "unLikedOpponentUserEmail": new RegExp(req.query.text, 'i') },
                    { "unLikedOpponentUserContactNumber": new RegExp(req.query.text, 'i') },
                    { "superLikedBy": new RegExp(req.query.text, 'i') },
                    { "superLikedByEmail": new RegExp(req.query.text, 'i') },
                    { "superLikedByContactNumber": new RegExp(req.query.text, 'i') },
                    { "superLikedOpponentUser": new RegExp(req.query.text, 'i') },
                    { "superLikedOpponentUserEmail": new RegExp(req.query.text, 'i') },
                    { "superLikedOpponentUserContactNumber": new RegExp(req.query.text, 'i') },
                ]
            }
        },
            { "$skip": offset * limit }, { "$limit": limit }
        );


    }
    else {
        condition.push({ "$skip": offset * limit }, { "$limit": limit });
    }


    //  console.log("Condirion----------------->>>>>>>>", JSON.stringify(condition));
    userListCollection.Aggregate(condition, (err, result) => {
        if (err) {
            console.log("===>>>", err)
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            result.forEach(element => {
                if (element.likedByEmail !== null && element.likedByEmail !== undefined && element.likedByEmail !== '') {
                    var maskid = "";
                    var myemailId = element.likedByEmail;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                    var postfix = myemailId.substring(myemailId.lastIndexOf("@"));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.likedByEmail = maskid;
                }
                if (element.opponentUserEmail !== null && element.opponentUserEmail !== undefined && element.opponentUserEmail !== '') {
                    var maskid = "";
                    var myemailId = element.opponentUserEmail;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                    var postfix = myemailId.substring(myemailId.lastIndexOf("@"));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.opponentUserEmail = maskid;
                }
                if (element.likedByContactNumber !== null && element.likedByContactNumber !== undefined && element.likedByContactNumber !== '') {
                    var maskid = "";
                    var myemailId = element.likedByContactNumber;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                    var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.likedByContactNumber = maskid;
                }
                if (element.opponentUserContactNumber !== null && element.opponentUserContactNumber !== undefined && element.opponentUserContactNumber !== '') {
                    var maskid = "";
                    var myemailId = element.opponentUserContactNumber;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                    var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.opponentUserContactNumber = maskid;
                }


                if (element.superLikedByEmail !== null && element.superLikedByEmail !== undefined && element.superLikedByEmail !== '') {
                    var maskid = "";
                    var myemailId = element.superLikedByEmail;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                    var postfix = myemailId.substring(myemailId.lastIndexOf("@"));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.superLikedByEmail = maskid;
                }
                if (element.unLikedOpponentUserEmail !== null && element.unLikedOpponentUserEmail !== undefined && element.unLikedOpponentUserEmail !== '') {
                    var maskid = "";
                    var myemailId = element.unLikedOpponentUserEmail;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                    var postfix = myemailId.substring(myemailId.lastIndexOf("@"));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.unLikedOpponentUserEmail = maskid;
                }
                if (element.unLikedByContactNumber !== null && element.unLikedByContactNumber !== undefined && element.unLikedByContactNumber !== '') {
                    var maskid = "";
                    var myemailId = element.unLikedByContactNumber;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                    var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.unLikedByContactNumber = maskid;
                }
                if (element.unLikedOpponentUserContactNumber !== null && element.unLikedOpponentUserContactNumber !== undefined && element.unLikedOpponentUserContactNumber !== '') {
                    var maskid = "";
                    var myemailId = element.unLikedOpponentUserContactNumber;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                    var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.unLikedOpponentUserContactNumber = maskid;
                }



                if (element.unLikedByEmail !== null && element.unLikedByEmail !== undefined && element.unLikedByEmail !== '') {
                    var maskid = "";
                    var myemailId = element.unLikedByEmail;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                    var postfix = myemailId.substring(myemailId.lastIndexOf("@"));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.unLikedByEmail = maskid;
                }
                if (element.superLikedOpponentUserEmail !== null && element.superLikedOpponentUserEmail !== undefined && element.superLikedOpponentUserEmail !== '') {
                    var maskid = "";
                    var myemailId = element.superLikedOpponentUserEmail;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                    var postfix = myemailId.substring(myemailId.lastIndexOf("@"));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.superLikedOpponentUserEmail = maskid;
                }
                if (element.superLikedByContactNumber !== null && element.superLikedByContactNumber !== undefined && element.superLikedByContactNumber !== '') {
                    var maskid = "";
                    var myemailId = element.superLikedByContactNumber;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                    var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.superLikedByContactNumber = maskid;
                }
                if (element.superLikedOpponentUserContactNumber !== null && element.superLikedOpponentUserContactNumber !== undefined && element.superLikedOpponentUserContactNumber !== '') {
                    var maskid = "";
                    var myemailId = element.superLikedOpponentUserContactNumber;
                    var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                    var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                    for (var i = 0; i < prefix.length; i++) {
                        if (i == 0 || i == prefix.length - 1) {   ////////
                            maskid = maskid + prefix[i].toString();
                        }
                        else {
                            maskid = maskid + "*";
                        }
                    }
                    maskid = maskid + postfix;
                    element.superLikedOpponentUserContactNumber = maskid;
                }






            });

            userListCollection.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('GetUserLikeFilter')['200'], data: result, totalCount: ress }).code(200);
            });
            // return res({ message: req.i18n.__('GetUserLikeFilter')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetUserLikeFilter')['204'] }).code(204);
        }
    })
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetUserLikeFilter']['200']), data: Joi.any(), totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['GetUserLikeFilter']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }