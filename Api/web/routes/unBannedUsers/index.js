
let headerValidator = require('../../middleware/validator');

let PutAPI = require('./Put');

module.exports = [
   
    {
        method: 'Put',
        path: '/unBannedUser',
        handler: PutAPI.APIHandler,
        config: {
            tags: ['api', 'bannedUsers'],
            description: 'This API is used to login an user in the app.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload : PutAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response

        }
    }
];