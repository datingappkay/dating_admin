'use strict';

const Joi = require("joi");
const logger = require('winston');
const userLikes = require("../../../models/supperLikes");
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");

const APIHandler = (req, res) => {


    let condition = [
        { "$match": { "targetUserId": ObjectID(req.params._id), "isMatch": { "$exists": false } } },
        { "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "Data" } },
        { "$unwind": "$Data" },
        {
            "$project": {
                "LikedOn": "$timestamp",
                "firstName": "$Data.firstName",
                "dob": "$Data.dob",
                "profilePic": "$Data.profilePic",
                "email": "$Data.email",
                "gender": "$Data.gender",
                "height": "$Data.height",
                "profileVideo": "$Data.profileVideo",
                "heightInFeet": "$Data.heightInFeet",
                "city": "$Data.address.city",
                "country": "$Data.address.country",
                "contactNumber": "$Data.contactNumber",
                "otherImages": "$Data.otherImages",
                "myPreferences": "$Data.myPreferences",
                "about": "$Data.about"

            }
        },
        { "$sort": { "LikedOn": -1 } }

    ]
    userLikes.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }

        result.forEach(element => {
            if (element.email !== null && element.email !== undefined && element.email !== '') {
                var maskid = "";
                var myemailId = element.email;
                var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
                var postfix = myemailId.substring(myemailId.lastIndexOf("@"));


                for (var i = 0; i < prefix.length; i++) {
                    if (i == 0 || i == prefix.length - 1) {   ////////
                        maskid = maskid + prefix[i].toString();
                    }
                    else {
                        maskid = maskid + "*";
                    }
                }
                maskid = maskid + postfix;
                element.email = maskid;
            }
            if (element.contactNumber !== null && element.contactNumber !== undefined && element.contactNumber !== '') {
                var maskid = "";
                var myemailId = element.contactNumber;
                var prefix = myemailId.substring(0, myemailId.lastIndexOf(""));
                var postfix = myemailId.substring(myemailId.lastIndexOf(""));
                for (var i = 0; i < prefix.length; i++) {
                    if (i == 0 || i == prefix.length - 1) {   ////////
                        maskid = maskid + prefix[i].toString();
                    }
                    else {
                        maskid = maskid + "*";
                    }
                }
                maskid = maskid + postfix;
                element.contactNumber = maskid;
            }

        });

        return res({ message: req.i18n.__('GetLikeById')['200'], data: result }).code(200);
    });
};

const payloadValidator = Joi.object({
    _id: Joi.string().required().min(24).max(24).description('iserId'),
}).options({ allowUnknown: true });


const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetLikeById']['200']), data: Joi.any()
        },
        422: { message: Joi.any().default(local['GetLikeById']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator, response }