const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const makeASuggestion = require('../../../models/makeASuggestion');


let APIHandler = (req, res) => {

    let inMylanguage = req.headers.lang || "1";
    let _id = req.auth.credentials._id;

    setInMongo()
        .then(function (value) {
            logger.info('2 Async success!!!!');
            return res({ message: "success" }).code(200);
        })
        .catch(function (err) {
            logger.info('Caught an error!', err);
            return res({ message: "user not found to de-activate" }).code(412);
        });


    function setInMongo() {
        return new Promise(function (resolve, reject) {

            let dataToInsert = {
                userId: ObjectID(_id),
                name: req.payload.name,
                email: req.payload.email,
                suggestion: req.payload.description
            }
            makeASuggestion.Insert(dataToInsert, (err, result) => {
                if (err) {
                    return reject(new Error('Ooops, something broke!'));
                }
                else {
                    return resolve(result);
                }
            });
        });
    }



};

let validator = Joi.object({
    name: Joi.string().required().description("ex : Rahul").example("Rahul").error(new Error('name is missing')),
    email: Joi.string().required().description("ex : Rahul@mobifyi.com ").example("Rahul@mobifyi.com").error(new Error('email is missing')),
    description: Joi.string().required().description("ex : Rahul is not a spam user").example("Rahul is not a spam user").error(new Error('description is missing')),
}).required();

module.exports = { APIHandler, validator }