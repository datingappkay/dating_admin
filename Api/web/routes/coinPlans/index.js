let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PostAPI = require('./Post');
let PutAPI = require('./Put');
let DeleteAPI=require('./Delete');
let GetByText = require('./GetByText');

module.exports = [
    {
        method: 'GET',
        path: '/coinPlan',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'coinPlan'],
            description: 'This API is used to get coinPlan.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,

                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response

        }
    },
   
    {
        method: 'POST',
        path: '/coinPlan',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'coinPlan'],
            description: 'This API is used to post coinPlan.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response

        }
    },
    {
        method: 'PUT',
        path: '/coinPlan',
        handler: PutAPI.APIHandler,
        config: {
            tags: ['api', 'coinPlan'],
            description: 'This API is used to update coinPlan.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    },
    {
        method: 'POST',
        path: '/coinPlans',
        handler: DeleteAPI.APIHandler,
        config: {
            tags: ['api', 'coinPlan'],
            description: 'This API is used to Delete Id wise coinPlan.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: DeleteAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
    {
        method: 'GET',
        path: '/coinPlans/{text}',
        handler: GetByText.APIHandler,
        config: {
            tags: ['api', 'coinPlan'],
            description: 'This API is used to search Coinplan by planName.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByText.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByText.response

        }
    },


];