
const Joi = require("joi");
const logger = require('winston');
const coinPlans = require('../../../models/coinPlans');
const local = require("../../../locales");



payloadValidator = Joi.object({
    planName: Joi.string().required().description('planName'),
    noOfCoinUnlock: Joi.array().required().description('{"Gold":10000}'),
    currency: Joi.string().required().description('currency'),
    currencySymbole: Joi.string().required().description('currencySymbole'),
    cost: Joi.number().required().description('cost'),
    planId: Joi.string().required().description('actualId'),
    iOS: Joi.string().required().description('iOS'),


}).options({ allowUnknown: true });

APIHandler = (req, res) => {

   // console.log("=======>",req.payload)
    var noOfCoinUnlock = {};
    var otherPlanName ={}
        req.payload.noOfCoinUnlock.forEach(e => {
        noOfCoinUnlock[e.coinName] = parseInt(e.value);
    });
    req.payload.otherLang.forEach(ee => {
        otherPlanName[ee.lanuageCode] = ee.languageName
    });

    let data = {
        "planName": req.payload.planName,
        "otherPlanName":otherPlanName,
        "noOfCoinUnlock": noOfCoinUnlock,
        "status": "active",
        "statusCode": 1,
        "currency": req.payload.currency,
        "currencySymbol": req.payload.currencySymbole,
        "cost": req.payload.cost,
        "actualId": req.payload.planId,
        "actualIdForiOS": req.payload.iOS,
        "actualIdForAndroid": req.payload.planId,
        "timestamp": new Date().getTime(),
    };

//console.log("========>",data)
    coinPlans.Insert(data, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('PostcoinPlans')['200'] }).code(200);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostcoinPlans']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, payloadValidator }