const Joi = require("joi");
const logger = require('winston');
const coinPlans = require("../../../models/coinPlans");
const local = require("../../../locales");

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();
const APIHandler = (req, res) => {
    let match = { statusCode: { "$ne": 0 } }
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;


    let project = {
        planName: 1,
        noOfCoinUnlock: 1,
        cost: 1,
        currency: 1,
        currencySymbol: 1,
        actualId: 1,
        actualIdForiOS: 1,
        actualIdForAndroid: 1,
        otherPlanName: 1
    }



    coinPlans.selectWithProject(match, { "_id": -1 }, offset * limit, limit, project, (err, result) => {
        if (err) {
            logger.silly(err);

            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            result.forEach(element => {
                let coin = [];
                element.noOfCoinUnlockCount = Object.keys(element.noOfCoinUnlock).length;
                for (var x in element.noOfCoinUnlock) {
                    var dt = {
                        name: x,
                        value: element.noOfCoinUnlock[x]
                    }
                    coin.push(dt);
                }
                element.noOfCoinUnlock = coin;
            });
            coinPlans.TotalCount({}, (err, ress) => {
                return res({ message: req.i18n.__('GetcoinPlans')['200'], data: result, totalCount: ress }).code(200);
            });
        } else {
            return res({ message: req.i18n.__('GetcoinPlans')['204'] }).code(204);
        }
    });
};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetcoinPlans']['200']), data: Joi.any(), noOfCoinUnlock: Joi.any(), totalCount: Joi.any()
        },
        204: { message: Joi.any().default(local['Getcoins']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response }