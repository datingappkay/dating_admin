'use strict'
const Joi = require("joi");
const logger = require('winston');
const coinPlans = require('../../../models/coinPlans');
const ObjectID = require('mongodb').ObjectID;
const local = require("../../../locales");



const payloadValidator = Joi.object({
    _id: Joi.string().required().min(24).max(24).description('Id'),
    planName: Joi.string().required().description('planName'),
    noOfCoinUnlock: Joi.array().required().description('Eg . {Gold:1000,Silver:1000}'),
    currency: Joi.string().required().description('currency'),
    currencySymbole: Joi.string().required().description('currencySymbole'),
    cost: Joi.number().required().description('cost'),
    planId: Joi.string().required().description('actualId'),
    iOS: Joi.string().required().description('iOS'),

}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
   // console.log("============>", req.payload)
    var otherPlanName = {}
    var dt = {};
    req.payload.noOfCoinUnlock.forEach(e => {
        dt[e.coinName] = parseInt(e.value);

    });
    req.payload.otherLang.forEach(e => {
        var xx = e
        for (var x in xx) {
            otherPlanName[x]=xx[x]
        }

    });

    let data = {
        "planName": req.payload.planName,
        "otherPlanName": otherPlanName,
        "noOfCoinUnlock": dt,
        "status": "active",
        "statusCode": 1,
        "currency": req.payload.currency,
        "currencySymbol": req.payload.currencySymbole,
        "cost": req.payload.cost,
        "actualId": req.payload.planId,
        "actualIdForiOS": req.payload.iOS,
        "actualIdForAndroid": req.payload.planId,
        "timestamp": new Date().getTime(),

    };
    //console.log("=======datadata=====>", data)
    coinPlans.UpdateById(ObjectID(req.payload._id), data, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('PutcoinPlans')['200'] }).code(200);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PutcoinPlans']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response, payloadValidator }