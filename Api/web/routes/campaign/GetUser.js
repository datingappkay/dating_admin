'use strict';

const Joi = require("joi");
const logger = require('winston');
const userList = require("../../../models/userList");
const local  = require("../../../locales");


const payloadValidator = Joi.object({
    longitude: Joi.number().required().description('Enter longitude for EX. 77.5895467960067'),
    latitude: Joi.number().required().description('Enter latitude for EX. 13.0286684772121'),
    distance: Joi.number().required().description('Enter distance for EX. 100M or 10KM '),
    distanceType: Joi.string().required().allow(["M", "KM"]).description('Configuration change distanceType'),

}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    let condition = [
        {
            "$geoNear": {
                "near": {
                    "longitude": req.query.longitude,
                    "latitude": req.query.latitude
                },
                spherical: true,
                distanceField: "distance",
                maxDistance: 1000000,
                distanceMultiplier: (req.query.distanceType == "KM") ? 6378.1 : 3963.2,
                // distanceMultiplier: 6378137,

            }
        },
        { "$match": { distance: { "$lt": req.query.distance } } },
        { "$match": {   "deleteStatus" : {$ne:1} }},
        {
            "$project": {
                firstName: 1,
                deviceType:"$currentLoggedInDevices.deviceType",
                "city": "$address.city",
                "country": "$address.country",
                gender: 1,
                email: 1,
                contactNumber: 1,
                distance: 1


            }

        }, { "$sort": { "_id": -1 } },
    ];
    userList.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            console.log("resulttttttttttttttt---------------->",result)
            return res({ message:  req.i18n.__('GetUser')['200'], data: result }).code(200);
        } else {
            return res({ message:  req.i18n.__('GetUser')['204'], data: [] }).code(204);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetUser']['200']), data: Joi.any()
        },
        422: { message: Joi.any().default(local['GetUser']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator,response }