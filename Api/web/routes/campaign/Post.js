'use strict';

const Joi = require("joi");
const logger = require('winston');
const Cryptr = require('cryptr');
const cryptr = new Cryptr("3Embed1008");
const campainDetail = require("../../../models/campaign");
const ObjectID = require('mongodb').ObjectID;
const local  = require("../../../locales");

const payloadValidator = Joi.object({
    campainTitle: Joi.string().required().description("for EX. new sale for march").error(new Error('campainTitle is missing')),
    startDate: Joi.number().required().description("startDate,Eg. 1525147200000").error(new Error('startDate is missing')),
    endDate: Joi.number().required().description("endDate,Eg. 1525147200000").error(new Error('endDate is missing')),
    city: Joi.string().required().description("Enter city or country").error(new Error('city  is missing')),
    country: Joi.string().required().description("Enter city or country").error(new Error('country is missing')),
    campainImageUrl: Joi.string().required().description("campainImageUrl").error(new Error('campainImageUrl is missing')),
    pushTitle: Joi.string().required().description("pushTitle,Eg. sell offer").error(new Error('pushTitle is missing.')),
    message: Joi.string().required().description("message,Eg.for august sale").error(new Error('message is missing')),
    url: Joi.string().required().description("for E.x www.daum.com").error(new Error('Url is missing')),
    usersId: Joi.array().required().description('usersIds for valid user for campin'),


}).options({ allowUnknown: true });

const APIHandler = (req, res) => {

    var ids = req.payload.usersId
    // console.log("-------------->",ids._id)
    let usersId = [];

    req.payload.usersId.forEach(element => {
        usersId.push(new ObjectID(element._id));
    })
    let data = {

        "campainTitle": req.payload.campainTitle,
        "startDate":req.payload.startDate,
        "endDate": req.payload.endDate,
        "city": req.payload.city,
        "country": req.payload.country,
        "campainImageUrl": req.payload.campainImageUrl,
        "pushTitle": req.payload.pushTitle,
        "message": req.payload.message,
        "url": req.payload.url,
        "campainCreateOn": new Date().getTime(),
        'usersId': usersId,
        "views": [],
        "clicked": []



    }
    campainDetail.Insert(data, (err, result) => {
        if (err) {
            return res({ message:req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({  message: req.i18n.__('PostCamping')['200'] }).code(200);
        }
    });

};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostCamping']['200']), 
        },
        422: { message: Joi.any().default(local['PostCamping']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator }