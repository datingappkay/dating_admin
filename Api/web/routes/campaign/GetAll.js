'use strict';

const Joi = require("joi");
const logger = require('winston');
const campaignList = require("../../../models/campaign");
const local = require("../../../locales");

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();
const APIHandler = (req, res) => {
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    let condition = [
        {
            "$project": {
                campainImageUrl: 1,
                startDate: 1,
                endDate: 1,
                message:1,
                campainTitle: 1,
                city: 1,
                country: 1,
                campainCreateOn: 1,
                noOfTargetedUsers: { "$size": "$usersId" },
                views: { "$size": "$views.userId" },
                // creation:"$views.creation",
                clicked: { "$size": "$clicked.userId" }

            }
        },
        { "$sort": { "campainCreateOn": -1 } },
        { "$skip": offset },
        { "$limit": limit },


    ];
    console.log("------------=============>>>>>>>>>>>>>", JSON.stringify(condition));
    campaignList.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            return res({ message: req.i18n.__('GetAllCampaingDetails')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetAllCampaingDetails')['204'], data: [] }).code(204);
        }
    });
};
const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetAllCampaingDetails']['200']), data: Joi.any()
        },
        422: { message: Joi.any().default(local['GetAllCampaingDetails']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, response }