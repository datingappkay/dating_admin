
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let GetByText = require('./GetByText');


module.exports = [

    {
        method: 'GET',
        path: '/subscription',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'subscription'],
            description: 'This API is used to Get User All plan.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
   
    {
        method: 'GET',
        path: '/subscription/{text}',
        handler: GetByText.APIHandler,
        config: {
            tags: ['api', 'subscription'],
            description: 'This API is used to search plan.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetByText.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByText.response

        }
    },

   
];