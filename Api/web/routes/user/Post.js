'use strick'
const Joi = require("joi");
const logger = require('winston');
const Cryptr = require('cryptr');
const async = require("async");
const cryptr = new Cryptr("3Embed1008");
const userList = require("../../../models/userList");
const coinWallet = require("../../../models/coinWallet");
const userListType = require("../../../models/userListType");
const moment = require('moment');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;
const local = require("../../../locales");
const searchPreferencesCollection = require("../../../models/preferenceTable");

const payloadValidator = Joi.object({
    firstName: Joi.string().required().description("First Name,Eg. Lisa/John").error(new Error('firstName is missing.')),
    lastName: Joi.string().required().description("lastName,Eg. Lisa/John").error(new Error('lastName is missing.')),
    email: Joi.string().required().description("Email,Eg.xyz@gmail.com").error(new Error('emailAddress is missing')),
    city: Joi.string().required().description("city ,Eg. Bangaluru").error(new Error('cityName  is missing')),
    countryCode: Joi.string().required().description("Country Code, Eg. +91/+1").error(new Error('countryCode is missing')),
    country: Joi.string().required().description("city ,Eg. INDIA").error(new Error('countryName  is missing')),
    contactNumber: Joi.string().required().description("Mobile Number, Eg. 1234567890").error(new Error('phone is missing')),
    dob: Joi.string().required().description("Date of Birth(miliseconds),Eg. 786342377000").error(new Error('dateOfBirth is missing')),
    gender: Joi.string().required().description("Gender,Eg. Male/Female").error(new Error('gender is missing')),
    height: Joi.string().required().description("Height(Centimetre),Eg. 100/200").error(new Error('height is missing')),
    longitude: Joi.number().description("Longitude,Eg. 77.5894554").example(13.123456).error(new Error('longitude is missing')),
    latitude: Joi.number().description("Latitude,Eg. 13.0286543").example(77.123456).error(new Error('latitude is missing')),
    profilePic: Joi.string().description("imageUrl"),
    profileVideo: Joi.string().description("videoUrl"),
    searchPreferences: Joi.array().description("searchPreferences").error(new Error('searchPreferences is missing')),
    myPreferences: Joi.array().description("myPreferences").error(new Error('myPreferences is missing')),
    //profileVideoThumbnail: Joi.string().required().description("videoThumbnail").error(new Error('videoThumbnail is missing')),

}).options({ allowUnknown: true });


const APIHandler = (req, res) => {
    //let searchPreference = req.payload.searchPreferences;
    // searchPreference.forEach(e => {
    //     var searchPreferenceObj = {
    //         pref_id: ObjectID(e.pref_id),
    //     }
    //     if (e.pref_id.toString() == "57231014e8408f292d8b4567") {
    //         searchPreferenceObj.selectedValue = e.selectedValue
    //     }
    //     else {
    //         var arr = e.selectedValue.map(Number);
    //         searchPreferenceObj.selectedValue = arr;
    //         searchPreferenceObj.optionsUnits = e.selectedUnit;
    //     }

    //     searchPreferenceArray.push(searchPreferenceObj)
    // })
    var searchPreferenceArray = [];
    let myPrefrancesArray = [];
    let myPreference = req.payload.myPreferences
    myPreference.forEach(e => {
        var myPreferenceObj = {
            pref_id: ObjectID(e.pref_id),
        }
        myPreferenceObj["isDone"] = true;
        myPreferenceObj["selectedValues"] = e.selectedValues;
        myPrefrancesArray.push(myPreferenceObj)

    })
    var regDate = new Date();
    var data = req.payload.dob
    var dateOfBirth = new Date(data).getTime()
    var st = req.payload.height
    let _id = new ObjectID();
    const heightInFeetObj = {
        "4.0(121 cm)": `4'0"`,
        "4.1(124 cm)": `4'1"`,
        "4.2(127 cm)": `4'2"`,
        "4.3(129 cm)": `4'3"`,
        "4.4(132 cm)": `4'4"`,
        "4.5(134 cm)": `4'5"`,
        "4.6(137 cm)": `4'6"`,
        "4.7(139 cm)": `4'7"`,
        "4.8(142 cm)": `4'8"`,
        "4.9(144 cm)": `4'9"`,
        "4.10(147 cm)": `4'10"`,
        "4.11(149 cm)": `4'11"`,
        "5.0(152 cm)": `5'0"`,
        "5.1(154 cm)": `5'1"`,
        "5.2(157 cm)": `5'2"`,
        "5.3(160 cm)": `5'3"`,
        "5.4(162 cm)": `5'4"`,
        "5.5(165 cm)": `5'5"`,
        "5.6(167 cm)": `5'6"`,
        "5.7(170 cm)": `5'7"`,
        "5.8(172 cm)": `5'8"`,
        "5.9(175 cm)": `5'9"`,
        "5.10(177 cm)": `5'10"`,
        "5.11(180 cm)": `5'11"`,
        "6.0(182 cm)": `6'0"`,
        "6.1(185 cm)": `6'1"`,
        "6.2(187 cm)": `6'2"`,
        "6.3(190 cm)": `6'3"`,
        "6.4(193 cm)": `6'4"`,
        "6.5(195 cm)": `6'5"`,
        "6.6(198 cm)": `6'6"`,
        "6.7(200 cm)": `6'7"`,
        "6.8(203 cm)": `6'8"`,
        "6.9(205 cm)": `6'9"`,
        "6.10(208 cm)": `6'10"`,
        "6.11(210 cm)": `6'11"`,
        "7.0(213 cm)": `7'0"`
    }
    const heightInCMObj = {
        "4.0(121 cm)": 121,
        "4.1(124 cm)": 124,
        "4.2(127 cm)": 127,
        "4.3(129 cm)": 129,
        "4.4(132 cm)": 132,
        "4.5(134 cm)": 134,
        "4.6(137 cm)": 137,
        "4.7(139 cm)": 139,
        "4.8(142 cm)": 142,
        "4.9(144 cm)": 144,
        "4.10(147 cm)": 147,
        "4.11(149 cm)": 149,
        "5.0(152 cm)": 152,
        "5.1(154 cm)": 154,
        "5.2(157 cm)": 157,
        "5.3(160 cm)": 160,
        "5.4(162 cm)": 162,
        "5.5(165 cm)": 165,
        "5.6(167 cm)": 167,
        "5.7(170 cm)": 170,
        "5.8(172 cm)": 172,
        "5.9(175 cm)": 175,
        "5.10(177 cm)": 177,
        "5.11(180 cm)": 180,
        "6.0(182 cm)": 182,
        "6.1(185 cm)": 185,
        "6.2(187 cm)": 187,
        "6.3(190 cm)": 190,
        "6.4(193 cm)": 193,
        "6.5(195 cm)": 195,
        "6.6(198 cm)": 198,
        "6.7(200 cm)": 200,
        "6.8(203 cm)": 203,
        "6.9(205 cm)": 205,
        "6.10(208 cm)": 208,
        "6.11(210 cm)": 210,
        "7.0(213 cm)": 213
    }
    for (const k in heightInCMObj) {
        if (k == st) {
            var heightInCm = heightInCMObj[k]
        }

    }
    for (const k in heightInFeetObj) {
        if (k == st) {
            var heightInFeet = heightInFeetObj[k]
        }

    }
    dataToSend = {
        _id: _id,
        "firstName": req.payload.firstName,
        "email": req.payload.email,
        "contactNumber": "+" + req.payload.countryCode + req.payload.contactNumber,
        "countryCode": "+" + req.payload.countryCode,
        "dob": dateOfBirth,
        "gender": req.payload.gender,
        "registeredTimestamp": new Date().getTime(),
        "profilePic": req.payload.profilePic,
        "profileVideo": req.payload.profileVideo,
        creationTs: new Timestamp(1, Date.now()),
        creationDate: new Date(),
        searchPreferences: searchPreferenceArray,
        "location": {
            "longitude": req.payload.longitude || 0,
            "latitude": req.payload.latitude || 0
        },
        myPreferences: myPrefrancesArray,
        height: heightInCm,
        heightInFeet: heightInFeet,
        onlineStatus: 0,
        likedBy: [],
        myLikes: [],
        myunlikes: [],
        mySupperLike: [],
        supperLikeBy: [],
        disLikedUSers: [],
        matchedWith: [],
        lastUnlikedUser: [],
        recentVisitors: [],
        blockedBy: [],
        myBlock: [],
        supperLikeByHistory: [],
        count: {
            rewind: 0,
            like: 0
        },
        lastTimestamp: {
            rewind: 0,
            like: 0
        },
        profileVideoWidth: "",
        profileVideoHeight: "",
        userType: "Normal",
        address: {
            "city": req.payload.city,
            "country": req.payload.country,
        },
        profileStatus: 0,
        subscription: [
            {
                "planId": ObjectID("5b0d52c33e05b520f1605611"),
                "subscriptionId": ObjectID("5ba0ff82e2ef115db3a6ecb1"),
                "purchaseDate": new Date(),
                "purchaseTime": new Date().getTime(),
                "userPurchaseTime": new Date().getTime(),
                "durationInMonths": 12,
                "expiryTime": moment(regDate).add(12, 'M').valueOf(),
                "likeCount": "10",
                "rewindCount": "10",
                "superLikeCount": "10",
                "whoLikeMe": false,
                "whoSuperLikeMe": false,
                "recentVisitors": false,
                "readreceipt": false,
                "passport": false,
                "noAdds": false,
                "hideDistance": false,
                "hideAge": false
            }
        ],


    }
    //console.log("data============", dataToSend);

    async.series([
        // function (callback) {
        //     userPrefrances.Select({}, (err, result) => {
        //         myPrefrances = result;
        //         callback(err, result);
        //     })
        // },
        // function (callback) {
        //     for (let index = 0; index < myPrefrances.length; index++) {
        //         myPrefrances[index]["isDone"] = false;
        //         myPrefrances[index]["selectedValues"] = [];
        //         dataToSend.myPreferences.push({ pref_id: myPrefrances[index]._id, isDone: false })
        //     }
        //     callback(null, 1);
        // },
        function (callback) {
            /* Get Preferences to save In userList collection */
            searchPreferencesCollection.Select({}, (err, result) => {
                if (err) callback(err, result);

                dataToSend.searchPreferences = [];
                for (let index = 0; index < result.length; index++) {
                    /* add in my searchPreferences */
                    let searchPreferencesData = {
                        pref_id: result[index]._id,
                        selectedValue: (result[index].TypeOfPreference == 1) ? [result[index].OptionsValue[0]] : result[index].OptionsValue
                    }
                    if (result[index].TypeOfPreference == "3" || result[index].TypeOfPreference == "4") {
                        searchPreferencesData["selectedUnit"] = result[index].optionsUnits[0];
                    }
                    if (result[index]._id.toString() == "57231014e8408f292d8b4567") {
                        if (dataToSend.gender == "Male") {
                            searchPreferencesData["selectedValue"] = ["Female"]
                        } else {
                            searchPreferencesData["selectedValue"] = ["Male"]
                        }
                    }
                    dataToSend.searchPreferences.push(searchPreferencesData);
                }
                callback(err, result);
            })
        }
    ], function (err, result) {


        var coin = {
            "_id": _id,
            "coins": {
                "Coin": 0
            }
        }
        //console.log("=================>,dataToSend", dataToSend)
        //insert into coinwallet collection
        coinWallet.Insert(coin, (err, ress) => {
            if (err) {
                logger.silly(err)
            } else {
                console.log("inserted in to coin wallet")
            }
        })
        //mongo db insert userData
        userList.Insert(dataToSend, (err, result) => {
            if (err) {
                logger.silly(err);
                return res({ message: req.i18n.__('genericErrMsg')['422'] }).code(422);
            } else {
                let lat = dataToSend.location.latitude;
                let lon = dataToSend.location.longitude;
                delete dataToSend.creationTs
                delete dataToSend.location;
                dataToSend.creationTs = "0";
                dataToSend.location = { "lat": lat, "lon": lon };
                //inserting into elasticsearch
                userListType.Insert(dataToSend, (err, result) => {

                    if (err) {
                        logger.silly(err)
                    } else {
                        console.log("inserted in elasticsearch")
                    }

                });


                return res({ message: req.i18n.__('PostUser')['200'] }).code(200);
            }
        });
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostUser']['200']),
        },
        422: { message: Joi.any().default(local['genericErrMsg']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}


module.exports = { APIHandler, payloadValidator, response }