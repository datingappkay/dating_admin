const login = require('./login');
const prefrances = require('./prefrances');
const user = require('./user');
const bannedUsers = require('./bannedUsers');
const reportedUsers = require('./reportedUsers');
const likes = require('./likes');
const disLikes = require('./disLikes');
const match = require('./match');
const recentVisitors = require('./recentVisitors');
const language = require('./language');
const reportReasons = require('./reportReasons');
const notification = require('./notification');
const citys = require('./citys');
const countrys = require('./countrys');
const device = require('./device');
const profile = require('./profile');
const termsandConditions = require('./termsandConditions');
const appRate = require('./appRate');
const deactiveUsers = require('./deactiveUsers');
const unBannedUsers = require('./unBannedUsers');
const myDisLikes = require('./myDisLikes');
const myLikes = require('./myLikes');
const searchActiveDates = require('./searchActiveDates');
const searchCompletedDates = require('./searchCompletedDates');
const searchExpiredDates = require('./searchExpiredDates');
const verification = require("./verification");
const usersFilter = require("./usersFilter");
const usersActivityFilter = require("./usersActivityFilter");
const supperLikes = require("./supperLike");
const campaign = require("./campaign");
const campaingNotification = require("./campaingNotification");
const usersMatchFilter = require("./usersMatchFilter");
const blockedUsers = require("./blockedUsers");
const plans = require('./plans');
const subscription = require('./subscription');
const coins = require('./coins');
const coinPlans = require('./coinPlans')
const coinWallet = require('./coinWallet')
const walletPG = require('./walletPG')
const walletApp = require('./walletApp');
const coinConfig = require('./coinConfig');
const dashboard = require('./dashboard');
const ProUser = require('./proUser');
const dollatToCoin = require('./dollarToCoin');
const UserBoost = require('./userBoost')
const uploadImageInServer = require('./uploadImageInServer')
const cangePassword = require('./cangePassword')
const appVersion = require('./appVersion')
const otpLogs = require('./otpLogs')
const Question = require('./Question')
const CDNkey = require('./CDN-key')


module.exports = [].concat(
      login, prefrances, user, bannedUsers, reportedUsers, likes, disLikes, match, recentVisitors,
      language, reportReasons, notification, countrys, citys, device, profile, coinConfig,
       termsandConditions, appRate, deactiveUsers, unBannedUsers, myDisLikes, myLikes, walletApp,
      searchActiveDates, searchCompletedDates, searchExpiredDates, verification, walletPG,
      usersActivityFilter, usersFilter, supperLikes, campaign, campaingNotification, usersMatchFilter,
      blockedUsers, plans, subscription, coins, coinPlans, coinWallet,Question,CDNkey,
      dashboard, ProUser, dollatToCoin, UserBoost, uploadImageInServer, cangePassword, appVersion, otpLogs
);