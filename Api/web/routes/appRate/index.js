
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/appRate',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'appRate'],
            description: 'This API is used Get users App  ratings.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response,

        }
    }
];