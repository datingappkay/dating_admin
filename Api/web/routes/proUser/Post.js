const Joi = require("joi");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const subscriptionCollection = require('../../../models/subscription');
const userListCollection = require('../../../models/userList');
const mqtt = require("../../../library/mqtt");

const local = require("../../../locales");
const moment = require('moment');


const payloadValidator = Joi.object({
    userId: Joi.array().required().description('["5b0e879734874c0500481530"]'),
    durationInMonths: Joi.number().required().description('durationInMonths'),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    var purchaseDate = new Date().getTime();
    var durationInMonths = req.payload.durationInMonths
    let subscriptionId = new ObjectID();

    var array = [];
    var Ids = [];
    var data = {
        "planId": ObjectID(req.payload.planId),
        "subscriptionId": subscriptionId,
        "cost": req.payload.cost,
        "durationInMonths": durationInMonths,
        "planName": req.payload.planName,
        "purchaseDate": purchaseDate,
        "purchaseTime": new Date().getTime(),
        "userPurchaseTime": new Date().getTime(),
        "expiryTime": moment(purchaseDate).add(durationInMonths, 'M').valueOf(),
        "statusCode": 1,
        "status": "active",
        "currencySymbol": req.payload.currencySymbol,
        "currencyCode": req.payload.currencyCode,
        "paymentGateway": "N/A",
        "paymentGatewayTxnId": "N/A",
        "likeCount": req.payload.likeCount,
        "rewindCount": req.payload.rewindCount,
        "superLikeCount": req.payload.superLikeCount
    }

    req.payload.userId.forEach(e => {
        Ids.push(new ObjectID(e));
        array.push({
            "planId": ObjectID(req.payload.planId),
            "purchaseDate": purchaseDate,
            "subscriptionId": subscriptionId,
            "purchaseTime": new Date().getTime(),
            "userPurchaseTime": new Date().getTime(),
            "durationInMonths": durationInMonths,
            "planName": req.payload.planName,
            "expiryTime": moment(purchaseDate).add(durationInMonths, 'M').valueOf(),
            "userId": ObjectID(e),
            "statusCode": 1,
            "status": "active",
            "cost": req.payload.cost,
            "currencySymbol": req.payload.currencySymbol,
            "currencyCode": req.payload.currencyCode,
            "paymentGateway": "N/A",
            "paymentGatewayTxnId": "N/A",
            "likeCount": req.payload.likeCount,
            "rewindCount": req.payload.rewindCount,
            "superLikeCount": req.payload.superLikeCount
        })
        mqtt.publish(e, JSON.stringify({ "messageType": "proUser", data: data }), { qos: 2 }, (e, r) => { if (e) logger.error("/boost/match", e) });

    })
    let condition = { '_id': { '$in': Ids } };
    let dataToSend = {

        "subscription": [data],
        "userType": "Paid"


    }
    // mqtt.publish(`proUser/${req.payload.userId}`, JSON.stringify({ data }), { qos: 2 }, () => { });
    subscriptionCollection.InsertMany(array, (err, result) => {
        if (err) {
            logger.silly(err)
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            userListCollection.Update(condition, dataToSend, (e, r) => { });
            return res({ message: req.i18n.__('PostProUser')['200'] }).code(200);
        }

    })

};


const response = {
    // status: {
    //     200: {
    //         message: Joi.any().default(local['PostLanguage']['200']),
    //     },
    //     204: { message: Joi.any().default(local['genericErrMsg']['204']) },
    //     400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    //     500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    // }
}
module.exports = { APIHandler, payloadValidator, response }