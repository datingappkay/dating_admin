'use strict'
const Joi = require("joi");
const logger = require('winston');
const userList = require('../../../models/userList');
const ObjectID = require('mongodb').ObjectID;
const local  = require("../../../locales");


const payloadValidator = Joi.object({
    usersId: Joi.array().required().description('usersIds'),
}).options({ allowUnknown: true });

const APIHandler = (req, res) => {
    let array = [];
    req.payload.usersId.forEach(element => {
        array.push(new ObjectID(element));
    });
    let condition = {
        '_id': { '$in': array }
    };
    let datatoUpdate = {
        isDeactive: true,
        isDeactiveTimestamp: new Date().getTime(),
    };
    console.log(array);
    userList.Update(condition,datatoUpdate, (err, result) => {
         if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('PostDeactiveUser')['200'] }).code(200);
       }
     });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostDeactiveUser']['200']),
        },
        422: { message: Joi.any().default(local['PostDeactiveUser']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, payloadValidator,response }