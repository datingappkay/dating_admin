const Joi = require("joi");
const local  = require("../../../locales");
const CdnKeyCollection = require('../../../models/CdnKey');


payloadValidator = Joi.object({
    cloudName: Joi.string().required().description('cloudName'),
    apiKey: Joi.string().required().description('apiKey'),
    apiSecret: Joi.string().required().description('apiSecret'),
    uploadPreset: Joi.string().required().description('uploadPreset'),
}).options({ allowUnknown: true });

APIHandler = (req, res) => {


    var data = {
        "cloudName": req.payload.cloudName,
        "apiKey": req.payload.apiKey,
        "apiSecret": req.payload.apiSecret,
        "uploadPreset": req.payload.uploadPreset,
        "status": 1,
        "timestamp": new Date().getTime(),
    };
    // console.log("=========resss",data)
    CdnKeyCollection.Insert(data, (err, result) => {
        if (err) {
            return res({ message:req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message:req.i18n.__('PostCdnKeys')['200'] }).code(200);
        }

    })

};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostCdnKeys']['200']), 
        },
        204: { message: Joi.any().default(local['PostCdnKeys']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator }