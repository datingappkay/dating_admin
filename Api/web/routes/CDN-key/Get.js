const Joi = require("joi");
const local = require("../../../locales");
const CdnKeyCollection = require('../../../models/CdnKey');

const queryValidator = Joi.object({

}).required();
APIHandler = (req, res) => {

    CdnKeyCollection.Select({},{"_id":-1}, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) {
            return res({ message: req.i18n.__('GetCdnKeys')['200'],data:result}).code(200);
        } else {
            return res({ message: req.i18n.__('GetCdnKeys')['204'] }).code(204);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetAllCampaingDetails']['200']), data: Joi.any()
        },
        422: { message: Joi.any().default(local['GetAllCampaingDetails']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}
module.exports = { APIHandler, queryValidator, response }