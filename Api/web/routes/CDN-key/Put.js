
const Joi = require("joi");
const local = require("../../../locales");
const CdnKeyCollection = require('../../../models/CdnKey');
const ObjectID = require('mongodb').ObjectID;



payloadValidator = Joi.object({
    id: Joi.string().required().min(24).max(24).description('id'),
    newcloudName: Joi.string().required().description('cloudName'),
    newapiKey: Joi.string().required().description('apiKey'),
    newapiSecret: Joi.string().required().description('apiSecret'),
    newuploadPreset: Joi.string().required().description('uploadPreset'),
}).options({ allowUnknown: true });

APIHandler = (req, res) => {
    // console.log("============>",req.payload)

    let data = {
        "cloudName": req.payload.newcloudName,
        "apiKey": req.payload.newapiKey,
        "apiSecret": req.payload.newapiSecret,
        "uploadPreset": req.payload.newuploadPreset,
        "status": 1,
        "timestamp": new Date().getTime(),
    };

    //console.log("============>",data)
    CdnKeyCollection.UpdateById(ObjectID(req.payload.id), data, (err, result) => {
        if (err) {
            return res({  message: req.i18n.__('PostCamping')['500'] }).code(500);
        } else {
            return res({  message: req.i18n.__('PutCdnKeys')['200'] }).code(200);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PutCdnKeys']['200']), 
        },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, payloadValidator }