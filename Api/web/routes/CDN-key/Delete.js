const Joi = require("joi");
const local = require("../../../locales");
const CdnKeyCollection = require('../../../models/CdnKey');
const ObjectID = require('mongodb').ObjectID;

payloadValidator = Joi.object({
    id: Joi.string().required().description('id'),
}).options({ allowUnknown: true });

APIHandler = (req, res) => {
    CdnKeyCollection.Delete({ _id: ObjectID(req.query.id) }, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('DeleteCdnKeys')['200'] }).code(200);
        }

    })


};

module.exports = { APIHandler, payloadValidator }