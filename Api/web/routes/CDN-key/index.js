
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');
let PostAPI = require("./Post");
let DeleteAPI = require("./Delete");
let PutAPI = require("./Put")

module.exports = [
    {
        method: 'GET',
        path: '/cdnKeys',
        handler: GetAPI.APIHandler,
        config: {
            tags: ['api', 'language'],
            description: 'This API is used to get cdnKeys daata.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    },
    {
        method: 'POST',
        path: '/cdnKeys',
        handler: PostAPI.APIHandler,
        config: {
            tags: ['api', 'language'],
            description: 'This API is used to post cdnKeys.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    },
    {
        method: 'DELETE',
        path: '/cdnKeys',
        handler: DeleteAPI.APIHandler,
        config: {
            tags: ['api', 'language'],
            description: 'This API is used delete cdnKeys.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: DeleteAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    },
    {
        method: 'PUT',
        path: '/cdnKeys',
        handler: PutAPI.APIHandler,
        config: {
            tags: ['api', 'language'],
            description: 'This API is used to update cdnKeys.',
            auth: "adminJwt",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    },
];