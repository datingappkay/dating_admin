import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { DataTableModule } from "angular2-datatable";
// import { DirectivesModule } from "../theme/directives/directives.module";

import { PolicyComponent } from './policy/policy.component';
import { CKEditorModule } from 'ng2-ckeditor';

// export const routes = [
//   { path: '', component: 'ng2-adminconfig', pathMatch: 'full' },
//   { path: 'adminconfig', component: AdminConfigComponent, data: { breadcrumb: 'Ng2 Adminconfig' } }

// ];
export const routes = [
  // { path: '', redirectTo: 'policy', pathMatch: 'full' },
  // { path: 'policy', component: PolicyComponent, data: { breadcrumb: 'policy' } }
];
@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    // DirectivesModule,
    RouterModule,
    // DataTableModule,
    CKEditorModule,
    RouterModule.forChild(routes),
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    PolicyComponent
  ],


})
export class PolicyModule {

}