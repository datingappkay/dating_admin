import { Routes, RouterModule } from '@angular/router';

import { PolicyComponent } from './policy/policy.component';

import { ModuleWithProviders } from '@angular/core';

export const policyroutes: Routes = [
  {
    path: '', component: PolicyComponent, data: { title: '' },
   
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(policyroutes);