import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tablePipe'
})
export class TablePipe implements PipeTransform {
    
    transform(categories: any, searchText: any): any {
        if(searchText == null) return categories;
    
        return categories.filter(function(category){
          return category.firstName.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
        })
      }
}