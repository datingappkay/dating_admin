import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { DataTableModule } from "angular2-datatable";
// import { DirectivesModule } from "../theme/directives/directives.module";
import {Ng2PaginationModule} from 'ng2-pagination'; //importing ng2-pagination

import { ApprateComponent } from './appRate/appRate.component';
import { CKEditorModule } from 'ng2-ckeditor';

// export const routes = [
//   { path: '', component: 'ng2-adminconfig', pathMatch: 'full' },
//   { path: 'adminconfig', component: AdminConfigComponent, data: { breadcrumb: 'Ng2 Adminconfig' } }

// ];
export const routes = [
  // { path: '', redirectTo: 'policy', pathMatch: 'full' },
  // { path: 'policy', component: ApprateComponent, data: { breadcrumb: 'policy' } }
];
@NgModule({
  imports: [
    Ng2PaginationModule,
    CommonModule,
    ChartsModule,
    // DirectivesModule,
    RouterModule,
    // DataTableModule,
    CKEditorModule,
    RouterModule.forChild(routes),
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ApprateComponent
  ],


})
export class AppRateModule {

}