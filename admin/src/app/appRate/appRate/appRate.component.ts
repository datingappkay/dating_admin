import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit, ViewChild } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';
import { AppRateService } from '../appRate.service';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { ImageCropperComponent, CropperSettings, Bounds } from 'ng2-img-cropper';
declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'policy',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./appRate.component.scss'],
    templateUrl: './appRate.component.html',
    providers: [AppRateService]
})


export class ApprateComponent {
    public rowsOnPage= 15;
    public p = 1;
    public totalusers;
    viewsData: any;

    public config: any;
    public configFn: any;
  

    @ViewChild('cropper', undefined) cropper: ImageCropperComponent;
    imageSrc: any;
    public pushData=[];
    constructor(private _appConfig: AppConfig, private router: Router,
        vcRef: ViewContainerRef, fb: FormBuilder, private _service: AppRateService) {

       

    }
    userDetails = {
        Name: "Rahul",
        PhoneNo: "+919672829202",
        Email: "Rahul@mobifyi.com",
        RegistrationDate: "Friday, January 5, 2018 5:39:37 AM",
        DateOfBirth: "17/08/1992",
        LastLogin: "Friday, January 5, 2018 5:39:37 AM",
        Age: "25",
        Gender: "Male",
        Height: "166 cm , 5'6\" feet",
        ProfileVideo: "https://s3.amazonaws.com/sync1to1/SYNC1TO1Video20180104114749AM.mp4",
        RegisteredFrom: "Bengaluru / india",
        ProfilePhoto: "https://s3.amazonaws.com/datumv3/Sync1To1Image20180104114737AM.png",
        OtherPhotos: [
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png"
        ],
        OtherImages: [],
        Preferences: [

        ]
    }
        ngOnInit(){
            this.getPage(this.p)

         }

         getPage(p) {
            this._service.getotps().subscribe(
                result => {
                   // console.log("otp result=>", result)
                    if (result.data && result.data.length > 0) {
    
                        this.totalusers = result.totalCount;
                        // console.log("result==============>", this.totalusers)
                        
                        this.viewsData = result.data
    
                    } else {
                        this.viewsData = []
    
                    }
                }
    
    
            )
        }
        gotoSearch(dt){
    
            if(dt != null){
                this._service.getotpsSearchData(dt).subscribe(
                    result => {
                      //  console.log("otp result=>", result)
                        if (result.data && result.data.length > 0) {
        
                            this.totalusers = result.totalCount;
                            // console.log("result==============>", this.totalusers)
                            
                            this.viewsData = result.data
        
                        } else {
                            this.viewsData = []
        
                        }
                    }
        
        
                )
    
            }else{
    
            }
    
        }
        
}