import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
import { Configuration } from '../app.constant';
declare var API_URL: string;
@Injectable()
export class AppRateService {

    constructor(private http: Http, public _config: Configuration) {
    }

    getotps() {
        let url = this._config.Server + "otplogs";
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    getotpsSearchData(dt) {
        let url = this._config.Server + "otplogs?text=" + dt + "&status=1"
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
}
