import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//General
import { PagesComponent } from './pages/pages.component';
import { DashboardComponent } from './dashboardComponent/dashboard.component';
import { DynamicChartComponent } from './dashboardComponent/barcharts/barChart.component';
import { DynamicChartComponentLifetime } from './dashboardComponent/barcharts-lifetime/barChart.component';
import { DynamicChartComponentMonth } from './dashboardComponent/barcharts-month/barChart.component';
import { DynamicChartComponentWeek } from './dashboardComponent/barcharts-week/barChart.component-week';
import { DynamicChartComponentYear } from './dashboardComponent/barcharts-year/barChart.component';

import { AuthGuard } from './guards/index'; // for authentcat
//import custom component routes
import { routes } from './loginComponent/login.routing';
import { usersroutes } from './users/users.routing';
import { prefroutes } from './preferences/preference.routing';
import { profLikesRoutes } from './profiles/profile.routing';
import { matchesRoutes } from './matches/matches.routing';
import { policyroutes } from "./policy/policy.routing";

import { planroutes } from './plan/plan.routing';

import { textSettingroutes } from "./textSetting/textSetting.routing";
import { PolicyComponent } from "./policy/policy/policy.component";
import { termsandconditionsroutes } from "./termsandconditions/termsandconditions.routing";
import { appRateroutes } from "./appRate/appRate.routing";
//Pages
import { LoginComponent } from './loginComponent/login.component';
import { PageNotFoundComponent } from './pages/error/pagenotfound.component';
import { UsersComponent } from './users/components/users.component';
import { RecentVisitorsComponents } from "./users/recentVisitors/recentVisitors.components";
import { Likes } from "./users/likes/likes.components";
import { DisLikes } from "./users/disLikes/disLikes.components";
import { Matches } from "./users/matches/matches.components";
import { DeviceLogs } from "./users/deviceLogs/deviceLogs.components";

import { ReportedInDetailsComponents } from "./users/reportedInDetails/reportedInDetails.components";

import { LanguageComponents } from "./textSetting/languages/languages.components"
import { ReportReasonsComponents } from "./textSetting/reportReasons/reportReasons.components";
import { ReportDetailPageComponents } from "./textSetting/reportDetailPage/reportDetailPage.components"
import { AppComponent } from './app.component';
import { MyDisLikes } from "./users/mydisLikes/mydisLikes.components";
import { MyLikes } from "./users/mylikes/mylikes.components";
import { ActiveDatesComponent } from "./dates/activeDates/activeDates.component";
import { activeDatesDetailPageComponents } from "./dates/activeDatesDetailPage/activeDatesDetailPage.components"
import { completedDatesDetailPageComponents } from "./dates/completedDatesDetailPage/completedDatesDetailPage.components";
import { expiredDatesDetailPageComponents } from "./dates/expiredDatesDetailPage/expiredDatesDetailPage.components";

import { compaignUserPageComponent } from "./compaign/compaignUserPage/compaignUserPage.component";
import { compaignroutes } from './compaign/compaign.routing';
import { AppModule } from './app.module';
import { targetedUserComponent } from './compaign/targetedUser/targetedUser.component';
import { viewsCompaignComponent } from './compaign/viewsCompaign/viewsCompaign.component';
import { clickedCompaignComponent } from './compaign/clickedCompaign/clickedCompaign.component';
import { newCompaignsComponent } from './compaign/component/newCompaigns.component';
import { userLikesroutes } from './UserLikes/UserLikes.routing';
import { LikePageComponents } from './UserLikes/likePage/likePage.components';
import { MatchUserroutes } from './MatchesUser/MatchesUser.routing';
import { MatchesComponent } from './matches/components/matches.component';
import { safetytipsroutes } from './safetyTips/safetyTips.routing';
import { communityguidelinesroutes } from './communityGuidelines/communityGuidelines.routing';
import { licensesroutes } from './licenses/licenses.routing';
import { superLikes } from './users/superLikes/superLikes.components';
import { MySuperLikes } from './users/MySuperLikes/MySuperLikes.components';
import { BlockUserDetailComponents } from './users/BlockUserDetail/BlockUserDetail.components';
import { subscriptionroutes } from './subscription/subscription.routing';
import { coinroutes } from './coin/coin.routing';
import { ViewStatementsComponent } from './coin/ViewStatements/ViewStatements.component';
import { AppWalletroutes } from './AppWallet/AppWallet.routing';
import { CoinWalletroutes } from './CoinWallet/CoinWallet.routing';
import { AppVersionRoutes } from './appVersion/appVersion.routing';
import { languageComponent } from './language/components/language.component';
import { appConfigComponent } from './appConfig/components/appConfig.component';
import { cdnKeyComponent } from './CDN-Key/components/cdnKey.component';




const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  },
  {
    path: 'pages',
    component: PagesComponent,
    children: [
      {
        path: '',
        redirectTo: '/pages/dashboard',
        pathMatch: 'full'
      },

      {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'compaign',
        children: [...compaignroutes],
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'component',
        component: newCompaignsComponent,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'targetedUser/:id',
        component: targetedUserComponent,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'viewsCompaign/:id',
        component: viewsCompaignComponent,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'clickedCompaign/:id',
        component: clickedCompaignComponent,
        data: {
          title: 'Admin'
        }
      },


      {
        path: 'policy',
        children: [...policyroutes],
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'activeDates',
        component: ActiveDatesComponent,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'appRate',
        children: [...appRateroutes],
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'termsAndConditions',
        children: [...termsandconditionsroutes],
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'users',
        children: [...usersroutes],
        data: {
          title: 'Admin'
        }

      },

      {
        path: 'cdnKey',
        component: cdnKeyComponent,
        data: {
          title: 'CDN-keys'
        }
      },
      {
        path: 'languageComponent',
        component: languageComponent,
        data: {
          title: 'language'
        }
      },
      {
        path: 'UserLikes',
        children: [...userLikesroutes],
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'coin',
        children: [...coinroutes],
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'CoinWallet',
        children: [...CoinWalletroutes],
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'AppWallet',
        children: [...AppWalletroutes],
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'ViewStatements/:id',
        component: ViewStatementsComponent,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'reportReasons',
        component: ReportReasonsComponents,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'reportReasonsDetails/:id',
        component: ReportDetailPageComponents,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'activeDatesDetailPage/:id',
        component: activeDatesDetailPageComponents,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'completedDatesDetailPage/:id',
        component: completedDatesDetailPageComponents,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'expiredDatesDetailPage/:id',
        component: expiredDatesDetailPageComponents,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'MatchesComponent',
        component: MatchesComponent,
        data: {
          title: 'Admin'
        }
      },

      {
        path: 'preferences',
        children: [...prefroutes],
        data: {
          title: 'Preferences'
        }

      },
      {
        path: 'profile',
        children: [...profLikesRoutes],
        data: {
          title: 'Admin'
        }

      },
      {
        path: 'matches',
        children: [...matchesRoutes],
        data: {
          title: 'Admin'
        }

      },
      {
        path: 'appConfig',
        component: appConfigComponent,
        data: {
          title: 'appConfig'
        }
      },
      // {
      //   path: 'reportedList',
      //   children: [...reportRoutes],
      //   data: {
      //     title: 'Admin'
      //   }
      // },
      // {
      //   path: 'price',
      //   children: [...priceroutes],
      //   data: {
      //     title: 'Admin'
      //   }
      // },
      {
        path: 'recentVisitors/:id',
        component: RecentVisitorsComponents,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'BlockUserDetail/:id',
        component: BlockUserDetailComponents,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'likes/:id',
        component: Likes,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'superLikes/:id',
        component: superLikes,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'MySuperLikes/:id',
        component: MySuperLikes,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'mylikes/:id',
        component: MyLikes,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'disLikes/:id',
        component: DisLikes,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'mydisLikes/:id',
        component: MyDisLikes,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'matches/:id',
        component: Matches,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'deviceLogs/:id',
        component: DeviceLogs,
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'reportedInDetails/:id',
        component: ReportedInDetailsComponents,
        data: {
          title: 'Admin'
        }
      },

      // {
      //   path: 'deactiveUsers',
      //   component: deactivatePageComponents,
      //   data: {
      //     title: 'Admin'
      //   }
      // },

      // {
      //   path: 'bannedUser',
      //   component: BannedUserPageComponents,
      //   data: {
      //     title: 'Admin'
      //   }
      // },
      // {
      //   path: 'superLikesPage',
      //   component: superLikesPageComponents,
      //   data: {
      //     title: 'Admin'
      //   }
      // },
      // {
      //   path: 'reportedUser',
      //   component: ReportUserPageComponents,
      //   data: {
      //     title: 'Admin'
      //   }
      // }, 
      // {
      //   path: 'disLikedUserPage',
      //   component: disLikesUserPageComponents,
      //   data: {
      //     title: 'Admin'
      //   }
      // },
      // {
      //   path: 'recentVisitorsPage',
      //   component: recentVisitorPageComponents,
      //   data: {
      //     title: 'Admin'
      //   }
      // },
      {
        path: 'MatchesUser',
        children: [...MatchUserroutes],
        data: {
          title: 'Admin'
        }
      },
      {
        path: 'plan',
        children: [...planroutes],
        data: {
          title: 'Admin'
        }
      },
      // {
      //   path: 'notifications',
      //   children: [...notificationsRoutes],
      //   data: {
      //     title: 'Admin'
      //   }
      // },
      {
        path: 'subscription',
        children: [...subscriptionroutes],
        data: {
          title: 'Admin'
        }
      },
      // {
      //   path: 'coinWallet',
      //   children: [...coinWalletroutes],
      //   data: {
      //     title: 'Admin'
      //   }
      // },
      // {
      //   path: 'coinTransaction',
      //   children: [...coinTransactionroutes],
      //   data: {
      //     title: 'Admin'
      //   }
      // },

      {
        path: 'userdashboard',
        component: DashboardComponent,
        data: {
          title: 'Dashboard'
        }
      },
      {
        path: 'userActivitesDashboard',
        component: DynamicChartComponent,
        data: {
          title: 'userActivitesDashboard'
        }
      },

      {
        path: 'userMatchDashboard',
        component: DynamicChartComponentLifetime,
        data: {
          title: 'userMatchDashboard'
        }
      },

      {
        path: 'userDeviceDashboard',
        component: DynamicChartComponentMonth,
        data: {
          title: 'userDeviceDashboard'
        }
      },
      {
        path: 'appEarningsDashboard',
        component: DynamicChartComponentWeek,
        data: {
          title: 'appEarningsDashboard'
        }
      },
      {
        path: 'userCoinsDashboard',
        component: DynamicChartComponentYear,
        data: {
          title: 'UsersCoinsDashboard'
        }
      },
      {
        path: 'communityGuidelines',
        children: [...communityguidelinesroutes],
        data: {
          title: 'communityGuidelines'
        }
      },
      {
        path: 'licenses',
        children: [...licensesroutes],
        data: {
          title: 'licenses'
        }
      },

      {
        path: 'safetyTips',
        children: [...safetytipsroutes],
        data: {
          title: 'safetytipsroutes'
        }
      },
      {
        path: 'appversion',
        children: [...AppVersionRoutes],
        data: {
          title: 'App Version'
        }
      },
      {
        path: 'otpLogs',
        children: [...appRateroutes],
        data: {
          title: 'SMS logs'
        }
      },




    ],
    canActivate: [AuthGuard]
    //  canActivate: [AuthGuard] // Added Authguard for automatic logout if current user doesnt exists
  },
  {
    path: '',
    children: [...routes]
  },

  {
    path: '**',
    component: PageNotFoundComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes, { useHash: true });