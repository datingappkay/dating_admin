 import { Routes, RouterModule }  from '@angular/router';

import { appWalletComponent } from './appWallet/appWallet.component';
import { ModuleWithProviders } from '@angular/core';

export const AppWalletroutes: Routes = [ 
 {
       path: '',
       component: appWalletComponent,
       data:{
         title: 'App Wallet'
       }
     
 }
];

export const routing: ModuleWithProviders = RouterModule.forChild(AppWalletroutes);