import { Routes, RouterModule } from '@angular/router';

import { LanguageComponents } from './languages/languages.components';
import {ReportReasonsComponents} from './reportReasons/reportReasons.components';
import { ModuleWithProviders } from '@angular/core';

export const textSettingroutes: Routes = [
  {
    path: '', component: LanguageComponents, data: { title: '' },
   
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(textSettingroutes);