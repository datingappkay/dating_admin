import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


// import {MdDialog} from '@angular/material';
// import { AppConfig } from "../../../app.config";
import { TextSettingService } from '../textSetting.service';

// import $ from 'jquery';

declare var $: any;


@Component({
    selector: 'reportReasons',
    // encapsulation: ViewEncapsulation.None,
    templateUrl: './reportReasons.components.html',
    providers: [TextSettingService]

})
export class ReportReasonsComponents {

    loader = true;
    data: any = [];

    public chatDetails: any;
    constructor(private _TextSettingService: TextSettingService, private route: ActivatedRoute, private router: Router) {
        this._TextSettingService.getReasons()
            .subscribe((dataA) => {
                this.data = dataA.data;
            });
    }
    openDetailPage(_id) {
        console.log("_id is ", _id);
        this.router.navigate(['/pages/reportReasonsDetails/' + _id]);
    }
    AddReason() {

        $("#addReason").modal("show");
    }
    SaveReason() {
        $("#addReason").modal("hide");
        let reason = $("#reason").val();

        this._TextSettingService.addReason({ reason: reason, data: [] })
            .subscribe((dataA) => {
                this._TextSettingService.getReasons()
                    .subscribe((dataA) => {
                        this.data = dataA.data;
                    });
            });
        $("#reason").val("");
    }
}