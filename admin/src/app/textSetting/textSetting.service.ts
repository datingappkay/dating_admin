import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
import { Configuration } from '../app.constant';
declare var API_URL: string;
@Injectable()
export class TextSettingService {

    constructor(private http: Http, public _config: Configuration) {
    }

    addLanguge(data) {
        let url = this._config.Server + "language";
        return this.http.post(url, data, { headers: this._config.headers }).map(res => res.json());
    }

    getLanguges() {
        let Url = this._config.Server + "language";
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    deleteLanguge(_id) {
        let Url = this._config.Server + "language/" + _id;
        return this.http.delete(Url, { headers: this._config.headers })
            .map(res => res.json());
    }

    addReason(data) {
        let url = this._config.Server + "reportReasons";
        return this.http.post(url, data, { headers: this._config.headers }).map(res => res.json());
    }
    updateReason(data) {
        let url = this._config.Server + "reportReasons";
        return this.http.put(url, data, { headers: this._config.headers }).map(res => res.json());
    }
    getReasons() {
        let Url = this._config.Server + "reportReasons";
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }

    getReportReasons(_id) {
        let Url = this._config.Server + "reportReasons/"+_id;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
   


}