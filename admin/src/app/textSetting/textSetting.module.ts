import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModuleWithProviders } from '@angular/core';
import { routing } from './textSetting.routing';

//============ importing Ng2PaginationModule,AgmCoreModule,MyDatePickerModule  ================
import { Ng2PaginationModule } from 'ng2-pagination'; //importing ng2-pagination
import { AgmCoreModule } from 'angular2-google-maps/core';
import { MyDatePickerModule } from 'mydatepicker';
import { NouisliderModule } from 'ng2-nouislider';
import { CKEditorModule } from 'ng2-ckeditor';

//============ importing UsersComponent  ================
import { LanguageComponents } from './languages/languages.components';
import {ReportReasonsComponents} from './reportReasons/reportReasons.components';
import { ReportDetailPageComponents } from "./reportDetailPage/reportDetailPage.components" ;
//============ importing AgePipe,UserType filter pipes ================

// import { UserType } from '../pipes/usertype.pipe';
// import { PrfPic } from '../pipes/profilepicture';


// import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper'; // me
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    routing,
    Ng2PaginationModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCEIFo_lhymH_7uUR375RmK7_2v5GG6jJI', // datum bee api key
      // apiKey:  'AIzaSyD-iTOQrT6uPa2tfz_zjGBr9z7MmJ_VJNE', // datum api key
      libraries: ['places']
    }),
    MyDatePickerModule,
    NouisliderModule,    
    CKEditorModule
  ],
  declarations: [
    LanguageComponents,
    ReportReasonsComponents,
    ReportDetailPageComponents
    // UserType,
    // PrfPic,
    // ImageCropperComponent
  ]
})
export class TextSettingModule { }
