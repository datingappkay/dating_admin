import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


// import {MdDialog} from '@angular/material';
// import { AppConfig } from "../../../app.config";
import { TextSettingService } from '../textSetting.service';

// import $ from 'jquery';

declare var $: any;


@Component({
    selector: 'languages',
    // encapsulation: ViewEncapsulation.None,
    templateUrl: './languages.components.html',
    providers: [TextSettingService]

})
export class LanguageComponents {

    loader = true;
    data: any = [];

    public chatDetails: any;
    constructor(private _TextSettingService: TextSettingService, private route: ActivatedRoute, private router: Router) {
        this._TextSettingService.getLanguges()
            .subscribe((dataA) => {
                this.data = dataA.result;
                console.log("")
            });
    }
    AddLanguage() {
        $("#addLanguage").modal("show");
    }
    SaveLanguage() {
        $("#addLanguage").modal("hide");
        let languageName = $("#langeugeName").val();
        let RTL = $("input[name='RTL']:checked").val();
        this._TextSettingService.addLanguge({ languageName: languageName, RTL: RTL })
            .subscribe((dataA) => {
                this._TextSettingService.getLanguges()
                    .subscribe((dataA) => {
                        this.data = dataA.result;
                    });
            });
        $("#langeugeName").val("");
    }
    RemoveLanguage(i) {
        let _id = this.data[i]._id;

        this._TextSettingService.deleteLanguge(_id)
            .subscribe((dataA) => {

            });
        this.data.splice(i, 1);
    }
}