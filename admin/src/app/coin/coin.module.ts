import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 import { ModuleWithProviders } from '@angular/core';
 import { routing } from './coin.routing';
 import { ModalModule } from 'angular2-modal';
 import {
  ImageCropperComponent,
  CropperSettings,
  Bounds
} from 'ng2-img-cropper';
import {keyValuePipe} from '../pipes/keyValue.pipe'
//============ importing Ng2PaginationModule,AgmCoreModule,MyDatePickerModule  ================
import {Ng2PaginationModule} from 'ng2-pagination'; //importing ng2-pagination
//============ importing UsersComponent  ================
import { coinComponent } from './components/coin.component';
import { ViewStatementsComponent} from './ViewStatements/ViewStatements.component'
// import {Ng2TagsInputModule} from 'ng2-tagsinput';

import { MyDatePickerModule } from 'mydatepicker';
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    routing,
    Ng2PaginationModule,
    MyDatePickerModule,
    

  
  ],
  declarations: [
    coinComponent,
    ViewStatementsComponent,
    keyValuePipe
    
   ],
   exports:      [ keyValuePipe ]
})
 export class CoinModule { }
 
                                     