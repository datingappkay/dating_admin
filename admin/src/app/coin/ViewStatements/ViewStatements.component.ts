import {
    Component,
    ViewEncapsulation,
    Input,
    ViewChild,
    Inject,
    OnInit,
    ElementRef,
    NgZone,
    ChangeDetectorRef
} from '@angular/core';
import {
    BrowserModule
} from '@angular/platform-browser';
import {
    AppConfig
} from "../../app.config";
import {
    Router
} from '@angular/router';
import { MyDatePickerModule } from 'mydatepicker';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';

//importing service
import {
    coinService
} from '../coin.service';

import { ActivatedRoute } from '@angular/router';
import {
    AppState
} from "../../app.state";

declare var swal: any;
declare var sweetAlert: any;

declare var $: any;
declare var google: any;
declare var jQuery: any;

@Component({
    selector: 'ViewStatements',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./ViewStatements.component.scss'],
    templateUrl: './ViewStatements.component.html',
    providers: [coinService]
})

export class ViewStatementsComponent implements OnInit {
    public addCustWallet: FormGroup;
   public coinData:any[];
   public coinWalletData:any[];
public coinWallet:any[];
public id :any;
public deleteUser: any= [];
public  userId;
public _id;
public AddCoinData ={};
public rowsOnPage = 10;
public totalusers:any;
public p = 1;
pushData = [];
ActiveDatesSearch = {
    "dateFrom": 0, "dateTo": 0,
    "txnType": "All", "text": "All",
    "coinType":"All"
};
public model: any = { date: { year: 2018, month: 10, day: 9 } };
userDetails = {
"txnType": "DEBIT",
"trigger": "perMsgWithoutMatch",
"currency": "N/A",
"currencySymbole": "N/A",
"coinType": "Gold",
"coinOpeingBalance": 1950,
"cost": 0,
"coinAmount": 0,
"coinClosingBalance": 1950,
"paymentType": "N/A",
"timestamp": 1527580946882,
"transctionTime": 1527580946882,
"transctionDate": 1527580946882,
"paymentTxnId": "N/A",
"initatedBy": "customer"
}

    constructor(@Inject(ElementRef) elementRef: ElementRef, fb: FormBuilder, public cdr: ChangeDetectorRef,
        private _state: AppState, private router: Router, private _coinService: coinService,private route: ActivatedRoute,
        private _zone: NgZone) {
            this.addCustWallet = fb.group({
                'txnType': ['', Validators.required],
                'note': ['', Validators.required],
                // 'currency': ['', Validators.required],
                // 'currencySymbole': ['', Validators.required],
              // 'userId':'',
                //  'coinType': ['', Validators.required],
                // 'cost': ['', Validators.required],
                'coinAmount': ['', Validators.required],
                // 'paymentType': ['', Validators.required],
             
            });
    }
    ngOnInit() {
       this.getCoins(this.p);
       //this.getCoinWallets();
       this.getCustomerWallet();
    }
    backToUsers(){
        this.router.navigate(['/pages/coin']);
    }
    getCustomerWallet(){
        this.route.params.subscribe(params => {
            this._id = params['id'];
            //console.log("id - ", this._id)
            this._coinService
                .getCustomerWallet(this._id)
                .subscribe((data) => {
                    console.log("getViewsByFbId data", data);
                    if (data.data) {
                        this.coinWallet = data.data;
                    }
                    error => console.log(error)
                });
        });
    }
    getCoins(p){
        this._coinService.getCoins(p - 1, this.rowsOnPage).subscribe(
            result => {
                  this.coinData = result.data;
                  this.totalusers = result.totalCount;
                  this.p = p;
                    console.log("dsdADGETTTTTTTTTTTTT==============>", this.coinData)
            }
        )
    }
getCoinWallets(p){
        this._coinService.getCoinWallet(p - 1, this.rowsOnPage).subscribe(
            result => {
                    this.coinWalletData = result.data;
                    this.totalusers = result.totalCount;
                    this.p = p;
                    console.log("CoinWallet==============>", this.coinWalletData)
            }
        )
    }
    setActiveFromDate(date) {
        this.ActiveDatesSearch.dateFrom =(new Date(date).getTime());
        
        this._coinService.getSearchFromTOtoCoinWallet(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.coinWallet = data.data;
                this.totalusers = data.data.length;

            } else {
                this.coinWallet = [];
            }
        });
    }
    setActiveToDate(date) {
        this.ActiveDatesSearch.dateTo = new Date(date).getTime();
        this._coinService.getSearchFromTOtoCoinWallet(this.ActiveDatesSearch).subscribe((data) => {
            if (data.data) {
                this.coinWallet = data.data;
                this.totalusers = data.data.length;

            } else {
                this.coinWallet = [];
            }
        });
    }

setCoinTitle(data) {
    if (data == "" || data == null) {
        this.ActiveDatesSearch.coinType = "All";
        this.getCustomerWallet();
    } else {
        this.ActiveDatesSearch.coinType = data;
    }
    this.ActiveDatesSearch.coinType = data;
    this._coinService.getcoinType(this.ActiveDatesSearch).subscribe((data) => {
       if (data.data) {
            this.coinWallet = data.data;
            this.totalusers = data.data.length;

            console.log("useridddddddddd", this.coinWallet)
        } else {
            this.coinWallet = [];
        }
    });
   

}
setTxnType(data) {
    console.log("useridddddddddd",data)
    if (data == "" || data == null) {
        this.ActiveDatesSearch.txnType = "All";
    } else {
        this.ActiveDatesSearch.txnType = data;
    }
    this.ActiveDatesSearch.txnType = data;
    this._coinService.getSearchFromCustWalletDates(this.ActiveDatesSearch).subscribe((data) => {
       if (data.data) {
            this.coinWallet = data.data;
            this.totalusers = data.data.length;

            console.log("useridddddddddd", this.coinWallet)
        } else {
            this.coinWallet = [];
        }
   });
   
}
setActiveText(data) {
    if (data == "" || data == null) {
        this.ActiveDatesSearch.text = "All";
    } else {
        this.ActiveDatesSearch.text = data;
    }
    this._coinService.getSearchFromCustWalletDates(this.ActiveDatesSearch).subscribe((data) => {
        if (data.data) {
            this.coinWallet = data.data;
            this.totalusers = data.data.length;

        } else {
            this.coinWallet = [];
        }
    });
    

}


submitForm(value) {
     var userId= this._id;
    this._coinService.addCustomerWallet(value._value, userId).subscribe(
        result => {
            if (result.code !== 200) {
                swal("Success!", "Customer Wallet Added Successfully!", "success");
                jQuery('#addCustomerWallet').modal('hide');
                this.getCustomerWallet();
            } else {
                sweetAlert("Oops...", "Something went wrong!", "error");
            }
               this.addCustWallet.reset();
        });
        }
     
    resetForm(){
        this.addCustWallet.reset();
        }

    }







