import { Component, ViewEncapsulation, Input, Output, ViewChild, Inject, OnInit, ElementRef, NgZone, ChangeDetectorRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppConfig } from "../../app.config";
import {
    Router
} from '@angular/router';

import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
    FormArray
} from '@angular/forms';
import {
    FileUploader, FileSelectDirective
} from 'ng2-file-upload';
import {
    AgmCoreModule,
    MapsAPILoader
} from 'angular2-google-maps/core';
import {
    ImageCropperComponent,
    CropperSettings,
    Bounds
} from 'ng2-img-cropper';




import {
    AppState
} from "../../app.state";


import 'rxjs/add/observable/timer';

import "../../../../node_modules/jquery/dist/jquery-1.9.1.js";
import "../../../../node_modules/jquery/dist/jquery-ui.js";
import { Pipe, PipeTransform } from '@angular/core';


import {
    coinService
} from '../coin.service';

declare var swal: any;
declare var sweetAlert: any;

declare var $: any;
declare var google: any;
declare var jQuery: any;
declare var Map: any;
@Pipe({ name: 'keyValuePipe' })
@Component({
    selector: 'coin',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./coin.component.scss'],
    templateUrl: './coin.component.html',
    providers: [coinService],

})

export class coinComponent implements OnInit {

    transform(value, args: string[]): any {
        let keyValuePipe = [];
        for (let key in value) {
            //   keys.push(key);
            keyValuePipe.push({ key: key, value: value[key] });
        }
        return keyValuePipe;
    }

    public addCoins: FormGroup[];
    public addcoins: FormGroup;
    private CoinData: any[];
    public CoinPlanData: any[];
    public CoinWalletData: any[];
    public addcoin: FormGroup;
    public editcoin: FormGroup;
    public addcoinPlan: FormGroup;
    public EditcoinPlan: FormGroup;
    public rowsOnPage = 10;
    public rowsOnPage1 = 10;
    public p = 1;
    public p1 = 1;
    oldcoin: any;
    oldcoinSymbol: any;
    oldplanId = [];
    oldplanName: any;
    oldnoOfCoinUnlock: any = [];
    oldcurrency: any;
    oldcurrencySymbole: any;
    oldcost: any;
    Aimage = '';
    private data: any;
    public croppingBoxPush: boolean = false;
    public detailsBoxPush: boolean = false;
    private hasError: boolean = false;
    private hasErrorE: boolean = false;
    private data2: any;
    public fullImagePath: any;
    public file_srcs: any[] = []; // me
    public file_srcsUp: any[] = []; // me
    public debug_size_before: any[] = []; // me
    public debug_size_after: any[] = []; // me
    public croppingBox: boolean = false;
    public croppingBoxUp: boolean = false;
    public image: any;
    public alertMsg: boolean;
    private isUploadBtn: boolean = true;
    public users: any[];
    public deleteUser: any = [];
    public id: any;
    private base64textString: string = "";
    cropperSettings: CropperSettings;
    cropperSettings2: CropperSettings;
    elementRef: ElementRef;
    filesToUpload: Array<File>;



    public rowsOnPage2 = 10;

    public PageDetails;
    public totalusers;

    public pageNum: number = 1;
    public limit: number = 10;

    public flag: number = 0;
    // public Status: number;
    public pushData = [];
    public fullVideoPath = "";
    public myPlayer: any;
    public lang: any;
    public playerHTML: any;
    public totalItem: any;
    toggleBool: boolean = true;
    newcoinSymbol: any;
    public AddCoinData = [];
    public pushWalletData = []
    public coinWallet: any[];

    constructor(@Inject(ElementRef) elementRef: ElementRef, fb: FormBuilder, public cdr: ChangeDetectorRef, private _coinService: coinService,
        private _state: AppState, private router: Router, private formBuilder: FormBuilder,
        private _zone: NgZone, private http: Http) {


        this.addcoin = fb.group({
            'coinTitle': ['', Validators.required],
            'imgURL': ['', Validators.required],

        });
        this.addcoins = fb.group({
            'coinTitle': ['', Validators.required],
            'coins': ['', Validators.required],

        });

        this.editcoin = fb.group({
            'coinTitle': ['', Validators.required],
            'imgURL': ['', Validators.required],

        });

        this.addcoinPlan = fb.group({
            'planName': ['', Validators.required],
            'noOfCoinUnlock': ['', Validators.required],
            'currency': ['', Validators.required],
            'currencySymbole': ['', Validators.required],
            'cost': ['', Validators.required],
            'planId': ['', Validators.required],
            'planIdiOS': ['', Validators.required],


        });

        this.EditcoinPlan = fb.group({
            'planName': ['', Validators.required],
            "noOfCoinUnlock": ['', Validators.required],
            'currency': ['', Validators.required],
            'currencySymbole': ['', Validators.required],
            'cost': ['', Validators.required],
            'planId': ['', Validators.required],
            'planIdiOS': ['', Validators.required]

        });

    }

    ngOnInit() {

        this.getCoins(this.p);
        this._coinService.getlang().subscribe(result => {
            //console.log("======", result.result)
            if (result.result) {
                this.lang = result.result
            }
        });

    }
    public uploader: FileUploader = new FileUploader({ url: URL, itemAlias: 'newProfilePicture' });
    title: string;
    previewImage: any;
    tempImage: any;
    source: string;

    store(data) {
        var dt = {
            "coinName": data.coinTitle,
            "value": data.coins
        };
        this.AddCoinData.push(dt);
        jQuery('#addCoins').modal('hide');
        this.addcoins.reset();
    }
    getCoins(p) {
        this._coinService.getCoins(p - 1, this.rowsOnPage).subscribe(
            result => {
                this.CoinData = result.data;
                this.totalusers = result.totalCount;
                this.p = p;
                //console.log("getCoins Data==============>", result)

            }
        )
    }
    getCoinPlans(p) {
        this._coinService.getCoinPlan(p - 1, this.rowsOnPage).subscribe(
            result => {
                this.CoinPlanData = result.data;
                this.totalusers = result.totalCount;
                this.p = p;
                //console.log("getCoinPlans==============>", result)
            });
    }
    showArrayData(id) {
        this.CoinPlanData.forEach(e => {
            if (e._id == id) {
                this.pushData = e.noOfCoinUnlock;
            }
        });

    }
    showWalletArrayData(id) {

        this.CoinWalletData.forEach(e => {
            if (e._id == id) {
                this.pushWalletData = e.coins;
            }
        });
    }
    getCoinWallets(p) {
        this._coinService.getCoinWallet(p - 1, this.rowsOnPage).subscribe(
            result => {
                this.CoinWalletData = result.data;
                this.totalusers = result.totalCount;
                this.p = p;
                //console.log("getCoinWallets data==============>", result)

            }
        )
    }
    ViewStatement(_id) {

        this.router.navigate(['/pages/ViewStatements/' + _id]);

    }
    submitForm(value) {

        this.addcoin.value.imgURL = this.fullImagePath;
        this._coinService.addCoins(value._value).subscribe(
            result => {
                if (result.code !== 200) {
                    swal("Success!", "Coin Added Successfully!", "success");
                    jQuery('#addCoin').modal('hide');
                    this.getCoins(this.p);

                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        )
        this.addcoin.reset();
    }
    languageAdd(event, i) {
        if (event.target.value.length > 0) {
            this.lang[i].langeugeCodVal = event.target.value;
            this.lang[i].checked = true;
        } else {
            this.lang[i].checked = false;
        }
    }
    submitCoinPlanForm(value) {
        var langName = [];
        this.lang.forEach(x => {
            if (x.checked == true) {
                var str = {
                    lanuageCode: x.code,
                    languageName: x.langeugeCodVal
                }
                langName.push(str);

            }
        });
        value._value.otherLang = langName;
        var noOfCoinUnlock = this.AddCoinData
        this._coinService.addCoinPlans(value._value, noOfCoinUnlock).subscribe(
            result => {
                if (result.code !== 200) {
                    swal("Success!", "Coin Plan Added Successfully!", "success");
                    jQuery('#addCoinPlan').modal('hide');
                    this.getCoinPlans(this.p)

                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        )
        this.addcoinPlan.reset();
    }


    makeFileRequest(url: string, params: Array<string>, files: any) {
        return new Promise((resolve, reject) => {
            var formData: FormData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append('photo', files, files.name);

            // console.log("formData : ", formData)
            // console.log("files : ", files)
            // console.log("url : ", url)
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));


                    } else {
                        reject(xhr.response);
                    }

                }

            }

            xhr.open('POST', url, true);
            xhr.send(formData);

        });

    }







    EditCoinModal(coin) {
        // console.log("========edite coin", coin)
        this.id = coin._id,
            this.oldcoin = coin.coinTitle,
            this.oldcoinSymbol = coin.imgURL
    }

    public arr = [];

    EditCoinPlanModal(coin) {
        console.log("coin", coin)
        var xx = coin.otherPlanName
        for (var x in xx) {
            jQuery('.text' + x).val(xx[x]);
        }
        this.id = coin._id,
            this.oldplanName = coin.planName,
            this.oldcurrency = coin.currency,
            this.oldcurrencySymbole = coin.currencySymbol,
            this.oldcost = coin.cost,
            this.oldplanId = coin
        //  console.log("coin", this.oldplanId)
        var dt = {
            "coinName": (!coin.noOfCoinUnlock[0])?"Coin":coin.noOfCoinUnlock[0].name,
            "value": (!coin.noOfCoinUnlock[0])?0:coin.noOfCoinUnlock[0].value 
        };

        var index = this.oldnoOfCoinUnlock.indexOf(dt);
        while (this.oldnoOfCoinUnlock.length != 0) {
            this.oldnoOfCoinUnlock.pop();
        }
        this.oldnoOfCoinUnlock.push(dt);
    }

    remove = function (coin, index) {
        this.oldnoOfCoinUnlock.splice(index, 1);
    }
    removeCoin(coin, index) {
        this.AddCoinData.splice(index, 1);
    }

    gotoeditCoin(coin) {
        console.log("dsadsadadold data ======>", coin._value)

        var id = this.id;
        let body = {
            id: this.id,

        }
        console.log("this.oldcoinSymbol", this.oldcoinSymbol);

        if (this.fullImagePath) {
            this._coinService
                .EditCoins(body, coin._value.coinTitle, this.fullImagePath).subscribe((data) => {
                    console.log("dataaaaaaaaaaaaaaaaaaaa", data);
                    if (data.code !== 200) {
                        this.CoinData = data.data;
                        swal("Success!", "Coin Successfully Edit!", "success");
                        jQuery('#EditCoin').modal('hide');
                        this.getCoins(this.p);
                    }
                })
        } else {
            this._coinService
                .EditCoins(body, coin._value.coinTitle, this.oldcoinSymbol).subscribe((data) => {
                    console.log("dataaaaaaaaaaaaaaaaaaaa", data);
                    if (data.code !== 200) {
                        this.CoinData = data.data;
                        swal("Success!", "Coin Successfully Edit!", "success");
                        jQuery('#EditCoin').modal('hide');
                        this.getCoins(this.p);
                    }
                })
        }



    }

    gotoeditCoinPlan(coin) {
        var langName = [];
        this.lang.forEach(x => {
           // if (x.checked == true) {
                var str = {
                    // lanuageCode: x.code,
                    // languageName: x.langeugeCodVal
                    [x.code]: jQuery('.text' + x.code).val()

                }
                langName.push(str);
            //}
        });
        // console.log("dsadsadadold data ======>", coin._value)
        var id = this.id;
        let body = {
            id: this.id,
        }
        this.oldnoOfCoinUnlock = this.AddCoinData

        this._coinService
            .EditCoinPlan(body, coin._value.planName, this.oldnoOfCoinUnlock, coin._value.cost, coin._value.currency, coin._value.currencySymbole, coin._value.planId, coin._value.planIdiOS,langName).subscribe((data) => {
                // console.log("dataaaaaaaaaaaaaaaaaaaa", data);
                if (data) {
                    $('EditCoinPlan').modal('hide')

                    this.CoinData = data.data;
                    swal("Success!", "Coin Plan Successfully Edited!", "success");
                    $('#EditCoinPlan').modal('hide')
                    this.getCoinPlans(this.p);
                }
            })


    }
    gotoSearchCoin(term) {
        this._coinService.getSearchCoins(term).subscribe(
            (data) => {
                this.CoinData = data.data;
                // console.log("yyyyyyyyyyy", data.data);
            });
        this.getCoins(this.p);
    }


    gotoSearchCoinPlan(term) {
        this._coinService.getSearchCoinPlans(term).subscribe(
            (data) => {
                this.CoinPlanData = data.data;
                // console.log("yyyyyyyyyyy", data.data);
            });
        this.getCoinPlans(this.p);
    }

    gotoSearchCoinWallet(term) {
        this._coinService.getSearchCoinWallet(term).subscribe(
            (data) => {
                this.CoinWalletData = data.data;
                // console.log("yyyyyyyyyyy", data.data);
            });
    }
    selectAllCoinCheckBox() {
        this.deleteUser = [];
        this.CoinData.forEach((obj) => {
            obj.checked = !obj.checked;
            if (obj.checked) {
                this.deleteUser.push(obj._id)
            }
            // this.gotocheck(obj.name, event);
        });
        // console.log("===>>", this.deleteUser)

    }
    selectAllCoinPlanCheckBox() {
        this.deleteUser = [];
        this.CoinPlanData.forEach((obj) => {
            obj.checked = !obj.checked;
            if (obj.checked) {
                this.deleteUser.push(obj._id)

            }

            // this.gotocheck(obj.name, event);
        });
        // console.log("===>>", this.deleteUser)

    }


    gotocheckCoin(id) {
        // console.log("idddddddddssssssssss", id);
        // this.users.forEach(this.user,(obj) => {
        //     obj.checked = !obj.checked;

        var index = this.deleteUser.indexOf(id);
        if (index === -1) {
            this.deleteUser.push(id);
        }
        else {
            this.deleteUser.splice(index, 1);

        }
        // console.log("selected:-", this.deleteUser);

    }

    DeleteCoin() {

        // console.log("idddddddd", this.deleteUser);
        if (this.deleteUser.length == 0) {
            swal("Please Select Coin");
        } else {
            var index = -1;
            this._coinService
                .deletecoins(this.deleteUser).subscribe((data) => {

                    if (data.code !== 200) {
                        this.CoinData = data.data;
                        swal("Success!", "Coin Deleted!", "success");
                        this.getCoins(this.p);
                        this.deleteUser.splice(index, 1)
                    }

                });
        }
    }

    DeleteCoinPlan() {
        //console.log("idddddddd", this.deleteUser);
        if (this.deleteUser.length == 0) {
            swal("Please Select Coin Plan");
        } else {
            var index = -1;
            this._coinService
                .deletecoinPlan(this.deleteUser).subscribe((data) => {

                    if (data.code !== 200) {
                        this.CoinData = data.data;
                        swal("Success!", "Coin Plan Deleted!", "success");
                        this.deleteUser.splice(index, 1)
                        this.getCoinPlans(this.p);
                    }

                });
        }
    }
    AfileChange(input) {
        if ((input.files[0].size / 1000) > 300) {
            jQuery("#upImg").show();
            jQuery("#eupImg").show();
        } else {
            jQuery("#upImg").hide();
            jQuery("#eupImg").hide();
            const reader = new FileReader();
            if (input.files.length) {
                const file = input.files[0];
                reader.onload = () => {
                    this.Aimage = reader.result;
                    // console.log("--------------->", this.Aimage)

                    this._coinService.uploadImgInServer(this.Aimage).subscribe(
                        result => {
                            this.fullImagePath = result.res

                        })
                }
                reader.readAsDataURL(file);
            }
        }
    }
    AremoveImage(): void { this.Aimage = ''; }

    CancelAddCoin() {
        this.addcoin.reset();
    }
    CancelAddCoinPlan() {
        this.addcoinPlan.reset();
    }
    CancelAddCoins() {
        this.addcoins.reset();
    }
}


