import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';


// import {MdDialog} from '@angular/material';
// import { AppConfig } from "../../../app.config";
import { DatesService } from '../dates.service';

// import $ from 'jquery';

declare var $: any;


@Component({
    selector: 'expiredDatesDetailPage',
    // encapsulation: ViewEncapsulation.None,
    templateUrl: './expiredDatesDetailPage.components.html',
    providers: [DatesService]

})
export class expiredDatesDetailPageComponents {

    loader = true;
    data: any;
    public prefData = [];

    userDetails = {
        Name: "Rahul",
        PhoneNo: "+919672829202",
        Email: "Rahul@mobifyi.com",
        RegistrationDate: "Friday, January 5, 2018 5:39:37 AM",
        DateOfBirth: "17/08/1992",
        LastLogin: "Friday, January 5, 2018 5:39:37 AM",
        Age: "25",
        about: "--",
        Gender: "Male",
        Height: "166 cm , 5'6\" feet",
        ProfileVideo: "https://s3.amazonaws.com/sync1to1/SYNC1TO1Video20180104114749AM.mp4",
        RegisteredFrom: "Bengaluru / india",
        ProfilePhoto: "https://s3.amazonaws.com/datumv3/Sync1To1Image20180104114737AM.png",
        OtherPhotos: [
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png"
        ],
        OtherImages: [],
        Preferences: [

        ]
    }

    public chatDetails: any;
    constructor(private _DatesService: DatesService, private route: ActivatedRoute, private router: Router) {
        this.route.params.subscribe(params => {
            let _id = params['id'];
            this._DatesService.getExpiredDateById(_id).subscribe((data) => {
                if (data.data) {
                    this.data = data.data;
                } else {
                    this.data = [];
                }
                console.log("Datav ", data);

            });
        });
    }
    backToUsers() {
        this.router.navigate(['/pages/activeDates']);
    }

    userDetailsModal(userID) {
        this._DatesService
            .getProfileById(userID)
            .subscribe((user) => {
                if (user.data[0]) {
                    user = user.data[0];
                    this.prefData = []
                    //  console.log("+++++++++",user)
                    var searchPreferences = user.myPreferences
                    //    console.log("-----------6^^][][][][]", this.prefData)
                    searchPreferences.forEach(ele => {
                        if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Religious beliefs",
                                "selectedValue": ele.selectedValues[0],
                                "type": 3,
                                "pref_id": ele.pref_id,
                            })
                        }
                        else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Ethnicity",
                                "selectedValue": ele.selectedValues,
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Kids ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Work ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Job",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Education ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Politics ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Family Plans",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Highest Level Attended ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Drinking ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Smoking ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Marijuana",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Drugs",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }
                        else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                            this.prefData.push({
                                "title": "Height ",
                                "selectedValue": ele.selectedValues[0],
                                "pref_id": ele.pref_id,
                                "type": 3
                            });
                        }


                    });
                    this.userDetails.OtherPhotos.length = 0;
                    this.userDetails.OtherPhotos = [];
                    var years = moment().diff(user.dob, 'years');
                    this.userDetails.Name = user.firstName || "--";
                    this.userDetails.PhoneNo = user.contactNumber || "--";
                    this.userDetails.Email = user.email || "--";
                    this.userDetails.Gender = user.gender || "--";
                    this.userDetails.DateOfBirth = user.dob || 0;
                    this.userDetails.about = user.about || "--";
                    this.userDetails.RegistrationDate = user.registeredTimestamp || "--";
                    this.userDetails.Height = user.height + " cm , " + user.heightInFeet + " feet" || "--";
                    this.userDetails.ProfileVideo = user.profileVideo || "";
                    jQuery(".videoAutoPlay").attr("src", this.userDetails.ProfileVideo);
                    this.userDetails.ProfilePhoto = user.profilePic || "https://help.github.com/assets/images/help/profile/identicon.png";
                    this.userDetails.OtherPhotos = user.otherImages || [];
                    this.userDetails.OtherPhotos.push(this.userDetails.ProfilePhoto);
                    this.userDetails.Preferences = user.favoritePreferences || [];
                    this.userDetails.Age = years || "--";
                    this.userDetails.LastLogin = user.lastLogin || user.registeredTimestamp,
                        this.userDetails.RegisteredFrom = "--";
                    if (user.address && (user.address.city || user.address.country)) {
                        this.userDetails.RegisteredFrom = user.address.city + " ," || " ";
                        this.userDetails.RegisteredFrom += user.address.country + " ," || " ";
                    }
                    $("#userDetails").modal("show");
                }
                error => console.log(error)
            });

    }

}