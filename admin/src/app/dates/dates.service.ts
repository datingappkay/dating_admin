import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
import { Configuration } from '../app.constant';
declare var API_URL: string;
@Injectable()
export class DatesService {

    constructor(private http: Http, public _config: Configuration) {
    }

    getProfileById(_id) {
        let url = this._config.Server + 'profile/'+_id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    getSearchFromActiveDates(data) {
        console.log("Active datedata", data)
        let url = this._config.Server + 'searchActiveDates';
        return this.http.post(url, data, { headers: this._config.headers }).map(res => res.json());
    }

    getActiveDateById(_id) {
        let url = this._config.Server + 'activeDate/'+_id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    getCompletedDateById(_id) {
        let url = this._config.Server + 'completedDate/'+_id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getExpiredDateById(_id) {
        let url = this._config.Server + 'expiredDate/'+_id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    getSearchFromCompleteDates(data) {
        let url = this._config.Server + 'searchCompletedDates';
        return this.http.post(url, data, { headers: this._config.headers }).map(res => res.json());
    }

    getSearchFromExpiredDates(data) {
        let url = this._config.Server + 'searchExpiredDates';
        return this.http.post(url, data, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchFromActiveDatesData(data,offset, limit) {
        let url = this._config.Server + 'searchActiveDates' + '?offset=' + offset + '&limit=' + limit;;
        return this.http.post(url, data, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchFromCompleteDatesData(data,offset, limit) {
        let url = this._config.Server + 'searchCompletedDates' + '?offset=' + offset + '&limit=' + limit;;
        return this.http.post(url, data, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchFromExpiredDatesData(data,offset, limit) {
        let url = this._config.Server + 'searchExpiredDates' + '?offset=' + offset + '&limit=' + limit;;
        return this.http.post(url, data, { headers: this._config.headers }).map(res => res.json());
    }

}


// this could also be a private method of the component class
function handleError(error: any) {
    // log error
    // could be something more sofisticated
    let errorMsg = error.message || `Yikes! There was was a problem with our hyperdrive device and we couldn't retrieve your data!`
    console.error(errorMsg);

    // throw an application level error
    return Observable.throw(errorMsg);
}
