import { Routes, RouterModule } from '@angular/router';

import { ActiveDatesComponent } from './activeDates/activeDates.component';

import { ModuleWithProviders } from '@angular/core';

export const datesroutes: Routes = [
  {
    path: '', component: ActiveDatesComponent, data: { title: '' },
   
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(datesroutes);