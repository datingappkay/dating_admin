import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { AppState } from '../../../app.state';
import { FormGroup, FormBuilder, AbstractControl, Validators, FormControl, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { contentHeaders } from '../../../loginComponent/headers';
import { AppService } from '../../../app.service';
declare var swal: any;
declare var sweetAlert: any;
declare var $: any;
declare var jQuery: any;
declare var document: any;
declare var localStorage:any;
@Component({
    selector: 'navbar',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./navbar.scss'],
    templateUrl: './navbar.html'
})

export class Navbar {
    form: FormGroup; // edit
    public oldPassword: AbstractControl;
    public password: AbstractControl;
    public confirmPassword: AbstractControl;
    public modal: boolean = false;
    public token: string;
    public isMenuCollapsed: boolean = false;
    public error: boolean = false;
    public errorMessage: string = '';
    public erro: string = '';

    constructor(private router: Router, public http: Http, private _state: AppState, private formBuilder: FormBuilder, private _AppService: AppService) {
        this.router = router;
        this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
            this.isMenuCollapsed = isCollapsed;
        });
        this.form = formBuilder.group({
            oldPassword: ['', Validators.required],
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required]
            //password: [''],
            // confirmPassword: ['']
        },
            { validator: matchingPasswords('password', 'confirmPassword') }
        );
        this.oldPassword = this.form.controls['oldPassword'];
        this.password = this.form.controls['password'];
        this.confirmPassword = this.form.controls['confirmPassword'];



    }

    clickmodal() {
        this.modal = true;
    }


    public onSubmit(values: Object): void {

        this._AppService.changePassword(values).subscribe(
            result => {
                console.log("result==============>", result)
                if (result) {
                    swal("Success!", "password change Successfully!", "success");
                    jQuery('#ChangPass').modal('hide');
                    this.logout()

                } else {
                    swal("Oops", "current password is wrong!", "error")
                    jQuery('#ChangPass').modal('hide');
                    this.form.reset()

                }

            }
        )

    }



    public toggleMenu() {
        this.isMenuCollapsed = !this.isMenuCollapsed;
        this._state.notifyDataChanged('menu.isCollapsed', this.isMenuCollapsed);


        var x = document.getElementById("myDIV");
        var y = document.getElementById("collapse-menu-link")
        if (x.style.display === "none") {
            x.style.display = "block";
            y.style.margin = "0px 0px 0px -52px";
        } else {
            x.style.display = "none";
            y.style.margin = "0px 0px 0px -175px";
        }
       

    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('adminToken');
        this.router.navigate(['/']);
    }


}

export function emailValidator(control: FormControl): { [key: string]: any } {
    var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if (control.value && !emailRegexp.test(control.value)) {
        return { invalidEmail: true };
    }
}
export function matchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
        let password = group.controls[passwordKey];
        let passwordConfirmation = group.controls[passwordConfirmationKey];
        if (password.value !== passwordConfirmation.value) {
            return passwordConfirmation.setErrors({ mismatchedPasswords: true })
        }
    }
}
