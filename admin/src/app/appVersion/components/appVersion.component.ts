import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, OnInit } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';
//importing service

//=================== importing form components ==================
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
//=================== importing Matches Service   =============
import { AppVersionService } from '../appVersion.service';
import { Console } from '@angular/core/src/console';
import { resolveTxt } from 'dns';
import { type } from 'os';
declare var swal: any;


@Component({
    selector: 'appVersion',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./appVersion.component.scss'],
    templateUrl: './appVersion.component.html',
    providers: [AppVersionService]
})

export class AppVersionComponent implements OnInit, AfterViewInit {
    addResonces: FormGroup;
    editResonces: FormGroup;
    loader = true;
    oldReason: any;
    newReason: any;
    // isMandatory = true;
    data: any;
    id: any
    type: any;
    deleteUserId: any;
    obj = [];
    userCount=[];
    reson: any;
    reportResonceName: any;
    public config: any;
    public configFn: any;

    constructor(private _matchesService: AppVersionService, private _appConfig: AppConfig, private router: Router, vcRef: ViewContainerRef, private fb: FormBuilder) {
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
        this.addResonces = fb.group({
            'reportResonceName': ['', Validators.required],
            'isMandatory': '',
        });

        this.editResonces = fb.group({
            'reportResonceName': ['', Validators.required],
            "isMandatory": '',
        });


    }

    ngOnInit() {
        this.type = 1

        this._matchesService.getData(1).subscribe(result => {
            if (result.version) {
               // console.log("this.data1", result)
                this.data = result.version
                this.userCount =result.userCount
            }
        });
    }
    getVersion(type) {
        this.type = type

        //console.log("==========getdata", this.type)
        this._matchesService.getData(this.type).subscribe(result => {
            if (result.version) {
                this.data = result.version
                this.userCount =result.userCount
               // console.log("this.data2", result)
              //  result.userCount && result.userCount.map(id => id._id.includes(targetUserId)

            }
        })
    }
    addata(type) {
        this.type = type
       // console.log("===========", type)
        jQuery('#addResonce').modal('show');

    }

    submitForm(value) {
        value._value.isMandatory = jQuery('#mandVal').is(':checked');
      //  console.log("val", value._value.isMandatory)
        this._matchesService.addResonce(value._value, this.type).subscribe(
            (result) => {
                if (result.code != 200) {
                    if (this.type == 1) {
                        this.ngOnInit();

                    } else {
                        this.getVersion(this.type)

                    }
                    this.addResonces.reset();
                    swal("Success!", "App Version Added Successfully!", "success");

                } else {

                }
                jQuery('#addResonce').modal('hide');



            })
    }



    showModalDelete(_id) {
        this._matchesService
            .deleteUser(_id)
            .subscribe((result) => {
                if (result.code != 200) {
                    swal("Success!", "App Version Deleted!", "success");
                    if (this.type == 1) {
                        this.ngOnInit();

                    } else {
                        this.getVersion(this.type)
                        
                    }
                } else {

                }

            })
    }


    editResonnce(reason) {
        this.obj = reason;
        this.id = reason._id


    }

    updatedReason(reason) {
        // console.log("reasonreason", reason);

        reason._value.reportResonceName;
        reason._value.isMandatory = jQuery('#mandValZ').is(':checked');
        //  console.log("sagfvGVEST")
        this._matchesService.editReason(reason._value, this.id, this.type).subscribe(
            result => {
                if (result.code != 200) {
                    swal("Success!", "App Version Edited!", "success");
                    if (this.type == 1) {
                        this.ngOnInit();

                    } else {
                        this.getVersion(this.type)

                    }
                    this.editResonces.reset();
                } else {
                }

                jQuery('#editCategory').modal('hide');

            }
        )
    }

}
