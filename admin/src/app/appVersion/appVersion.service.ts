import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../app.constant';

@Injectable()
export class AppVersionService {

    constructor(private http: Http, public _config: Configuration) {
    }


    getData(type) {
     //   console.log("servicesssssssss",type)
        let url = this._config.Server + 'appVersion/'+ type;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }



    addResonce(data, type): Observable<any> {
        let url = this._config.Server + "appVersion";
        let body = {
            appversion: data.reportResonceName,
            isMandatory: data.isMandatory,
            type: type

        }
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }

    deleteUser(Id) {
       // console.log(Id);
        let url = this._config.Server + "appVersion/" + Id;
     //   console.log(url);
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }

    editReason(data, _id,type): Observable<any> {
      //  console.log("data", data)
        let url = this._config.Server + "appVersion";
        let body = {
            appversion: data.reportResonceName,
            isMandatory: data.isMandatory,
            _id: _id,
            type: type

        }
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }
}
