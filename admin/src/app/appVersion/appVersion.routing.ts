import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppVersionComponent } from './components/appVersion.component';
 
 

export const AppVersionRoutes: Routes = [ 
         {
       path: '',
       component: AppVersionComponent,
         data:{
         title: ''
       }
      }
 
];
export const routing: ModuleWithProviders = RouterModule.forChild(AppVersionRoutes); // not required


