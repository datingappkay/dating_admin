// General modules
import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// custom modules
import { LoginModule } from './loginComponent/login.module';
import { UsersModule } from './users/users.module';
import { ProfilesModule } from './profiles/profile.module';
import { MatchesModule } from './matches/matches.module';
import { PreferenceModule } from './preferences/preference.module';
//   import { ReportsModule } from './reportedList/report.module';
//  import { PurchasesModule } from './purchases/purchases.module';
//  import { PriceModule } from './price/price.module';
import { PlanModule } from './plan/plan.module';
import { TextSettingModule } from './textSetting/textSetting.module';
//  import { NotificationssModule } from './notifications/notifications.module';
//  import { coinWalletModule } from './coinWallet/wallet.module';
//  import { coinTransactionModule } from './coinTransactions/coinTransaction.module';
// import { MarketingModule } from "./marketing/marketing.module";


import { routing } from './app.routing';
import { AppConfig } from './app.config';
import { AuthGuard } from './guards/index'; // for authentication

//General
import { AppComponent } from './app.component';
import { PagesComponent } from './pages/pages.component';
import { DashboardComponent } from './dashboardComponent/dashboard.component';

import { PageNotFoundComponent } from './pages/error/pagenotfound.component';
import { Navbar } from './theme/components/navbar/navbar.component';
import { Messages } from './theme/components/messages/messages.component';
import { Sidebar } from './theme/components/sidebar/sidebar.component';
import { Breadcrumb } from './theme/components/breadcrumb/breadcrumb.component';
import { BackTop } from './theme/components/back-top/back-top.component';

//directives
import { SlimScroll } from './theme/directives/slim-scroll/slim-scroll.directive';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { DynamicChartComponent } from './dashboardComponent/barcharts/barChart.component';
import { DynamicChartComponentWeek } from './dashboardComponent/barcharts-week/barChart.component-week';
import { DynamicChartComponentMonth } from './dashboardComponent/barcharts-month/barChart.component';
import { DynamicChartComponentYear } from './dashboardComponent/barcharts-year/barChart.component';
import { DynamicChartComponentLifetime } from './dashboardComponent/barcharts-lifetime/barChart.component';
import { AppVersionComponent } from './appVersion/components/appVersion.component';

import { GeoHeatmap } from './dashboardComponent/heatmaps/heatMap.component';
import { DevType } from './dashboardComponent/doughNuts/devicetype.component';
import { MatchesStat } from './dashboardComponent/doughNuts/matchesDoughNut.component';
import { Configuration } from './app.constant';
import { PolicyModule } from "./policy/policy.module";
import { TermsandConditionsModule } from "./termsandconditions/termsandconditions.module";
import { AppRateModule } from "./appRate/appRate.module"
import { DatesModule } from "./dates/dates.module";
import { LanguageComponents } from './textSetting/languages/languages.components';
import { targetedUserComponent } from './compaign/targetedUser/targetedUser.component';
import { compaignUserPageComponent } from './compaign/compaignUserPage/compaignUserPage.component';
import { compaignModule } from './compaign/compaign.module';
import { viewsCompaignComponent } from './compaign/viewsCompaign/viewsCompaign.component';
import { clickedCompaignComponent } from './compaign/clickedCompaign/clickedCompaign.component';
import { languageModule } from './language/language.module';
import { appConfigModule } from './appConfig/appConfig.module';
import { cdnKeyModule } from './CDN-Key/cdnKey.module';

import { UserLikesModule } from './UserLikes/UserLikes.module';
import { LikePageComponents } from './UserLikes/likePage/likePage.components';
// import {superLikesUserActComponents} from './UserLikes/superLikesUserAct/superLikes.components';
// import {disLikesUserPageComponents} from './UserLikes/disLikesUserPage/disLikesUserPage.components';


import { MatchesUserModule } from './MatchesUser/MatchesUser.module';
import { CommunityGuidelinesModule } from './communityGuidelines/communityGuidelines.module';
import { SafetyTipsModule } from './safetyTips/safetyTips.module';
import { licensesModule } from './licenses/licenses.module';
import { subscriptionModule } from './subscription/subscription.module';
import { CoinModule } from './coin/coin.module';
import { ImageCropperComponent } from 'ng2-img-cropper';
import { keyValuePipe } from './pipes/keyValue.pipe';
import { AppWalletModule } from './AppWallet/AppWallet.module';
import { CoinWalletModule } from './CoinWallet/CoinWallet.module';

@NgModule({
  imports: [
    DatesModule,
    AppRateModule,
    PolicyModule,
    TermsandConditionsModule,
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    LoginModule,
    UsersModule,
    ProfilesModule,
    languageModule,
    MatchesModule,
    PreferenceModule,
    cdnKeyModule,
    // PurchasesModule,
    // PriceModule,
    PlanModule,
    TextSettingModule,
    // NotificationssModule,
    // coinWalletModule,
    ChartsModule,
    //  coinTransactionModule,
    appConfigModule,
    MatchesUserModule,
    UserLikesModule,
    SafetyTipsModule,
    licensesModule,
    compaignModule,
    UserLikesModule,
    CommunityGuidelinesModule,
    subscriptionModule,
    CoinModule,
    CoinWalletModule,
    AppWalletModule


  ],

  declarations: [

    AppComponent,
    PagesComponent,
    PageNotFoundComponent,
    Navbar,
    Messages,
    AppVersionComponent,
    Sidebar,
    DashboardComponent,
    Breadcrumb,
    BackTop,
    SlimScroll,
    DynamicChartComponent,
    DynamicChartComponentWeek,
    DynamicChartComponentMonth,
    DynamicChartComponentYear,
    DynamicChartComponentLifetime,
    GeoHeatmap,
    DevType,

    MatchesStat
  ],
  providers: [
    Title,
    AppConfig,
    AuthGuard,
    Configuration
  ],
  bootstrap: [AppComponent]

})
export class AppModule {
}