import { Component, ViewEncapsulation } from '@angular/core';
import { AppConfig } from "../../app.config";
import {
    DashboardService
} from '../dashboard.service';
@Component({
    selector: 'dynamic-chartLifetime',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './bar-chart.html',
    providers: [DashboardService]

})

export class DynamicChartComponentLifetime {
    public config: any;
    public configFn: any;
    public dashboardData: any;
    public pieChartLabels: string[] = ['Android', 'Ios', 'Web'];
    public pieChartData: number[] = [300, 500, 100];
    public pieChartType: string = 'pie';


    public barChartOptions: any = {
        scaleShowVerticalLines: false,
        responsive: true
    };
    public barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];

    public abcdate = [];
    public barChartType: string = 'bar';
    public barChartLegend: boolean = true;

    public barChartData: any[] = [
        { data: [65, 59, 80, 81, 56, 55, 40], label: 'Number Of User' },
    ];
    constructor(private _dashboardService: DashboardService) {
    }
    ngOnInit() {
        this.func();

    }
    func() {
        this.barChartLabels = [];

        this._dashboardService
            .chart()
            .subscribe((result) => {
                {

                    console.log("the result", result)
                    this.pieChartData = result.device;
                    for (let i = 0; i <= 30; i++) {
                        this.barChartLabels.push(result.datazzz[i]);
                        // this.barChartLabels = result.datazzz;

                        this.barChartData = result.count;
                    }


                }

            });
    }
    // private getuserMatchDashboard(): void {
    //     this._dashboardService
    //         .getuserMatchDashboard()
    //         .subscribe((result) => {
    //             {
    //                 if (result.data) {
    //                     this.dashboardData = result.data.report;
    //                     jQuery(".iframeListuseractivity").attr("src", this.dashboardData);                
    //                 } else {
    //                 } 

    //             }

    //         })
    // }
    public chartClicked(e: any): void {
        //   console.log(e);
    }

    public chartHovered(e: any): void {
        // console.log(e);
    }

    //public randomize() {
    // Only Change 3 values
    // let data = [
    //     Math.round(Math.random() * 100),
    //     59,
    //     80,
    //     (Math.random() * 100),
    //     56,
    //     (Math.random() * 100),
    //     40];
    // let clone = JSON.parse(JSON.stringify(this.barChartData));
    // clone[0].data = data;
    // this.barChartData = clone;
    /**
     * (My guess), for Angular to recognize the change in the dataset
     * it has to change the dataset variable directly,
     * so one way around it, is to clone the data, change it and then
     * assign it;
     */
    // }
}
