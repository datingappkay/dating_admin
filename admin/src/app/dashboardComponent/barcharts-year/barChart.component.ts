import { Component, ViewEncapsulation } from '@angular/core';
import { AppConfig } from "../../app.config";
import {
    DashboardService
} from '../dashboard.service';
@Component({
    selector: 'dynamic-chartYear',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './bar-chart.html',
    providers: [ DashboardService]

})

export class DynamicChartComponentYear {
        
    public config: any;
    public configFn: any;
    public dashboardData: any;

    constructor( private _dashboardService: DashboardService) {
    }
    ngOnInit() {
        this.getuserCoinDashboard()
    }
    private getuserCoinDashboard(): void {
        this._dashboardService
            .getuserCoinsDashboard()
            .subscribe((result) => {
                {
                    if (result.data) {
                        this.dashboardData = result.data.report;
                        jQuery(".iframeListuseractivity").attr("src", this.dashboardData);                
                    } else {
                    } 

                }

            })
    }

}
