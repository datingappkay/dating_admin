import { Component ,OnInit, Input } from '@angular/core';
import { AppConfig } from "../../app.config";
import { MatchesService } from '../../matches/matches.service';
@Component({
  selector: 'MatchesStat',
   template: `
    <div style="display: block">
  <canvas baseChart
          [data]="pieChartData"
          [labels]="pieChartLabels"
          [options]="lineChartOptions"
          [colors]="colors"
          [chartType]="pieChartType"
    
         ></canvas>     
</div>
  `,
     providers: [MatchesService] 
})
//      (chartHover)="chartHovered($event)"  (chartClick)="chartClicked($event)"

export class MatchesStat implements OnInit {
      @Input() dashboardData:any;
        @Input() unMatchesData:any;
         @Input() blockedData:any;
  // Pie
     constructor(private _appConfig:AppConfig,private _matchesService:MatchesService){
         
    } 
     public arr = []
  public pieChartData:number[];
public m;
   public pieChartType:string = 'doughnut';
  public colors: any[] = [
      { 
        backgroundColor:["#46bfbd" ,"rgb(255, 157, 63)","rgb(254, 91, 63)"] 
      }];

       ngOnInit() {
  this.pieChartData =[this.dashboardData,this.unMatchesData,this.blockedData];
 
    }
  public pieChartLabels:string[] = ['Matches','UnMatches','Blocked'];
      

 

      



  public lineChartOptions:any = {
    legend : {
        labels : {
          fontColor : '#aaa'  
        }
    }
};



}
 