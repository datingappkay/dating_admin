import { Component, ViewEncapsulation, OnInit, Input } from '@angular/core';
import { AppConfig } from "../app.config";
//importing service
import {
    UsersService
} from '../users/users.service';
import {
    PreferenceService
} from '../preferences/preference.service';
//=================== importing Matches Service   =============
import { MatchesService } from '../matches/matches.service';
import {
    DashboardService
} from './dashboard.service';


@Component({
    selector: 'dashboardData',
    encapsulation: ViewEncapsulation.None,
    // styleUrls: ['./dashboard.component.scss'],
    templateUrl: './dashboard.component.html',
    providers: [UsersService, PreferenceService, MatchesService, DashboardService]
})

export class DashboardComponent implements OnInit {
    public dashboardData: any;
    private usersStat: number = 1;
    private matchesStat: number = 1;
    public childTitle: string;
    public config: any;
    public configFn: any;
    public bgColor: any;
    public date = new Date();
    public weatherData: any;
    //   public dashboardData:any;
    public users: any;
    public preferences: any;
    public profMatches: any;
    constructor(private _appConfig: AppConfig, private _usersService: UsersService, private _dashboardService: DashboardService, private _preferenceservice: PreferenceService, private _matchesService: MatchesService) {
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
    }
    ngOnInit() {
        this.getDashboardData()
    }
    private getDashboardData(): void {
        this._dashboardService
            .getDashboardData()
            .subscribe((result) => {
                {
                    if (result.data) {
                        this.dashboardData = result.data.report;
                        jQuery(".iframeList").attr("src", this.dashboardData);                
                    } else {
                    } 

                }

            })
    }
    private getUsers(): void {
        this._usersService
            .getAllUsers()
            .subscribe((data: any[]) => {
                {
                    if (data) {
                        this.users = data;
                    } else {
                        this.users = [];
                    }// console.log(this.users)

                }

            })
    }
    private getPreferences(): void {
        this._preferenceservice
            .getAll()
            .subscribe((data: any[]) => {
                {
                    if (data) {
                        this.preferences = data;
                    } else {
                        this.preferences = [];
                    }    //  console.log(data)

                }

            })
    }

    getMatches() {
        this._matchesService
            .getMatches()
            .subscribe((data: any) => {
                if (data.errCode == 201) {
                    this.profMatches = data.response.data

                } else {
                    this.profMatches = []
                }
            })

    }


}
