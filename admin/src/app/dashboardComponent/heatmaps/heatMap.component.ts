import { Component,Input } from '@angular/core';
import {
    DashboardService
} from '../dashboard.service';

declare var google: any;

@Component({
  selector: 'heatmap',
  
  templateUrl: './heatMap.html',
  styleUrls: ['./heatMap.css'],
})
export class GeoHeatmap {
    @Input() title2:string;
      @Input() dashboardData:any;
      private heatMapData:any;
  title = 'Heat Map';
  map:any;
  heatmap:any;
  that=this;

    constructor(private _dashboardService: DashboardService)    {
      
        } 


  ngOnInit() {
   
//     console.log('this.dashboardData')
// console.log(this.dashboardData)
  this.map = new google.maps.Map(document.getElementById('map'), {
    zoom: 2,
    radius:'90',
    center: {lat: 12.9116, lng: 77.5946},
  });

  this.heatmap = new google.maps.visualization.HeatmapLayer({
    // data: this.getPoints(),
   data: this.getPoints(),
    // data:  [
    // new google.maps.LatLng( 13.029183699999999, 77.59058569999999),
 
    // new google.maps.LatLng( 12.9116, 77.5846),
    // ],
 
    map: this.map
  });

}
  getHeatMapData() {
        this._dashboardService
            .getHeatMapData()
            .subscribe((data) => {
                {
                    if (data.response.data) {
                       //  this.heatMapData = data.response.data;
                       // console.log(this.heatMapData)
                        var arr = [] 
// for(var i=0;i<data.response.data.length;i++){
 
//console.log(data.response.data[i].latitude)
  
// arr.push(new google.maps.LatLng( data.response.data[i].latitude, data.response.data[i].longitude))
return [ new google.maps.LatLng( 13.59058569999999, 77.59058569999999)];
// console.log(data.response.data[i].latitude,data.response.data[i].longitude)
 
// }
                         
 
//  console.log(JSON.stringify(arr)) 
//  return arr;
          
                } else {
                      this.heatMapData = [];
                    } //console.log(this.dashboardData)

                }

            })
    }
toggleHeatmap() {
  this.heatmap.setMap(this.heatmap.getMap() ? null : this.map);
}


 
darkStyle(){
 var styles: [
            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
            {
              featureType: 'administrative.locality',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'geometry',
              stylers: [{color: '#263c3f'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [{color: '#6b9a76'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [{color: '#38414e'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry.stroke',
              stylers: [{color: '#212a37'}]
            },
            {
              featureType: 'road',
              elementType: 'labels.text.fill',
              stylers: [{color: '#9ca5b3'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [{color: '#746855'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry.stroke',
              stylers: [{color: '#1f2835'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [{color: '#f3d19c'}]
            },
            {
              featureType: 'transit',
              elementType: 'geometry',
              stylers: [{color: '#2f3948'}]
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [{color: '#17263c'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [{color: '#515c6d'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.stroke',
              stylers: [{color: '#17263c'}]
            }
          ];
          this.that.map.set('styles', this.that.map.get('styles') ? styles : styles);
}

 changeGradient() {
  var gradient = [
    'rgba(0, 255, 255, 0)',
    'rgba(0, 255, 255, 1)',
    'rgba(0, 191, 255, 1)',
    'rgba(0, 127, 255, 1)',
    'rgba(0, 63, 255, 1)',
    'rgba(0, 0, 255, 1)',
    'rgba(0, 0, 223, 1)',
    'rgba(0, 0, 191, 1)',
    'rgba(0, 0, 159, 1)',
    'rgba(0, 0, 127, 1)',
    'rgba(63, 0, 91, 1)',
    'rgba(127, 0, 63, 1)',
    'rgba(191, 0, 31, 1)',
    'rgba(255, 0, 0, 1)'
  ]
  this.heatmap.set('gradient', this.heatmap.get('gradient') ? null : gradient);
}

 changeRadius() {
  this.heatmap.set('radius', this.heatmap.get('radius') ? null : 20);
}

 changeOpacity() {
  this.heatmap.set('opacity', this.heatmap.get('opacity') ? null : 0.2);
}

// fifteenMinsLatLong(){
//    this.heatmap.set('data', this.heatmap.get('data') ? this.changeLatLong.pastFifteenMins :this.getPoints() );
//  }
 
// thirtyminsLatLong(){
//    this.heatmap.set('data', this.heatmap.get('data') ? this.changeLatLong.pastThirtyMins :this.getPoints() );
//  }

//  fourtyMinsLatLong(){
//    this.heatmap.set('data', this.heatmap.get('data') ? this.changeLatLong.pastFourtyMins :this.getPoints() );
//  }

//  OneHourLatLong(){
//    this.heatmap.set('data', this.heatmap.get('data') ? this.changeLatLong.pastThirtyMins :this.getPoints() );
//  }

//  TwohoursLatLong(){
//    this.heatmap.set('data', this.heatmap.get('data') ? this.changeLatLong.pastFourtyMins :this.getPoints() );
//  }

setlive(){
   this.heatmap.set('data', this.heatmap.get('data') ? this.getPoints() :this.getPoints() );
 }

changeLatLong={
  

}

// Heatmap data: 500 Points
 getPoints() {
  return [
    new google.maps.LatLng( 13.029183699999999, 77.59058569999999),
 
    new google.maps.LatLng( 12.9116, 77.5846),
    // new google.maps.LatLng( 12.9116, 77.5940),
    // new google.maps.LatLng( 12.9116, 77.5916),
    // new google.maps.LatLng( 12.9116, 77.5946),
    // new google.maps.LatLng( 12.9116, 77.5903),
    // new google.maps.LatLng( 12.9116, 77.5992),
    // new google.maps.LatLng( 12.9116, 77.5981),
    // new google.maps.LatLng( 12.9116, 77.5970),
    // new google.maps.LatLng( 12.9116, 77.5969),
    // new google.maps.LatLng( 12.9116, 77.5958),
    // new google.maps.LatLng( 12.9116, 77.5947),
    // new google.maps.LatLng( 12.9116, 77.5936),
    // new google.maps.LatLng( 12.9116, 77.5925),
    // new google.maps.LatLng( 12.9116, 77.5914),
    // new google.maps.LatLng( 12.9116, 77.5901),
    // new google.maps.LatLng( 12.9116, 77.5990),
    // new google.maps.LatLng( 12.9116, 77.5989),
    // new google.maps.LatLng( 12.9116, 77.5978),
    // new google.maps.LatLng( 12.9116, 77.5967),
    // new google.maps.LatLng( 12.9116, 77.5956),
    // new google.maps.LatLng( 12.9116, 77.5945),
    // new google.maps.LatLng( 12.9116, 77.5934),
    // new google.maps.LatLng( 12.9116, 77.5922),
    // new google.maps.LatLng( 12.9116, 77.5912),
    // new google.maps.LatLng( 12.9116, 77.5984),
    // new google.maps.LatLng( 12.9116, 77.5980),
    // new google.maps.LatLng( 12.9116, 77.5986),
    // new google.maps.LatLng( 12.9118, 77.5946),
    // new google.maps.LatLng( 12.9115, 77.5906),
    // new google.maps.LatLng( 12.9117, 77.5846),
    // new google.maps.LatLng( 12.9166, 77.5940),
    // new google.maps.LatLng( 12.9156, 77.5916),
    // new google.maps.LatLng( 12.9176, 77.5946),
    // new google.maps.LatLng( 12.9186, 77.5903),
    // new google.maps.LatLng( 12.9196, 77.5992),
    // new google.maps.LatLng( 12.9106, 77.5981),
    // new google.maps.LatLng( 12.9126, 77.5970),
    // new google.maps.LatLng( 12.9136, 77.5969),
    // new google.maps.LatLng( 12.9146, 77.5958),
    // new google.maps.LatLng( 12.9111, 77.5947),
    // new google.maps.LatLng( 12.9115, 77.5936),
    // new google.maps.LatLng( 12.9120, 77.5925),
    // new google.maps.LatLng( 12.9125, 77.5914),
    // new google.maps.LatLng( 12.9130, 77.5901),
    // new google.maps.LatLng( 12.9135, 77.5990),
    // new google.maps.LatLng( 12.9140, 77.5989),
    // new google.maps.LatLng( 12.9145, 77.5918),
    // new google.maps.LatLng( 12.9150, 77.5927),
    // new google.maps.LatLng( 12.9155, 77.5996),
    // new google.maps.LatLng( 12.9165, 77.5935),
    // new google.maps.LatLng( 12.9175, 77.5924),
    // new google.maps.LatLng( 12.9189, 77.5942),
    // new google.maps.LatLng( 12.9190, 77.5952),
    // new google.maps.LatLng( 12.9145, 77.5954),
    // new google.maps.LatLng( 12.9108, 77.5990),
    // new google.maps.LatLng( 12.9148, 77.5906),
       new google.maps.LatLng( 56.130366, -106.34677099999999)
    
  ];
 }


}