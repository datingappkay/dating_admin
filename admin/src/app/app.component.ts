import './app.loader.ts';

import { Component, ViewEncapsulation } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
//importing service
import {
    AppService
} from './app.service';
@Component({
    selector: 'app',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./app.scss'],
    template: '<router-outlet></router-outlet>',
    providers: [AppService]

})

export class AppComponent {
    public data: any;
    constructor(public router: Router, private _AppService: AppService) {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this._AppService
                    .tokenVerification()
                    .subscribe((data) => {
                        console.log("Token Api data", data);

                        if (data.code == "401") {
                            this.router.navigate(['']);
                        }
                    });
            }
        });

    }

    ngOnInit() {
    }

}

