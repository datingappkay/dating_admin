import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
import { Configuration } from '../app.constant';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

//import {HttpClient, HttpParams, HttpRequest, HttpEvent, Http, Response, Headers, RequestOptions, RequestMethod} from '@angular/common/http';
declare var $q: any;

declare var API_URL: string;
@Injectable()
export class UsersService {
    // private BASE_URL: string = myGlobals.images_admin+'/datum_2.0-admin/usersFilters?userType=1/';
    constructor(private http: Http, public _config: Configuration) {


    }

    // getAllUser() {
    //     let url = this._config.Server + 'usersFilters?userType=1';
    //     return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    // }

    // getAllUsers(): Observable<any[]> {
    //     return this.http
    //         .get(API_URL + 'usersFilters?userType=1', { headers: contentHeaders })
    //         .map((response: Response) => <any[]>response.json())
    //         .catch(handleError);
    // }
    // getAllUsersP(n, seachControl, flagg) {
    //     let body = JSON.stringify({ pageNum: n, searchP: seachControl, flag: flagg });
    //     console.log()
    //     var term = seachControl ? seachControl : 0;
    //     let url = this._config.Server + "usersFilters?userType=1";
    //     return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    // }
    getLikesByFbId(id) {
        let Url = this._config.Server + "liksBy/" + id;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }

    getSuperLikesByFbId(id) {
        let Url = this._config.Server + "supperliksBy/" + id;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getMySuperLikesByFbId(id) {
        let Url = this._config.Server + "mysupperliksBy/" + id;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getUnLikesByFbId(id) {
        let Url = this._config.Server + "disLikes/" + id;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getMyDisLikesById(id) {
        let Url = this._config.Server + "myDisLikes/" + id;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getMyLikesById(id) {
        let Url = this._config.Server + "myLikes/" + id;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getMatchedUsers(id) {
        let Url = this._config.Server + "match/" + id;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    DeleteUser(id) {
        console.log("delete iddddddd", id);
        let Url = this._config.Server + "userMultipleDelete";
        let body = {
            usersId: id.user._id
        }
        console.log("after Bodyyy", id)
        return this.http.post(Url, body, { headers: this._config.headers }).map(res => res.json());
    }
    getMatchesByFbId(id) {
        let options = new RequestOptions({
            headers: contentHeaders
        });
        let Url = API_URL + "/users/matches/";
        let UrlWithId = Url + id;
        return this.http.get(UrlWithId, options)
            .map(res => res.json());
    }
    getViewsByFbId(id) {
        let options = new RequestOptions({
            headers: contentHeaders
        });
        let Url = API_URL + "/users/views/";
        let UrlWithId = Url + id;
        return this.http.get(UrlWithId, options)
            .map(res => res.json());
    }
    getRecentVisitors(id) {
        let Url = this._config.Server + "recentVisitors/" + id;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }

    search(seachControl) {
        // let options = new RequestOptions({
        //     method: RequestMethod.Post,
        //     headers: contentHeaders
        // });
        let body = { text: seachControl.search };
        let Url = API_URL + "activeUsers";
        return this.http.post(Url, body, { headers: this._config.headers })
            .map(res => res.json())
    }


    searchUser(queryString: string) {
        let Url = this._config.Server + "usersFilters?userType=1/" + '';
        let _URL = Url + queryString;
        return this.http.get(_URL, { headers: this._config.headers })
            .map(res => res.json());
    }
    deleteUser(UserId) {
        let url = this._config.Server + "userMultipleDelete"
        console.log("before", UserId)
        let body = {
            usersId: UserId
        }
        console.log("before", UserId)
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }



    updateUser(value, id) {
        let url = this._config.Server + "profile";
        let body = value;
        body["_id"] = id;
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }

    putUnBannedUser(_id) {
        let url = this._config.Server + "unBannedUser";

        return this.http.put(url, { _id: _id }, { headers: this._config.headers }).map(res => res.json());

    }

    banUser(id) {
        let url = this._config.Server + "bannedAllUsers";
        let body = {
            _id: id


        };
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }


    // checkMail(email) {
    //     let options = new RequestOptions({
    //         headers: contentHeaders
    //     });
    //     let body = email;
    //     let Url = API_URL + "/emailIdExistsVerificaiton";
    //     return this.http.patch(Url, body, options)
    //         .map(res => res.json())
    // }
    checkNumber(number) {
        let options = new RequestOptions({
            headers: contentHeaders
        });
        let body = number;
        let Url = API_URL + "/phoneNumberExistsVerificaton";
        return this.http.patch(Url, body, options)
            .map(res => res.json())
    }
    getPrefByUserId(user) {
        let options = new RequestOptions({
            headers: contentHeaders
        });
        let body = JSON.stringify({ _id: user });
        let Url = API_URL + "/users/search";
        return this.http.post(Url, body, options)
            .map(res => res.json());
    }
    getUserById(id) {
        let url = this._config.Server + 'editUserData/' + id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getLastLoginDetails(id) {
        let url = this._config.Server + 'lastLoginDetails/' + id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }



    getReportedUserDetails(_id) {
        let url = this._config.Server + 'reportedUsers/' + _id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());

    }
    getBlockedUserDetails(_id) {
        let url = this._config.Server + 'blockedUsers/' + _id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());

    }
    getAllLiked() {
        let url = this._config.Server + 'liksBy';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());

    }
    getAllDisLikedUsers() {
        let url = this._config.Server + 'disLikes';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    getAllRecentVisters() {
        let url = this._config.Server + 'recentVisitors';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getAllMatchUsers() {
        let url = this._config.Server + 'match';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getProfileById(id) {
        // var fdsfid = id._id
        //console.log("profileeeeeeeeeeeeee=================>", id)
        let url = this._config.Server + 'profile/' + id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getProfileByContectNumber(contectNumber) {
        console.log("profileeeeeeeeeeeeee", contectNumber)
        let url = this._config.Server + 'profile/contectNumber/' + contectNumber;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getProfileByEmail(email) {
        let url = this._config.Server + 'profile/email/' + email;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    getUserStatus(id) {
        let options = new RequestOptions({
            headers: contentHeaders
        });
        let Url = API_URL + "/users/profileStatus/";
        let UrlWithId = Url + id;
        return this.http.get(UrlWithId, options)
            .map(res => res.json());
    }
    getDeviceLogById(id) {
        let url = this._config.Server + "device/" + id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    deleteImage(data) {
        let options = new RequestOptions({
            method: RequestMethod.Delete,
            headers: contentHeaders
        });
        let body = data;
        console.log(body);
        let Url = API_URL + "/image";
        return this.http.post(Url, body, options)
            .map(res => res.json());
    }

    uploadImage(image) {
        let url = this._config.Server + "uploadImage";
        // let  headers:{'Content-Type':'image/jpeg'};
        console.log(url);
        // let body = {image:data.fullpath};
        return this.http.post(url, image).map(res => res.json());

    }
    videoUpload(video) {
        let url = this._config.Server + "uploadVideo";
        // let  headers:{'Content-Type':'image/jpeg'};
        console.log(url);
        // let body = {image:data.fullpath};
        return this.http.post(url, video, { headers: this._config.headers }).map(res => res.json());

    }
    sendNotification(message, usersId) {
        let url = this._config.Server + "notification";
        let body = {
            campainTitle: message.line2,
            message: message.message,
            usersId: usersId,
        }

        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());

    }





    getDashboardData() {
        let options = new RequestOptions({
            headers: contentHeaders
        });
        let Url = API_URL + "/dashboard";
        //let UrlWithId= Url + id;
        return this.http.get(Url, options)
            .map(res => res.json());
    }

    CreateUser(user, myPreference) {

        let url = this._config.Server + 'users';
        let body
            = {


            profilePic: user.profilePic || 'https://confighelloperfect.co/campagin.png',
            firstName: user.firstName || "N/A",
            lastName: user.lastName || "N/A",
            dob: user.dob || "N/A",
            email: user.email || "N/A",
            city: user.city || "N/A",
            country: user.country || "N/A",
            contactNumber: user.contactNumber || "N/A",
            countryCode: user.countryCode || "N/A",
            gender: user.gender || "N/A",
            height: user.height || "152",
            //  heightInFeet: user.heightInFeet 
            latitude: parseFloat(user.lat) || "77.5894775789696",
            longitude: parseFloat(user.lng) || "13.028583716676",
            profileVideo: user.profileVideo || "N/A",
            myPreferences: myPreference || "",
            // searchPreferences:searchPreferences

            // profilePic: value.fullpath || "https://www.bythewayers.com/images/default-user.png",


        }
        console.log("dtrdrdrdrdrdsrddr", body);
        //console.log("successfully submitted");
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());

    }
    getSearchFromActiveUser(user) {
        console.log("^^^^^^^^^^^^^^^^", user)

        let url = this._config.Server + 'usersFilters?userType=1' + '&dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchFromBanDates(user) {
        console.log("^^^^^^^^^^^^^^^^", user)

        let url = this._config.Server + 'usersFilters?userType=2' + '&dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchFromDeactivateDates(user) {
        console.log("^^^^^^^^^^^^^^^^", user)

        let url = this._config.Server + 'usersFilters?userType=3' + '&dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchFromReportedDates(user) {
        console.log("^^^^^^^^^^^^^^^^", user)

        let url = this._config.Server + 'usersFilters?userType=4' + '&dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchFromBlockedDates(user) {
        console.log("^^^^^^^^^^^^^^^^", user)

        let url = this._config.Server + 'usersFilters?userType=5' + '&dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchFromDeletedDates(user) {
        console.log("^^^^^^^^^^^^^^^^", user)

        let url = this._config.Server + 'usersFilters?userType=6' + '&dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getUser(offset, limit) {
        let url = this._config.Server + 'usersFilters?userType=1' + '&offset=' + offset + '&limit=' + limit;

        return this.http.get(url, { headers: this._config.headers })
            .map(res => res.json());

    }
    getBannedUsers(offset, limit) {
        let url = this._config.Server + 'usersFilters?userType=2' + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());

    }
    getReportedUsers(offset, limit) {
        let url = this._config.Server + 'usersFilters?userType=4' + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getDeactivateUsers(offset, limit) {
        let Url = this._config.Server + 'usersFilters?userType=3' + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getBlockUsers(offset, limit) {
        let Url = this._config.Server + 'usersFilters?userType=5' + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getDeleteUsers(offset, limit) {
        let Url = this._config.Server + 'usersFilters?userType=6' + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getUserByActivetext(text) {
        let Url = this._config.Server + "usersFilters?userType=1" + '&text=' + text;
        // if (text == "") {
        //     Url = this._config.Server + "users";
        // }

        console.log(text)
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getUserByBantext(text) {
        let Url = this._config.Server + "usersFilters?userType=2" + '&text=' + text;


        console.log(text)
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getUserByReportedtext(text) {
        let Url = this._config.Server + "usersFilters?userType=4" + '&text=' + text;


        console.log(text)
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getUserByDeacttext(text) {
        let Url = this._config.Server + "usersFilters?userType=3" + '&text=' + text;
        console.log(text)
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getUserByBlocktext(text) {
        let Url = this._config.Server + "usersFilters?userType=5" + '&text=' + text;


        console.log(text)
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getUserByDeletetext(text) {
        let Url = this._config.Server + "usersFilters?userType=6" + '&text=' + text;

        console.log(text)
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }

    getPreferences() {
        let Url = this._config.Server + 'preferences';
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());

    }
    getRecentVisitorpagination(id, offset, limit) {
        let Url = this._config.Server + "recentVisitors/" + id + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getSuperLikespagination(id, offset, limit) {
        let Url = this._config.Server + "supperliksBy/" + id + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getReporteduserpagination(_id, offset, limit) {
        let url = this._config.Server + 'reportedUsers/' + _id + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());

    }
    getMySuperLikespagination(id, offset, limit) {
        let Url = this._config.Server + "mysupperliksBy/" + id + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getMyLikespagination(id, offset, limit) {
        let Url = this._config.Server + "myLikes/" + id + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getMyDisLikespagination(id, offset, limit) {
        let Url = this._config.Server + "myDisLikes/" + id + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getMatchedUserspagination(id, offset, limit) {
        let Url = this._config.Server + "match/" + id + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getLikespagination(id, offset, limit) {
        let Url = this._config.Server + "liksBy/" + id + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getUnLikespagination(id, offset, limit) {
        let Url = this._config.Server + "disLikes/" + id + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getDeviceLogpagination(id, offset, limit) {
        let url = this._config.Server + "device/" + id + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getBlockedUserpagination(_id, offset, limit) {
        let url = this._config.Server + 'blockedUsers/' + _id + '&offset=' + offset + '&limit=' + limit;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());

    }
    getplans() {
        let url = this._config.Server + "plan?planType=Active";
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    addProUserPlan(Ids, planData) {
        let url = this._config.Server + "proUser";
        let body = {
            planId: planData._id,
            durationInMonths: planData.durationInMonths,
            planName: planData.planName,
            userId: Ids,
            cost: planData.cost,
            currencySymbol: planData.currencyCode,
            currencyCode: planData.currencySymbol,
            likeCount: planData.likeCount,
            rewindCount: planData.rewindCount,
            superLikeCount: planData.superLikeCount

        }
        // console.log("bodyyyyyyyyyyyyyyyyy",body)
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    uploadImgInServer(imgData){
        let url = this._config.Server + "UploadImgInServer";
        let body = {
            activeimage:imgData
        }

        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());

    }
}


function handleError(error: any) {
    // log error
    // could be something more sofisticated
    let errorMsg = error.message || `Yikes! There was was a problem with our hyperdrive device and we couldn't retrieve your data!`
    console.error(errorMsg);

    // throw an application level error
    return Observable.throw(errorMsg);
}
