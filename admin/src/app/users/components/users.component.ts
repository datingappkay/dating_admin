import { Component, ViewEncapsulation, Input, Output, ViewChild, Inject, OnInit, ElementRef, NgZone, ChangeDetectorRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppConfig } from "../../app.config";
import {
    Router
} from '@angular/router';
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';

import { NouisliderModule } from 'ng2-nouislider';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
    FormArray
} from '@angular/forms';
import {
    FileUploader, FileSelectDirective
} from 'ng2-file-upload';
import {
    AgmCoreModule,
    MapsAPILoader
} from 'angular2-google-maps/core';
import {
    ImageCropperComponent,
    CropperSettings,
    Bounds
} from 'ng2-img-cropper';
import * as angular from "angular";

//importing service
import {
    UsersService
} from '../users.service';
import {
    PreferenceService
} from '../../preferences/preference.service';
//validations
import { ProfilesService } from '../../profiles/profile.service';
import { Subject } from 'rxjs/Subject'

import {
    AppState
} from "../../app.state";


import "../../../../node_modules/jquery/dist/jquery-1.9.1.js";
import "../../../../node_modules/jquery/dist/jquery-ui.js";

import { Pipe } from '@angular/core';
import { UserType } from '../../pipes/usertype.pipe';
import { Console } from '@angular/core/src/console';



declare var google: any;
declare var jQuery: any;
declare var swal: any;
declare var XMLHttpRequest: any;
declare var XLSX: any;

// declare var Promise: any;
declare var $: any;

@Component({
    selector: 'users',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./users.component.scss'],
    templateUrl: './user.component.html',
    providers: [UsersService, PreferenceService, ProfilesService]


})

@Pipe({
    name: 'filter'
})

export class UsersComponent implements OnInit {
    @ViewChild('videoPlayer') videoplayer: any;

    private isUploadBtn: boolean = true;
    public prefsettings: any[];
    public height = [];
    public valuesExist: boolean = false;
    public valuesExistUser: boolean = false;

    public location: any;

    public model: any = { date: { year: 2018, month: 10, day: 9 } };
    public disabled: boolean = false;
    public someValue: number = 5;
    public someMin: number = -10;
    public someMax: number = 10;
    public rowsOnPage = 10;
    public p = 1;
    public totalusers;
    public userProPlan: any
    cropperSettings2: CropperSettings;
    public PreferenceData: any = [];
    public PreferenceArrayData: any = [];
    public UserDetails;
    public PageDetails;
    public id: any;
    planData = [];
    public pageNum: number = 1;
    public limit: number = 10;
    public place: any;
    public let: any;
    public long: any;
    Aimage = '';
    public flag: number = 0;
    public count: any;
    public usersCountData: any;
    public likesData: any;
    public viewsData: any;
    public profileExist: any;
    public unLikesData: any;
    public matchesData: any;
    public image: any;

    private name: any;
    public checkBtn: any;
    elementRef: ElementRef;
    form1: FormGroup;
    myForm: FormGroup;
    myFormUpDateUser: FormGroup;
    addUser: FormGroup;
    private editUserForm: FormGroup; // edit
    private userDetailForm: FormGroup;
    public notificationM: FormGroup;
    public flage: string;
    private prefID: any;
    public users: any[];
    public prefData = [];
    public proPlanData: any;
    public userLog: any[];
    public reportedData: any[];
    public deactUser: any[];
    public banUsers: any[];
    public blockData: any[];
    public deleteData: any[];
    searchControl = new FormControl(); // search
    email = new FormControl();
    bannedError = "";
    public preferences: any[];
    public prefUser: any[];
    public preferencesUp: any[];
    public prefUpUser: any[];
    public autocompleted: any;
    public prefsettingsUser: any[];
    public addForm: boolean = false;
    private values: any[] = []; // me
    private valuesUser: any[] = []; // me
    private imageurl: any[] = [];
    private videourl: any[] = [];
    public minAge: any;

    private userById: any; // for edit

    public mapmodal: boolean;
    public mapmodalUp: boolean;
    public alertMsg: boolean;
    public msg: string;
    public activePageTitle: string = '';
    public Btn: string = '';
    public file_srcs: any[] = []; // me
    public file_srcsUp: any[] = []; // me
    public debug_size_before: any[] = []; // me
    public debug_size_after: any[] = []; // me
    public croppingBox: boolean = false;
    public croppingBoxUp: boolean = false;
    public croppingBoxPush: boolean = false;
    public detailsBoxPush: boolean = false;
    public searchEnabled = 0;
    public searchTerm = '';
    //for cropping images
    cropperSettings: CropperSettings;
    private data: any;
    private dataUp: any;
    private data2: any;
    private loader: boolean = false;
    private user: any;
    private someRange: any = [0, 1000];
    public slide: any;
    // public height = ["4.0(121 cm)", "4.1(124 cm)", "4.2(127 cm)", "4.3(129 cm)", "4.4(132 cm)", "4.5(134 cm)",
    //     "4.6(137 cm)", "4.7(139 cm)", "4.8(142 cm)", "4.9(144 cm)", "4.10(147 cm)", "4.11(149 cm)",
    //     "5.0(152 cm)", "5.1(154 cm)", "5.2(157 cm)", "5.3(160 cm)", "5.4(162 cm)", "5.5(165 cm)",
    //     "5.6(167 cm)", "5.7(170 cm)", "5.8(172 cm)", "5.9(175 cm)", "5.10(177 cm)", "5.11(180 cm)",
    //     "6.0(182 cm)", "6.1(185 cm)", "6.2(187 cm)", "6.3(190 cm)", "6.4(193 cm)", "6.5(195 cm)",
    //     "6.6(198 cm)", "6.7(200 cm)", "6.8(203 cm)", "6.9(205 cm)", "6.10(208 cm)",
    //     "6.11(210 cm)", "7.0(213 cm)"];
    public myDatePickerOptions: any;
    // public valuesExist: boolean = false;
    // public valuesExistUser: boolean = false;
    private ids: any[] = [];
    public favPreference = [];
    public sliderBtn = [];
    deleteUserId = "";
    banndUserId = "";
    url:any;
    banndUserName = "";
    banndemail = "";
    emailAreadyTaken = false;
    numberAreadyTaken = false;
    public deleteUser = [];
    public fullImagePath = "";
    public fullVideoPath = "";
    public myPlayer: any;
    public playerHTML: any;
    public totalItem: any;
    cloudinaryImage: any;
    uploader: CloudinaryUploader = new CloudinaryUploader(
      new CloudinaryOptions({ cloudName: 'diy6vdafb', uploadPreset: 'zf6dyjoi' })
    );
  
    date: Date;
    toggleBool: boolean = true;
    editUserObj = { favoritePreferences: [] };
    // user details object
    userDetails = {
        Name: "Dipen Ahir",
        PhoneNo: "+919672829202",
        Email: "dipen@mobifyi.com",
        RegistrationDate: "Friday, January 5, 2018 5:39:37 AM",
        DateOfBirth: "17/08/1992",
        LastLogin: "Friday, January 5, 2018 5:39:37 AM",
        Age: "25",
        Gender: "Male",
        Height: "--",
        city: "",
        about: "",
        country: "",
        ProfileVideo: "https://s3.amazonaws.com/sync1to1/SYNC1TO1Video20180104114749AM.mp4",
        RegisteredFrom: "   ",
        ProfilePhoto: "",
        OtherPhotos: [
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png"
        ],
        videoGallery: [],
        OtherImages: [],
        Preferences: [

        ]
    }
    // ReporteduserDetails object
    ReporteduserDetails = {
        Name: "Dipen Ahir",
        PhoneNo: "+919672829202",
        Email: "dipen@mobifyi.com",
        RegistrationDate: "Friday, January 5, 2018 5:39:37 AM",
        DateOfBirth: "17/08/1992",
        LastLogin: "Friday, January 5, 2018 5:39:37 AM",
        Age: "25",
        Gender: "Male",
        Height: "166 cm , 5'6\" feet",
        city: "",
        about: "",
        country: "",
        ProfileVideo: "https://s3.amazonaws.com/sync1to1/SYNC1TO1Video20180104114749AM.mp4",
        RegisteredFrom: "   ",
        ProfilePhoto: "",
        OtherPhotos: [
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png"
        ],
        OtherImages: [],
        Preferences: [

        ]
    }
    // BlockuserDetails object
    BlockedUserDetails = {
        Name: "Dipen Ahir",
        PhoneNo: "+919672829202",
        Email: "dipen@mobifyi.com",
        RegistrationDate: "Friday, January 5, 2018 5:39:37 AM",
        DateOfBirth: "17/08/1992",
        LastLogin: "Friday, January 5, 2018 5:39:37 AM",
        Age: "25",
        about: "",
        Gender: "Male",
        Height: "166 cm , 5'6\" feet",
        city: "",
        country: "",
        ProfileVideo: "https://s3.amazonaws.com/sync1to1/SYNC1TO1Video20180104114749AM.mp4",
        RegisteredFrom: "   ",
        ProfilePhoto: "",
        OtherPhotos: [
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png"
        ],
        OtherImages: [],
        Preferences: [

        ]
    }
    prefranceValues = [];
    constructor(@Inject(ElementRef) elementRef: ElementRef, public cdr: ChangeDetectorRef, private _usersService: UsersService, private _profileService: ProfilesService, private _preferenceservice: PreferenceService, private formBuilder: FormBuilder,
        private _state: AppState, private router: Router, private http: Http,
        private _loader: MapsAPILoader,
        private _zone: NgZone) {
        this.elementRef = elementRef;
       // this.filesToUpload = [];
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.width = 240;
        this.cropperSettings.height = 240;
        this.cropperSettings.croppedWidth = 330;
        this.cropperSettings.croppedHeight = 330;
        this.cropperSettings.canvasWidth = 180;
        this.cropperSettings.canvasHeight = 200;
        this.cropperSettings2 = new CropperSettings();
        this.cropperSettings2.width = 540;
        this.cropperSettings2.height = 283;
        this.cropperSettings2.croppedWidth = 230;
        this.cropperSettings2.croppedHeight = 140;
        this.cropperSettings2.canvasWidth = 180;
        this.cropperSettings2.canvasHeight = 150;
        jQuery("#myDate").datepicker({ maxDate: new Date, minDate: new Date(2007, 6, 12) });
        this.uploader.onSuccessItem = (item: any, response: string, status: number, headers: any) => {
            this.cloudinaryImage = JSON.parse(response);
            //console.log("this.cloudinaryImage",this.cloudinaryImage)
            jQuery('#loader').hide();
            this.fullVideoPath = this.cloudinaryImage.secure_url;
            console.log("resposnse of cloudinary video upload",this.fullVideoPath)
            return { item, response, status, headers };
          };
        var today = new Date();
        var minAge = 18;
        let validateDate = { year: today.getFullYear() - minAge, month: today.getMonth(), day: today.getDate() }
        //for myDatePickerOptions
        this.myDatePickerOptions = {
            showTodayBtn: true,
            dateFormat: 'yyyy-mm-dd',
            firstDayOfWeek: 'mo',
            sunHighlight: true,
            height: '28px',
            width: '187px',
            inline: false,
            disableSince: validateDate,
            selectionTxtFontSize: '12px',
            editableDateField: false,
            disableUntil: { year: today.getFullYear() - 100, month: today.getMonth(), day: today.getDate() }
        };

        this.data = {};
        this.dataUp = {};
        this.data2 = {};



        this._state.subscribe('menu.activeLink', (activeLink) => {
            if (activeLink) {
                this.activePageTitle = activeLink;
                this.Btn = activeLink;
            }
        });




    }

    ActiveDates = [];
    ActiveUserSearchDates = {
        "dateFrom": 0, "dateTo": 0

    };
    BanUserSearchDates = {
        "dateFrom": 0, "dateTo": 0

    };
    DeactiveUserSearchDates = {
        "dateFrom": 0, "dateTo": 0

    };
    ReporetdUserSearchDates = {
        "dateFrom": 0, "dateTo": 0

    };
    BlockUserSearchDates = {
        "dateFrom": 0, "dateTo": 0

    };
    DeletedUserSearchDates = {
        "dateFrom": 0, "dateTo": 0

    };
    // public uploader: FileUploader = new FileUploader({ url: URL, itemAlias: 'newProfilePicture' });
    title: string;
    previewImage: any;
    tempImage: any;
    source: string;
   
    results: any[] = [];
    queryField: FormControl = new FormControl();
    public slide1;
    public slide2;
    ngOnInit() {
        
        this.getAll(this.p);
        this.myForm = this.formBuilder.group({
            imageUrl: [''],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            dob: [''],
            email: ['', Validators.compose([Validators.required])],
            city: ['', Validators.required],
            height: ['', Validators.required],

            gender: ['', Validators.required],
            country: ['', Validators.required],
            contactNumber: ['', Validators.compose([Validators.required])],
            countryCode: ['', Validators.required],
            lat: ['', Validators.required],
            lng: ['', Validators.required],
        })


        this.notificationM = this.formBuilder.group({

            line2: ['', Validators.compose([Validators.required, lengthValidator])],
            message: ['', Validators.compose([Validators.required, lengthValidatorr])],


        })

        this.autocompleted = new google.maps.places.Autocomplete((<HTMLInputElement>document.getElementById('location')));
        this.autocompleted.addListener('place_changed', () => {
            this.place = this.autocompleted.getPlace();
            this.location = this.place.name;
            this.let = this.place.geometry.location.lat();
            this.long = this.place.geometry.location.lng();
            console.log("name", this.location, this.let, this.long);
            (<HTMLInputElement>document.getElementById('latitude')).value = this.place.geometry.location.lat();
            (<HTMLInputElement>document.getElementById('longitude')).value = this.place.geometry.location.lng();
            jQuery("input#location").val(this.place.name);
        });
    }
    
    public totalItems: number;
    zoom: number = 8;
    lat: number = 51.673858;
    lng: number = 7.815982;
    filename: string;
   

    public currdate: any;
    public mydate: any;
    public setDate1: any;
    setDate() {
        // Set today date using the patchValue function
        let date = new Date();
        this.myForm.patchValue({
            dob: {
                date: {
                    year: date.getFullYear(),
                    month: date.getMonth() + 1,
                    day: date.getDate()
                },
            }
        });
    }
    clearDate(): void {
        // Clear the date using the patchValue function
        this.myForm.patchValue({ dob: null });
    }
    
    upload() {
        jQuery('#loader').show();
        this.uploader.uploadAll();
      }
    

    SelectedIDs: any[];
    selectID(id, event: any) {
        this.users.push(id);
        console.log("zzzzzz", this.users)
    }
    deleteSelected() {
        this.SelectedIDs.forEach(function (obj) {
            this.pagedItems = this.pagedItems.filter(item => item.id !== obj.id);
        });
    }
    isChecked = function (entity) {
        return this.checkedEntity === entity;
    };
    toggleSelection = function (entity) {
        entity.checked = !entity.checked;
        if (entity.checked) {
            this.checkedEntity = entity;
            //  this.activeRecords = event.target.checked;
            this.users.forEach(function (item) {

                (item).selected = entity.checked;
                // console.log(item);
            });
        } else {
            this.checkedEntity = null;
        }
    };
    toggleSelectionCheckbox = function toggleSelection(event) {

        // alert(event.target.checked);
    };
    selectAllCheckBox() {
        this.deleteUser = [];
        this.users.forEach((obj) => {
            obj.checked = !obj.checked;
            if (obj.checked) {
                this.deleteUser.push(obj._id)
                this.toggleBool = false;
            }
            else {
                this.toggleBool = true;
            }
        });
        console.log("===>>", this.deleteUser)
    }
    public isChecking = false;
    gotocheck(id) {
        var index = this.deleteUser.indexOf(id)
        if (index === -1) {
            this.deleteUser.push(id)
            this.isChecking = true;
        }
        else {
            this.deleteUser.splice(index, 1)
            this.isChecking = false;

        }
        console.log("selected:-", this.deleteUser);
    }

    removeAll = function () {
        var removeList = [];
        var selectedIds = new Array();
        this.users.forEach(this.user, function (emp) {
            if (emp.selected) {
                selectedIds.push(emp.id);
            }
        });

    };

    toggleVideo(event: any) {
        this.videoplayer.nativeElement.play();
    }

    toggleSelect = function (event) {
        this.activeRecords = event.target.checked;
        this.users.forEach(function (item) {
            (item).selected = event.target.checked;
        });

    }

    selectall(_id, user) {
        this.users = _id;

        console.log("_id : ", _id);
        $("#BannedUser").modal("show");
    }

    
    refresh() {
        this.ids = [];

    }
    test(i) {
        this.slide = i
    }

    showDetails(_id) {
        this.loader = true;
        this.router.navigate(['/pages/reportedInDetails/' + _id]);
    }
    showBlockUserDetails(_id) {
        this.loader = true;
        this.router.navigate(['/pages/BlockUserDetail/' + _id]);
    }
    getAll(p) {
        this._usersService.getUser(p - 1, this.rowsOnPage).subscribe(
            data => {
                this.users = data.data;
                this.totalusers = data.totalCount;
                this.p = p;
                console.log("Active dataaaaaaaaaaa", data, this.totalusers);
            })
    }
    getBanUser(p) {
        this._usersService.getBannedUsers(p - 1, this.rowsOnPage).subscribe((data) => {
            if (data.data) {
                this.banUsers = data.data;
                this.totalusers = data.totalCount;
                this.p = p;
                // console.log("banned dataaaaaaaaaaa", data);
            }
            error => console.log(error)
        });
    }
    getDeactivateUser(p) {
        this._usersService.getDeactivateUsers(p - 1, this.rowsOnPage).subscribe((data) => {
            if (data.data) {
                this.deactUser = data.data;
                this.totalusers = data.totalCount;
                this.p = p;
                // console.log("desctivateuser dataaaaaaaaaaa", this.users);
            }
            error => console.log(error)
        });
    }
    getReportedUser(p) {
        this._usersService.getReportedUsers(p - 1, this.rowsOnPage).subscribe((data) => {
            if (data.data) {
                this.userLog = data.data;
                this.totalusers = data.totalCount;
                this.p = p;
                // console.log("reported dataaaaaaaaaaa", data);
            }
            error => console.log(error)
        });
    }
    getBlockUser(p) {
        this._usersService.getBlockUsers(p - 1, this.rowsOnPage).subscribe((data) => {
            if (data.data) {
                this.blockData = data.data;
                this.totalusers = data.totalCount;
                this.p = p;
                //console.log("block user data", data);
            }
            error => console.log(error)
        });


    }
    getDeleteUser(p) {
        this._usersService.getDeleteUsers(p - 1, this.rowsOnPage).subscribe((data) => {
            if (data.data) {
                this.deleteData = data.data;
                this.totalusers = data.totalCount;
                this.p = p;
                //console.log("getDeleteUser data", data, this.totalusers);
            }
            error => console.log(error)
        });


    }
    public showPrefData;
    getPreferenceData() {
        this.fullImagePath = '';
        this.fullVideoPath = '';
        this._usersService.getPreferences().subscribe((data) => {
            //console.log("dataaaaaaaaaaaaa", data)

            if (data.data) {
                this.PreferenceArrayData = data.data.Mypreference[0];
                //  console.log("------------->", this.PreferenceData)



                this.PreferenceData = data.data.searchPreferences[0];
                this.PreferenceData.forEach(x => {
                    if (x.type == 2) {
                        x.data = [];
                    }
                });
                this.PreferenceData.forEach(height => {
                    if (height.type == 10) {
                        this.height = height.options;
                        //  console.log("-------------this.height>", this.height)

                    }
                });
                console.log("Second Preference data", this.PreferenceArrayData);
            }
            error => console.log(error)

        });

    }

    unbannedUser: any;
    UnBannduser(id) {
        this.unbannedUser = id;
        this._usersService.putUnBannedUser(id).subscribe((data) => {
            console.log("data", data);
            if (data) {
                this.banUsers = data.data;
                swal("Success!", "User UnBanned!", "success");
                this.getBanUser(this.p);
            }
            error => console.log(error)
        });
    }



    initAddForm() {
        this.myForm = this.formBuilder.group({
            firstName: ['', Validators.required],
            lname: [''],
            dob: ['', Validators.required],
            email: ['', Validators.compose([Validators.required])],
            city: ['', Validators.required],
            country: ['', Validators.required],
            gender: ['', Validators.required],
            about: [''],
            lat: ['', Validators.required],
            lng: ['', Validators.required],
            fullImagePath: [''],
            countryCode: ['', Validators.required],
            contactNumber: ['', Validators.required],
            values: this.formBuilder.array([]),
            valuesUser: this.formBuilder.array([]),
            OtherImages: this.formBuilder.array([]),
            range: [[152, 213]],
            singlVal: [0],
            dstTyp: [1],
            lmtTyp: [2],
            password: ['', Validators.required],
            height: ['', Validators.required],
            Distance: [this.someRange],
            Age: [this.someRange],
            Height: [this.someRange],
            fullpath: ['']

        })


    }
    userDetailsModal(user) {
        this.prefData = []
        //console.log("+++++++++",user)
        var searchPreferences = user.myPreferences
        //console.log("-----------6^^][][][][]", this.prefData)
        searchPreferences.forEach(ele => {
            if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Religious beliefs",
                    "selectedValue": ele.selectedValues[0],
                    "type": 3,
                    "pref_id": ele.pref_id,
                })
            }
            else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Ethnicity",
                    "selectedValue": ele.selectedValues,
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Kids ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Work ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Job",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Education ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Politics ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Family Plans",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Highest Level Attended ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drinking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Smoking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Marijuana",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drugs",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Height ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }


        });
        this.userDetails.OtherPhotos.length = 0;
        this.userDetails.OtherPhotos = [];
        var years = moment().diff(user.dob, 'years');
        this.userDetails.Name = user.firstName || "--";
        this.userDetails.PhoneNo = user.contactNumber || "--";
        this.userDetails.Email = user.email || "--";
        this.userDetails.city = user.city || "--";
        this.userDetails.country = user.country || "--";
        this.userDetails.Gender = user.gender || "--";
        this.userDetails.DateOfBirth = user.dob;
        this.userDetails.about = user.about || "--"
        this.userDetails.RegistrationDate = user.registeredTimestamp || "--";
        this.userDetails.ProfileVideo = user.profileVideo || "";
        jQuery(".videoAutoPlay").attr("src", this.userDetails.ProfileVideo);
        this.userDetails.videoGallery.push(this.userDetails.ProfileVideo)
        this.userDetails.ProfilePhoto = user.profilePic;
        this.userDetails.OtherPhotos = user.otherImages || [];
        this.userDetails.OtherPhotos.push(this.userDetails.ProfilePhoto);
        this.userDetails.Preferences = user.favoritePreferences || [];
        this.userDetails.Age = years || "--";
        this.userDetails.LastLogin = user.lastOnline || "--";
        this.userDetails.RegisteredFrom = "--";
        if (user.city || user.country) {
            this.userDetails.RegisteredFrom = user.city + " ," || "--";
            this.userDetails.RegisteredFrom += user.country || "--";
        }
        this.userDetails.Height = user.height + " cm," + (parseFloat((user.height * 0.032808399).toString())).toFixed(1) + "feet" || "--";
        // });
    }
    //}
    userDetailsReportModal(user) {
        var ReportPreferences = user.myPreferences
        ReportPreferences.forEach(ele => {
            if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Religious beliefs",
                    "selectedValue": ele.selectedValues[0],
                    "type": 3,
                    "pref_id": ele.pref_id,
                })
            }
            else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Ethnicity",
                    "selectedValue": ele.selectedValues,
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Kids ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Work ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Job",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Education ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Politics ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Family Plans",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Highest Level Attended ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drinking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Smoking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Marijuana",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drugs",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Height ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
        });
        this.ReporteduserDetails.OtherPhotos.length = 0;
        this.ReporteduserDetails.OtherPhotos = [];
        var years = moment().diff(user.dob, 'years');
        this.ReporteduserDetails.Name = user.reportedUser || "--";
        this.ReporteduserDetails.PhoneNo = user.reportedPhoneNo || "--";
        this.ReporteduserDetails.Email = user.reportEdEmail || "--";
        this.ReporteduserDetails.city = user.city || "--";
        this.ReporteduserDetails.country = user.country || "--";
        this.ReporteduserDetails.Gender = user.gender || "--";
        this.ReporteduserDetails.about = user.about || "--";
        this.ReporteduserDetails.DateOfBirth = user.dob;
        this.ReporteduserDetails.RegistrationDate = user.creation || "--";
        this.ReporteduserDetails.Height = user.height + "cm," + (parseFloat(((user.height * 0.032808399)).toString()).toFixed(1)) + "feet" || "--";
        this.ReporteduserDetails.ProfileVideo = user.profileVideo || "";
        jQuery(".videoAutoPlay").attr("src", this.userDetails.ProfileVideo);
        this.ReporteduserDetails.ProfilePhoto = user.profilepic;
        this.ReporteduserDetails.OtherPhotos = user.otherImages || [];
        this.ReporteduserDetails.OtherPhotos.push(this.ReporteduserDetails.ProfilePhoto);
        this.ReporteduserDetails.Preferences = user.favoritePreferences || [];
        this.ReporteduserDetails.Age = years || "--";
        //this.userDetails.LastLogin = user.lastLogin || user.lastOnlineStatus;
        this.ReporteduserDetails.RegisteredFrom = "--";
        if (user.city || user.country) {
            this.ReporteduserDetails.RegisteredFrom = user.city + " ," || " ";
            this.ReporteduserDetails.RegisteredFrom += user.country || " ";
        }
    }
    userDetailsBlockedModal(user) {
        // console.log("===>>>", user)
        // console.log("-----------6^^][][][][]", this.prefData)
        var BlockedModalPreferences = user.myPreferences
        //console.log("ReportPreferences", BlockedModalPreferences)
        BlockedModalPreferences.forEach(ele => {
            if (ele.pref_id == "5a30ff2627322defa4a146a8" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Religious beliefs",
                    "selectedValue": ele.selectedValues[0],
                    "type": 3,
                    "pref_id": ele.pref_id,
                })
            }
            else if (ele.pref_id == "5a30fb2827322defa4a14586" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Ethnicity",
                    "selectedValue": ele.selectedValues,
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fcb327322defa4a145f4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Kids ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fda027322defa4a14638" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Work ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdd127322defa4a14649" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Job",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fdfa27322defa4a14653" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Education ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ff7e27322defa4a146c4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Politics ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fd0e27322defa4a1460e" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Family Plans",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fe6527322defa4a14673" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Highest Level Attended ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30ffb227322defa4a146d4" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drinking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31000227322defa4a146eb" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Smoking ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31002827322defa4a146f9" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Marijuana",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a31005027322defa4a14703" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Drugs",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }
            else if (ele.pref_id == "5a30fa6d27322defa4a14550" && ele.isDone == true) {
                this.prefData.push({
                    "title": "Height ",
                    "selectedValue": ele.selectedValues[0],
                    "pref_id": ele.pref_id,
                    "type": 3
                });
            }

        });
        this.BlockedUserDetails.OtherPhotos.length = 0;
        this.BlockedUserDetails.OtherPhotos = [];
        var years = moment().diff(user.dob, 'years');
        this.BlockedUserDetails.Name = user.blockedUser || "--";
        this.BlockedUserDetails.PhoneNo = user.blockedPhoneNo || "--";
        this.BlockedUserDetails.Email = user.blockedEdEmail || "--";
        this.BlockedUserDetails.city = user.city || "--";
        this.BlockedUserDetails.country = user.country || "--";
        this.BlockedUserDetails.Gender = user.gender || "--";
        this.BlockedUserDetails.about = user.about || "--";
        this.BlockedUserDetails.DateOfBirth = user.dob;
        this.BlockedUserDetails.RegistrationDate = user.timestamp || "--";
        this.BlockedUserDetails.Height = user.height + " cm," + (parseFloat(((user.height * 0.032808399)).toString()).toFixed(1)) + "feet" || "--";
        this.BlockedUserDetails.ProfileVideo = user.profileVideo || "";
        jQuery(".videoAutoPlay").attr("src", this.userDetails.ProfileVideo);
        this.BlockedUserDetails.ProfilePhoto = user.profilepic;
        this.BlockedUserDetails.OtherPhotos = user.otherImages || [];
        this.BlockedUserDetails.OtherPhotos.push(this.BlockedUserDetails.ProfilePhoto);
        this.BlockedUserDetails.Preferences = user.favoritePreferences || [];
        this.BlockedUserDetails.Age = years || "--";
        //this.userDetails.LastLogin = user.lastLogin || user.lastOnlineStatus;
        this.BlockedUserDetails.RegisteredFrom = "--";
        if (user.city || user.country) {
            this.BlockedUserDetails.RegisteredFrom = user.city + " ," || " ";
            this.BlockedUserDetails.RegisteredFrom += user.country || " ";
        }


    }
    recentVisitors(_id) {
        this.loader = true;
        this.router.navigate(['/pages/recentVisitors/' + _id]);
    }
    likes(_id) {
        this.loader = true;
        this.router.navigate(['/pages/likes/' + _id]);
    }
    mylikes(_id) {
        this.loader = true;
        this.router.navigate(['/pages/mylikes/' + _id]);
    }
    SuperLikes(_id) {
        this.loader = true;
        this.router.navigate(['/pages/superLikes/' + _id]);
    }
    mySuperLikes(_id) {
        this.loader = true;
        this.router.navigate(['/pages/MySuperLikes/' + _id]);
    }
    disLikes(_id) {
        this.loader = true;
        this.router.navigate(['/pages/mydisLikes/' + _id]);
    }
    mydisLikes(_id) {
        this.loader = true;
        this.router.navigate(['/pages/disLikes/' + _id]);
    }
    matchesUser(_id) {
        this.loader = true;
        this.router.navigate(['/pages/matches/' + _id]);
    }
    deviceLogs(_id) {
        this.loader = true;
        this.router.navigate(['/pages/deviceLogs/' + _id]);

    }
    showModalDelete(_id) {

        this.loader = true;

    }
    Deleteuser() {
        //console.log("idddddddd", this.deleteUser);
        if (this.deleteUser.length == 0) {
            swal("Please Select User");
        } else {
            var index = -1;
            this._usersService
                .deleteUser(this.deleteUser).subscribe((data) => {
                    if (data.code !== 200) {
                        this.users = data.data;
                        swal("Success!", "User Deleted!", "success");
                        this.ngOnInit();
                        this.deleteUser.splice(index, 1)
                        this.isChecking = false;
                        this.getAll(this.p);
                    }
                });
        }
    }
    Bannduser() {
        // console.log("idddddddd", this.deleteUser);
        if (this.deleteUser.length == 0) {
            swal("Please Select User");
        } else {
            var index = -1;
            this._usersService
                .banUser(this.deleteUser)
                .subscribe((data) => {
                    if (data.code !== 200) {

                        this.users = data.data;
                        swal("Success!", "User Banned!", "success");
                        this.deleteUser.splice(index, 1);
                        this.isChecking = false;
                        this.getAll(this.p);

                    }
                });
        }

    }


    // for Map in add user select location
    initMap(myMap) {

        myMap.triggerResize() // to resize map on popup

        this._loader.load().then(() => {
            var control = document.getElementById("autocompleteInput");
            var input = /** @type {!HTMLInputElement} */ (control);
            var autocomplete = new google.maps.places.Autocomplete(input, { types: ['geocode'] });
            google.maps.event.addListener(autocomplete, 'place_changed', () => {
                this._zone.run(() => {
                    let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                    //set latitude and longitude
                    var address = place.formatted_address;
                    var res = place.address_components;
                    //  console.log(address)
                    this.lat = place.geometry.location.lat();
                    this.lng = place.geometry.location.lng();
                    var add = place.formatted_address;
                    var value = add.split(",");
                    var count = value.length;
                    var country = value[count - 1];
                    var city = value[count - 3];
                    //    console.log(city + country)
                    this.myForm.controls['lat'].setValue(this.lat)
                    this.myForm.controls['lng'].setValue(this.lng)
                    this.myForm.controls['city'].setValue(city)
                    this.myForm.controls['country'].setValue(country)
                    //console.log(this.myForm.value)
                });
            });
        }); // not in oninit
    }






    mapClick() {
        this.mapmodal = true;
        if (this.myForm.value.lat) {
            this.lat = this.myForm.value.lat;
            this.lng = this.myForm.value.lng;
            this.zoom = 15;
        }
    }
    closeMap() {
        this.mapmodal = false;
        jQuery('#myMapModal').hide();
    }
    cancelMap() {
        this.mapmodal = false;
        this.myForm.value.lat = '',
            this.myForm.value.lng = ''
        jQuery('#myMapModal').hide();
    }
    resetForm() {
        this.myForm.reset();
    }
    mapClickUp() {
        this.mapmodalUp = true;
        if (this.userDetailForm.value.lat) {
            this.lat = this.userDetailForm.value.lat;
            this.lng = this.userDetailForm.value.lng;
            this.zoom = 15;
        }
    }
    cancelMapUp() {
        this.mapmodalUp = false;
    }
    closeMapUp() {
        this.mapmodalUp = false;
    }
    setCurrentPosition() {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.lat = position.coords.latitude;
                this.lng = position.coords.longitude;
                this.zoom = 15;
                this.myForm.controls['lat'].setValue(this.lat)
                this.myForm.controls['lng'].setValue(this.lng)
            });
        }
    }
    setRadioValue(e, i) {  // insert
        const option = <FormArray>this.myForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['selectedValue']
        options.setValue([e.target.value])

    }
    setRadioValueUserSettings(e, i) {  // insert
        const option = <FormArray>this.myForm.controls['valuesUser']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        options.setValue([e.target.value])

    }
    setRadioValueUp(e, i) {  // update 
        let Values = this.editUserForm.value.values;
        
        const option = <FormArray>this.editUserForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
       
        options.setValue([e.target.value])
       

    }
    setRadioValueUpUserSettings(e, i) {  // update 
      
        let Values = this.editUserForm.value.valuesUser;
       
        const option = <FormArray>this.editUserForm.controls['valuesUser']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        
        options.setValue([e.target.value])
      

    }
    setCheckValue(e, i, opt, id) {
        const option = <FormArray>this.myForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        // console.log("iiiiiiiiiiii", i);
        if (e.target.checked) {
            // console.log("checked")
            if (options.value) {
                this.values = options.value
                this.values.push(opt)
            } else {
                this.values.push(opt)
            }
            options.setValue(this.values)
        } else {
            // console.log("unchecked")
            if (options.value) {
                this.values = options.value
                this.values.splice(this.values.indexOf(opt), 1)
            }
            // else {
            //     this.values.splice(this.values.indexOf(opt), 1)
            // }
            options.setValue(this.values)
        }
        var dt = {
            pref_id: id,
            selectedValue: options.value
        };
        this.checkBtn = dt;
        
        this.values = []
    }
    setCheckValueUserSettings(e, i, opt) {
        const option = <FormArray>this.myForm.controls['valuesUser']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        if (e.target.checked) {
            // console.log("checked")
            if (options.value) {
                this.valuesUser = options.value
                this.valuesUser.push(opt)
            } else {
                this.valuesUser.push(opt)
            }
            options.setValue(this.valuesUser)
        } else {
            //console.log("unchecked")
            if (options.value) {
                this.valuesUser = options.value
                this.valuesUser.splice(this.valuesUser.indexOf(opt), 1)
            }
           
            options.setValue(this.valuesUser)
        }
        //console.log(options.value)
        this.valuesUser = []
    }
    setCheckValueUp(e, i, opt, id) {
        const option = <FormArray>this.editUserForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        if (e.target.checked) {
            //console.log("checked")
            if (options.value) {
                this.values = options.value
                this.values.push(opt)
            } else {
                this.values.push(opt)
            }
            options.setValue(this.values)
        } else {
            // console.log("unchecked")
            if (options.value) {
                this.values = options.value
                this.values.splice(this.values.indexOf(opt), 1)
            }
            options.setValue(this.values)
        }
        var dt = {
            pref_id: id,
            selectedValue: options.value
        };
        this.checkBtn = dt;
        console.log("options.value", dt)
        this.values = []
    }
    setCheckValueUpUserSettings(e, i, opt) {
        const option = <FormArray>this.editUserForm.controls['valuesUser']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        if (e.target.checked) {
            // console.log("checked")
            if (options.value) {
                this.valuesUser = options.value
                this.valuesUser.push(opt)
            } else {
                this.valuesUser.push(opt)
            }
            options.setValue(this.valuesUser)
            //console.log(this.editUserForm.value)
        } else {
            // console.log("unchecked")
            if (options.value) {
                this.valuesUser = options.value
                this.valuesUser.splice(this.valuesUser.indexOf(opt), 1)
            }
            options.setValue(this.valuesUser)
        }
        //console.log(options.value)
        this.valuesUser = []
    }
    cropped(e, i) {
        //console.log(this.data) // for cropping
        this.cropper = true;

    }

    croppedUp(e, i) {

    }
    croppedPush(e, i) {

    }


    public type;
    public preferenceUser = [];
    public valueArray = [];
    public isDuplicate: boolean = false;
    // add my prefrence from admin
    SubmitPreferences(o, id, type, event, i) {
        // console.log("the values of the id :", id, o, type, event, i);

        if (type == 1) {
            if (event.target.checked == true) {
                this.PreferenceData[i].data = o;
                this.PreferenceData[i].checked = true;
            } else {
                this.PreferenceData[i].checked = false;
            }
        } else if (type == 2) {
            if (event.target.checked == true) {
                this.PreferenceData[i].data.push(o)
                this.PreferenceData[i].checked = true;
            } else {
                let indexx = this.PreferenceData[i].data.indexOf(o);
                this.PreferenceData[i].data.splice(indexx, 1);
                // this.PreferenceData[i].checked = false;
            }

        } else if (type == 5) {
            this.PreferenceData[i].data = o;
            this.PreferenceData[i].checked = true;

        }
        else if (type == 10) {

            this.PreferenceData[i].data = event.target.value;
            this.PreferenceData[i].checked = true;
        }
        // console.log("prefarrayyyyyyyyyyyyyyy", this.PreferenceData)
    }
    public TypeOfPreference;
    public preferenceSearchUser = [];
    // add my serch prefrence from admin
    SubmitSearchPreferences(o, id, type, event, i) {
        console.log("the values of the id for my prefrences :", o, id, type, event, i);
        if (type == 1) {
            if (event.target.checked == true) {
                this.PreferenceArrayData[i].data = o;
                this.PreferenceArrayData[i].checked = true;
                console.log("slider data================>", this.PreferenceArrayData)
            } else {
                this.PreferenceArrayData[i].checked = false;
            }


        }


    }
    // for add new admin user 
    onSubmit() {
        this.PreferenceData.forEach(e => {
            if (e.checked) {
                if (e.type == 2) {
                    this.preferenceUser.push({
                        pref_id: e._id,
                        selectedValues: e.data
                    })
                } else {
                    this.preferenceUser.push({
                        pref_id: e._id,
                        selectedValues: [e.data]
                    })
                }
            }
        });

        var mydata = this.preferenceUser;
        this.myForm.value.profilePic = this.fullImagePath;
        this.myForm.value.profileVideo = this.fullVideoPath;

        this._usersService
            .CreateUser(this.myForm.value, mydata)
            .subscribe((data) => {
                this.users = data.data;
                this.getAll(this.p)
                swal("Success!", "User Created successfully !", "success");
                this.myForm.reset();
                this.fullImagePath = '';
                this.fullVideoPath = '';
                this.preferenceUser = [];
                this.Aimage = '';
                $("#addUser").modal("hide");
            });

    }
    setSliderValue(i, id) {  // insert

        const option = <FormArray>this.myForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['selectedValue']
        // console.log("name", name)
        // console.log("name : i ", i)
        let Obj = [
            this.myForm.value.range[0],
            this.myForm.value.range[1]
        ];
        options.setValue(Obj)
        var dt = {
            pref_id: id,
            selectedValue: [
                this.myForm.value.range[0],
                this.myForm.value.range[1]
            ]
        }

        if (this.favPreference.length != 0) {
            var flg = 0;
            this.favPreference.forEach(e => {
                if (e.pref_id == id) {
                    flg = 1;
                    e.selectedValue = [
                        this.myForm.value.range[0],
                        this.myForm.value.range[1]
                    ]
                }
            });
            if (flg == 0) {
                this.favPreference.push(dt);
            }
        } else {
            this.favPreference.push(dt);

        }
        console.log("this.favPreference", this.myForm.controls.range);
        console.log("this.favPreference ", this.favPreference)

    }
    setUserSliderValue(i) {  // insert
        const option = <FormArray>this.myForm.controls['valuesUser']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        // console.log(name)
        let Obj = {
            "Start": this.myForm.value.range[0].toString(),
            "End": this.myForm.value.range[1].toString()
        }
        options.setValue(Obj)
        // console.log(Obj)

    }

    setSliderValE(i) {
        const option = <FormArray>this.editUserForm.controls['values']
        const name = <FormGroup>option.controls[i]
        const options = name.controls['options']
        // console.log(name)
        let Obj = {
            "Start": this.editUserForm.value.slideval[0].toString(),
            "End": this.editUserForm.value.slideval[1].toString()
        }
        options.setValue(Obj)
        //console.log(Obj)
    }
    getUserById(id, e) {
        //  console.log("id", id);
        this.valuesExist = false
        this.valuesExistUser = false;
        this.loader = true
        this._usersService
            .getUserById(id)
            .subscribe((data) => {
                // console.log("data", data);
                this.imageurl.splice(0)
                this.file_srcsUp.splice(0)
                if (data.code == 200) {
                    this.user = data.data;
                    this.loader = false
                    this.userById = id
                    this.getUsrById()
                    this.dataUp = [] // to make notuploaded image to null in edit mode

                }
                error => console.log(error)
            });

    }
    filteredItems: any[];
    inputName: string = '';
    FilterFromTo() {
        this._usersService.getSearchFromActiveUser(this.ActiveUserSearchDates).subscribe((data) => {
            if (data.data) {
                this.ActiveDates = data.data;
                this.totalusers = data.data.length;

            } else {
                this.ActiveDates = [];
            }
        });
    }
    setActiveFromDate(date) {
        this.ActiveUserSearchDates.dateFrom = new Date(date).getTime();
        this._usersService.getSearchFromActiveUser(this.ActiveUserSearchDates).subscribe((data) => {
            if (data.data) {
                this.users = data.data;
                this.totalusers = data.data.length;
            } else {
                this.users = [];
            }
        });
    }
    setActiveToDate(date) {
        this.ActiveUserSearchDates.dateTo = new Date(date).getTime();
        this._usersService.getSearchFromActiveUser(this.ActiveUserSearchDates).subscribe((data) => {
            if (data.data) {
                this.users = data.data;
                this.totalusers = data.data.length;
            } else {
                this.users = [];
            }
        });
    }
    setBanFromDate(date) {
        this.BanUserSearchDates.dateFrom = new Date(date).getTime();
        this._usersService.getSearchFromBanDates(this.BanUserSearchDates).subscribe((data) => {
            if (data.data) {
                this.banUsers = data.data;
                this.totalusers = data.data.length;
            } else {
                this.banUsers = [];
            }
        });
    }

    setBanToDate(date) {
        this.BanUserSearchDates.dateTo = new Date(date).getTime();
        this._usersService.getSearchFromBanDates(this.BanUserSearchDates).subscribe((data) => {
            if (data.data) {
                this.banUsers = data.data;
                this.totalusers = data.data.length;
            } else {
                this.banUsers = [];
            }
        });
    }
    setDeactivateFromDate(date) {
        this.DeactiveUserSearchDates.dateFrom = new Date(date).getTime();
        this._usersService.getSearchFromDeactivateDates(this.DeactiveUserSearchDates).subscribe((data) => {
            if (data.data) {
                this.deactUser = data.data;
                this.totalusers = data.data.length;
            } else {
                this.deactUser = [];
            }
        });
    }

    setDeactivateToDate(date) {
        this.DeactiveUserSearchDates.dateTo = new Date(date).getTime();
        this._usersService.getSearchFromDeactivateDates(this.DeactiveUserSearchDates).subscribe((data) => {
            if (data.data) {
                this.deactUser = data.data;
                this.totalusers = data.data.length;
            } else {
                this.deactUser = [];
            }
        });
    }
    setReportedFromDate(date) {
        this.ReporetdUserSearchDates.dateFrom = new Date(date).getTime();
        this._usersService.getSearchFromReportedDates(this.ReporetdUserSearchDates).subscribe((data) => {
            if (data.data) {
                this.userLog = data.data;
                this.totalusers = data.data.length;
            } else {
                this.userLog = [];
            }
        });
    }

    setReportedToDate(date) {
        this.ReporetdUserSearchDates.dateTo = new Date(date).getTime();
        this._usersService.getSearchFromReportedDates(this.ReporetdUserSearchDates).subscribe((data) => {
            if (data.data) {
                this.userLog = data.data;
                this.totalusers = data.data.length;
            } else {
                this.userLog = [];
            }
        });
    }
    setBlockedFromDate(date) {
        this.BlockUserSearchDates.dateFrom = new Date(date).getTime();
        this._usersService.getSearchFromBlockedDates(this.BlockUserSearchDates).subscribe((data) => {
            if (data.data) {
                this.blockData = data.data;
                this.totalusers = data.data.length;
            } else {
                this.blockData = [];
            }
        });
    }

    setBlockedToDate(date) {
        this.BlockUserSearchDates.dateTo = new Date(date).getTime();
        this._usersService.getSearchFromBlockedDates(this.BlockUserSearchDates).subscribe((data) => {
            if (data.data) {
                this.blockData = data.data;
                this.totalusers = data.data.length;
            } else {
                this.blockData = [];
            }
        });
    }
    setDeletedFromDate(date) {
        this.DeletedUserSearchDates.dateFrom = new Date(date).getTime();
        this._usersService.getSearchFromDeletedDates(this.DeletedUserSearchDates).subscribe((data) => {
            if (data.data) {
                this.deleteData = data.data;
                this.totalusers = data.data.length;
            } else {
                this.deleteData = [];
            }
        });
    }

    setDeletedToDate(date) {
        this.DeletedUserSearchDates.dateTo = new Date(date).getTime();
        this._usersService.getSearchFromDeletedDates(this.DeletedUserSearchDates).subscribe((data) => {
            if (data.data) {
                this.deleteData = data.data;
                this.totalusers = data.data.length;
            } else {
                this.deleteData = [];
            }
        });
    }
    gotoSearchActiveUser(term) {
        this._usersService.getUserByActivetext(term).subscribe(
            (data) => {
                this.users = data.data;
                this.totalusers = data.data.length;
            });
        if (term == "") {
            this.getAll(this.p);
        }

    }
    gotoSearchBanUser(term) {
        this._usersService.getUserByBantext(term).subscribe(
            (data) => {
                this.banUsers = data.data;
                this.totalusers = data.data.length;
            });
        if (term == "") {
            this.getBanUser(this.p);
        }
    }
    gotoSearchDeactUser(term) {
        this._usersService.getUserByDeacttext(term).subscribe(
            (data) => {
                this.deactUser = data.data;
                this.totalusers = data.data.length;
            });
        if (term == "") {
            this.getDeactivateUser(this.p);
        }
    }
    gotoSearchReportedUser(term) {
        this._usersService.getUserByReportedtext(term).subscribe(
            (data) => {
                this.userLog = data.data;
                this.totalusers = data.data.length;
            });
        if (term == "") {
            this.getReportedUser(this.p);
        }

    }
    gotoSearchBlockUser(term) {
        this._usersService.getUserByBlocktext(term).subscribe(
            (data) => {
                this.blockData = data.data;
                this.totalusers = data.data.length;
            });
        if (term == "") {
            this.getBlockUser(this.p);
        }
    }
    gotoSearchDeleteUser(term) {
        this._usersService.getUserByDeletetext(term).subscribe(
            (data) => {
                this.deleteData = data.data;
                this.totalusers = data.data.length;
            });
        if (term == "") {
            this.getDeleteUser(this.p);
        }

    }
    delUserById() {
        this._usersService.deleteUser(this.ids).subscribe(
            res => {
                if (res.code == 200) {
                    this.ngOnInit();
                    this.ids = [];
                    swal("Success!", "User Deleted!", "success")
                }
            }
        )

    }
  
    makeFileRequest(url: string, params: Array<string>, files: any) {
        return new Promise((resolve, reject) => {
            var formData: FormData = new FormData();
            var xhr = new XMLHttpRequest();
            formData.append('photo', files, files.name);
           
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));


                    } else {
                        reject(xhr.response);
                    }

                }

            }

            xhr.open('POST', url, true);
            xhr.send(formData);

        });

    }
    chngImg() {
        this.croppingBoxUp = true;
        jQuery('#croppingBoxUp').modal('show')
    }


    clrImg() {
        this.myForm.controls['fullpath'].setValue('')
        this.image = '';
    }

    closCropM() {
        this.croppingBoxUp = false;
        jQuery('#croppingBoxUp').modal('hide')
    }


    UploadImg() {
        this.croppingBox = true;
        jQuery('#croppingBox').modal('show')


    }
    UploadImgPush() {
        this.croppingBoxPush = true;
        jQuery('#croppingBoxPush').modal('show')
    }
    pushFormDetails() {
        this.detailsBoxPush = true;
        jQuery('#detailsBoxPush').modal('show')
    }
    closCropMAd() {
        this.myForm.value.fullpath = ""
        this.croppingBox = false;
        jQuery('#croppingBox').modal('hide')
    }
    closCropPush() {
        this.croppingBoxPush = false;
        jQuery('#croppingBoxPush').modal('hide')
    }
    profilePic(value) {
        if (value == null || value == "" || value == "string") {
            return "assets/img/app/noimage.png"
        }
        return value;
    }

    showNotificationModal() {
        this.loader = true;
        if (this.deleteUser.length == 0) {
            swal("Please Select User");
        } else {
            jQuery('#SendPushNotification').modal('show')
        }
    }
    sendNotification() {
        var index = -1;
        jQuery('#SendPushNotification').modal('show')
        var usersId = this.deleteUser;
        this._usersService.sendNotification(this.notificationM.value, usersId)
            .subscribe((data) => {
                if (data.code !== 200) {
                    swal("Success!", "Notification Sent Successfully!", "success");
                    this.ngOnInit();
                    this.deleteUser.splice(index, 1)
                    this.isChecking = false;
                    this.getAll(this.p);
                }
                else {
                }
            })
        jQuery('#SendPushNotification').modal('hide')
    }
    clear() {
        this.notificationM.reset();
    }
    gotoProUser() {
        this.deleteUser.forEach(userId => {
            this._usersService.getProfileById(userId).subscribe(
                result => {
                    if (result.data[0].userType == "Paid" && result.data[0].subscription[0].status == "active") {
                        this.userProPlan = result.data[0].subscription[0].planName
                    }
                    else {
                        this.userProPlan = []
                    }
                })
        });


        if (this.deleteUser.length == 0) {
            swal("Please Select  User");
        } else if (this.deleteUser.length > 1) {
            swal("Please Select any One User");
        } else {
            jQuery('#proUser').modal('show')
        }
        this._usersService.getplans().subscribe(
            result => {
                this.planData = result.data;

            })
    }
    sendpro() {
        this._usersService.addProUserPlan(this.deleteUser, this.proPlanData).subscribe(
            result => {
                this.getAll(this.p)
                jQuery('#proUser').modal('hide')
                swal("Success!", "subscription Added Successfully!", "success");
                this.deleteUser = [];
                this.proPlanData = [];

            })
    }
    gotocheckplan(id) {
        this.proPlanData = id;

    }

    AfileChange(input) {
        if ((input.files[0].size / 1000) > 300) {
            jQuery("#upImg").show();
            jQuery("#eupImg").show();
        } else {
            jQuery("#upImg").hide();
            jQuery("#eupImg").hide();
            const reader = new FileReader();
            if (input.files.length) {
                const file = input.files[0];
                reader.onload = () => {
                    this.Aimage = reader.result;
                    // console.log("--------------->", this.Aimage)

                    this._usersService.uploadImgInServer(this.Aimage).subscribe(
                        result => {
                            this.fullImagePath = result.res

                        })
                }
                reader.readAsDataURL(file);
            }
        }
    }
    AremoveImage(): void { this.Aimage = ''; }
    // gotoXlfileOpen(inputFile) {
    //     console.log("-==============>inputFileinputFileinputFile", inputFile)
    //     var url = inputFile;
    //     var oReq = new XMLHttpRequest();
    //     oReq.open("GET", url, true);
    //     oReq.responseType = "arraybuffer";

    //     oReq.onload = function(e) {
    //         var arraybuffer = oReq.response;

    //         /* convert data to binary string */
    //         var data = new Uint8Array(arraybuffer);
    //         var arr = new Array();
    //         for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
    //         var bstr = arr.join("");

    //         /* Call XLSX */
    //         var workbook = XLSX.read(bstr, {
    //             type: "binary"
    //         });

    //         /* DO SOMETHING WITH workbook HERE */
    //         var first_sheet_name = workbook.SheetNames[0];
    //         /* Get worksheet */
    //         var worksheet = workbook.Sheets[first_sheet_name];
    //         console.log(XLSX.utils.sheet_to_json(worksheet, {
    //             raw: true
    //         }));
    //     }

    //     oReq.send();
    // }
    showUnitName(unit) {

        //  console.log(e.target.value)
        switch (parseInt(unit)) {
            case 0:
                return 'Others';

            case 1:
                return 'Km';

            case 2:
                return "Miles";

            case 3:
                return 'Years';

            case 4:
                return 'Kg';

            case 5:
                return 'Pound';
            case 6:
                return 'cm';

        }

    }
    deviceType(num) {
        switch (parseInt(num)) {

            case 1:
                return 'IOS';

            case 2:
                return "Android";

            case 3:
                return 'Browser';

        }
    }
}

export function emailValidator(control: FormControl): {
    [key: string]: any
} {
    var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
    if (control.value && !emailRegexp.test(control.value)) {
        return {
            invalidEmail: true
        };
    }
}

export function mobileValidator(control: FormControl): {
    [key: string]: any
} {
    var mobileRegexp = /^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/;
    if (control.value && !mobileRegexp.test(control.value)) {
        return {
            invalidMobile: true
        };
    }
}
export function numValidator(control: FormControl): {
    [key: string]: any
} {

    var Regexp = /^[0-9]*\.?[0-9]*$/;
    if (control.value && !Regexp.test(control.value)) {
        return {
            invalidNumber: true
        };
    }
}
export function urlValidator(control: FormControl): {
    [key: string]: any
} {

    var Regexp = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;

    if (control.value && !Regexp.test(control.value)) {
        return {
            invalidUrl: true
        };
    }
}

export function lengthValidator(control: FormControl): {
    [key: string]: any
} {

    if (control.value.length >= 45) {
        // alert("success");
        return {
            checkLength: true
        };
    }


}

export function lengthValidatorr(control: FormControl): {
    [key: string]: any
} {

    if (control.value.length >= 100) {
        // alert("success");
        return {
            checkLengthh: true
        };
    }

}




