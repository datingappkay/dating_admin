import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModuleWithProviders } from '@angular/core';
import { routing } from './users.routing';

//============ importing Ng2PaginationModule,AgmCoreModule,MyDatePickerModule  ================
import { Ng2PaginationModule } from 'ng2-pagination'; //importing ng2-pagination
import { AgmCoreModule } from 'angular2-google-maps/core';
import { MyDatePickerModule } from 'mydatepicker';
import { NouisliderModule } from 'ng2-nouislider';
import { CKEditorModule } from 'ng2-ckeditor';

//============ importing UsersComponent  ================
import { UsersComponent } from './components/users.component';
//============ importing AgePipe,UserType filter pipes ================
import { AgePipe } from '../pipes/age.pipe';
import { UserType } from '../pipes/usertype.pipe';
import { PrfPic } from '../pipes/profilepicture';
import { RecentVisitorsComponents } from './recentVisitors/recentVisitors.components';
import { Likes } from "./likes/likes.components";
import { DisLikes } from "./disLikes/disLikes.components";
import { Matches } from "./matches/matches.components";
import { DeviceLogs } from "./deviceLogs/deviceLogs.components"
// import {BannedUserPageComponents} from "./bannedUsersPage/bannedUserPage.components"
// import {ReportUserPageComponents} from "./reportedUsersPage/reportedUserPage.components"
import { ReportedInDetailsComponents } from "./reportedInDetails/reportedInDetails.components";
// import { deactivatePageComponents} from "./deactivatePage/deactivatePage.components";
import { MyDisLikes } from "./mydisLikes/mydisLikes.components";
import { MyLikes } from "./mylikes/mylikes.components"

import { superLikes } from "./superLikes/superLikes.components";
import { FileUploadModule, FileUploader } from 'ng2-file-upload';
//import {FileUploader} from 'ng2-file-upload';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper'; // me
//import{ UsersComponent} from './components/users.component';
import { MySuperLikes } from './MySuperLikes/MySuperLikes.components';
import { BlockUserDetailComponents } from './BlockUserDetail/BlockUserDetail.components';
// import { NouisliderModule } from 'ng2-nouislider';

@NgModule({

  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    routing,
    Ng2PaginationModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCEIFo_lhymH_7uUR375RmK7_2v5GG6jJI', // datum bee api key
      // apiKey:  'AIzaSyD-iTOQrT6uPa2tfz_zjGBr9z7MmJ_VJNE', // datum api key
      libraries: ['places']
    }),
    MyDatePickerModule,
    NouisliderModule,
    CKEditorModule,
    routing,
    FileUploadModule,

  ],
  declarations: [
    UsersComponent,

    AgePipe,
    UserType,
    PrfPic,
    RecentVisitorsComponents,
    Likes,
    superLikes,
    DisLikes,
    Matches,
    DeviceLogs,

    ReportedInDetailsComponents,

    ImageCropperComponent,
    MyDisLikes,
    MyLikes,
    MySuperLikes,
    BlockUserDetailComponents,

  ]
})
export class UsersModule { }
