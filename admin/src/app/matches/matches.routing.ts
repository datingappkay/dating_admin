//importing all the essential modules
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//importing all the custom components
//============ importing Matches Componentt ================
import { MatchesComponent } from './components/matches.component';
 
 

export const matchesRoutes: Routes = [ 
         {
       path: '',
       component: MatchesComponent,
         data:{
         title: ''
       }
      }
 
];
export const routing: ModuleWithProviders = RouterModule.forChild(matchesRoutes); // not required


