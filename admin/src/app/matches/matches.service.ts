import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../app.constant';

@Injectable()
export class MatchesService {

    constructor(private http: Http, public _config: Configuration) {
    }

    
    getData() {
        let url = this._config.Server + 'reportReasons';
        return this.http.get(url,{ headers: this._config.headers }).map(res => res.json());
    }

    getlang() {
        let url = this._config.Server + "language/" + 1;
        return this.http.get(url,{ headers: this._config.headers }).map(res => res.json());
    }

    addResonce(reportReasons,reasonArray): Observable<any> {
        let url = this._config.Server + "reportReasons";
        let body = {reportReason:reportReasons,reason:reasonArray}
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }

    deleteUser(UserId) {
        console.log(UserId);
        let url = this._config.Server + "reportReasons/"+UserId;
        console.log(url);
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }

        editReason(newReason,langName,_id): Observable<any> {
            let url = this._config.Server + "reportReasons";
            let body = { reportReason: newReason,reason:langName,reasonId:_id };
            return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
        }
}
