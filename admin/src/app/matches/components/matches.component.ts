import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, OnInit } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';

//importing service

//=================== importing form components ==================
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


//=================== importing Matches Service   =============
import { MatchesService } from '../matches.service';
import { UsersService } from '../../users/users.service';
import { ProfilesService } from '../../profiles/profile.service';
import { Console } from '@angular/core/src/console';
declare var swal: any;


@Component({
    selector: 'matches',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./matches.component.scss'],
    templateUrl: './matches.component.html',
    providers: [MatchesService, UsersService, ProfilesService]
})

export class MatchesComponent implements OnInit, AfterViewInit {
    addResonces: FormGroup;
    editResonces: FormGroup;
    loader = true;
    oldReason: any;
    newReason: any;
    data: any;
    es: any
    lang: any;
    deleteUserId: any;
    obj: any;
    reson: any;
    reportResonceName: any;
    public config: any;
    public configFn: any;

    constructor(private _matchesService: MatchesService, private _appConfig: AppConfig, private router: Router, vcRef: ViewContainerRef, private fb: FormBuilder) {
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
        this.addResonces = fb.group({
            'reportResonceName': ['', Validators.required],
        });

        this.editResonces = fb.group({
            'reportResonceName': [''],
            "newReason_id": [''],
        });


    }

    ngOnInit() {
        this._matchesService.getData().subscribe(result => {
            if (result.data) {
                this.data = result.data
            }
        });

        this._matchesService.getlang().subscribe(result => {
            console.log("======", result.result)
            if (result.result) {
                this.lang = result.result
            }
        });

    }
    languageAdd(event, i) {
        if (event.target.value.length > 0) {
            this.lang[i].langeugeCodVal = event.target.value;
            this.lang[i].checked = true;
        } else {
            this.lang[i].checked = false;
        }
    }

    submitForm(value) {
        var langName = [];
        this.lang.forEach(x => {
            if (x.checked == true) {
                var str = {
                    lanuageCode: x.code,
                    languageName: x.langeugeCodVal
                }
                langName.push(str);

            }
        });
        this._matchesService.addResonce(value._value.reportResonceName, langName).subscribe(
            (result) => {

                if (result) {
                    this.ngOnInit();
                    swal("Success!", "ReportReasons Added!", "success");
                    this.addResonces.reset();


                } else {
                    console.log("Reson NOT inserted");

                }
                jQuery('#addResonce').modal('hide');



            })
    }


    showModalDelete(_id) {
        this._matchesService
            .deleteUser(_id)
            .subscribe((result) => {
                if (result.code != 200) {
                    swal("Success!", "ReportReasons Deleted!", "success");
                    this.ngOnInit();

                } else {

                }

            })
    }



    editResonnce(reason) {
        console.log("---->", reason)
        var xx = reason.reason
        for (var x in xx) {
            jQuery('.text' + x).val(xx[x]);
            // console.log("xxxxxxxxxx", x);
            // console.log("reason", xx[x]);
        }
        this.es = reason.reason.es
        this.obj = reason;
        this.oldReason = reason.reportReason;
        jQuery('#newReason').val(this.oldReason);
        jQuery('#newReasonES').val(this.es);


    }

    updatedReason(reason) {

        var langName = [];
        this.lang.forEach(x => {
            if (x.checked == true) {
                var str = {
                    // lanuageCode: x.code,
                    // languageName: x.langeugeCodVal
                    [x.code]: jQuery('.text' + x.code).val()

                }
                langName.push(str);
            }
        });
        this.newReason = jQuery("input#newReason").val();

        this._matchesService.editReason(this.newReason, langName, this.obj._id).subscribe(
            result => {
                if (result.code == 200) {
                } else {
                }
                this.ngOnInit();
                swal("Success!", "ReportReasons Edited!", "success");
                this.editResonces.reset();

                jQuery('#editCategory').modal('hide');

            }
        )
    }

}
