import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../app.constant';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
declare var API_URL: string;
@Injectable()
export class subscriptionService {

    constructor(private http: Http, public _config: Configuration) {
    }

    getSubscription(offset, limit) {
        let url = this._config.Server + 'subscription?planType=Active' + '&offset=' + offset + '&limit=' + limit;
        //console.log("----->", url)
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getExpiredSubscription(offset, limit) {
        let url = this._config.Server + 'subscription?planType=Expired' + '&offset=' + offset + '&limit=' + limit;

        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }


    deleteSubcription(SubId): Observable<any> {
        let url = this._config.Server + "deleteMultiplesubscription";
        let body = {
            planId: SubId
        }
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }

    getUserBySearchtext(text) {
        //let url = this._config.Server + "subscription/" + text;
        let url = this._config.Server + "subscription?planType=Active" + '&text=' + text;


        console.log(text)
        return this.http.get(url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getUserBySearchtextofexpire(text) {
        //let url = this._config.Server + "subscription/" + text;
        let url = this._config.Server + "subscription?planType=Expired" + '&text=' + text;


        console.log(text)
        return this.http.get(url, { headers: this._config.headers })
            .map(res => res.json());
    }


  
}



