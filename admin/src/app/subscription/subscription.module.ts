import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 import { routing } from './subscription.routing';

//============ importing Ng2PaginationModule,AgmCoreModule,MyDatePickerModule  ================
import {Ng2PaginationModule} from 'ng2-pagination'; //importing ng2-pagination
//============ importing UsersComponent  ================
import {subscriptionComponent } from './components/subscription.component';
 
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    routing,
    Ng2PaginationModule
  ],
  declarations: [
    subscriptionComponent,
   ]
})
 export class subscriptionModule { }
 
                                     