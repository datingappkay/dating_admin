import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTableModule } from "angular2-datatable";
import { communityGuidelinesComponent } from './communityGuidelines/communityGuidelines.component';
import { CKEditorModule } from 'ng2-ckeditor';

export const routes = [

];
@NgModule({
  imports: [
    CommonModule,
    ChartsModule,
    RouterModule,
   
    CKEditorModule,
    RouterModule.forChild(routes),
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    communityGuidelinesComponent
  ],


})
export class CommunityGuidelinesModule {

}