import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, OnInit } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';

//importing service

//=================== importing form components ==================
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


//=================== importing Matches Service   =============
import { appConfigService } from '../appConfig.service';
import { UsersService } from '../../users/users.service';
import { ProfilesService } from '../../profiles/profile.service';
import { constructDependencies } from '@angular/core/src/di/reflective_provider';
declare var swal: any;
declare var $: any;
declare var sweetAlert: any;

@Component({
    selector: 'appConfig',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./appConfig.component.scss'],
    templateUrl: './appConfig.component.html',
    providers: [appConfigService, UsersService, ProfilesService]
})

export class appConfigComponent implements OnInit, AfterViewInit {
    public rowsOnPage = 10;
    msg = true;
    filter: FormGroup;
    editFilter: FormGroup;
    isMandatory = true;
    type = 1;
    Priority: any;
    data = [];
    isman:any;
    langData = [];
    status: any;
    obj = '';
    filterId: any;
    valCount = false;
    btnFlag = true;
    public config: any;
    public configFn: any;

    constructor(private _service: appConfigService, private _appConfig: AppConfig, private router: Router, vcRef: ViewContainerRef, private fb: FormBuilder) {
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
        this.filter = fb.group({
            'fieldName': ['', Validators.required],
            'type': ['', Validators.required],
            'titledropdown': ['', Validators.required],
          //  'titileLable': ['', Validators.required],
            'values': ['1'],
            'valuesOther': ['1'],
            'fieldNameOther': ['', Validators.required],
          //  'titleNameOther': ['', Validators.required],
            'isMandatory': 'true',
        });
        this.editFilter = fb.group({
            'newFieldName': [''],
            'newtype': [''],
            'newtitledropdown': [''],
           // 'newtitileLable': [''],
            'newvaluesOther': ['1'],
            'newvalues': ['1'],
            'newisMandatory': '',
        });
    }

    ngOnInit() {
        jQuery("#addDiv").hide();
        this.getVices(1)
        this._service.getLanguage().subscribe(
            res => {
                //   console.log("res.data", res.result)
                if (res.result && res.result.length > 0) {
                    this.langData = res.result;
                } else {
                    this.langData = [];
                }
            }
        )

    }
    refresh(){
        this.getVices(1)
    }
    getVices(status) {
        //console.log("this.status", this.status = status)
        this._service.getData(1).subscribe(
            res => {
                // console.log("=============>getVices", res.data.searchPreferences[0])
                if (res.data.searchPreferences[0]) {
                    this.data = res.data.searchPreferences[0];
                    this.msg = false;
                } else {
                    this.msg = true;
                    this.data = [];
                }
            }
        )
    }
    // getVirtues(status) {
    //     //console.log("this.status", this.status = status)

    //     this._service.getData(2).subscribe(
    //         res => {
    //             // console.log("=============>getVirtues", res.data.searchPreferences[0])
    //             if (res.data.searchPreferences[0]) {
    //                 this.data = res.data.searchPreferences[0];
    //                 this.msg = false;
    //             } else {
    //                 this.msg = true;
    //                 this.data = [];
    //             }
    //         }
    //     )
    // }
   
    


    checkValues(s) {
        if (s == 12) {
            jQuery("#addDiv").hide();
            jQuery("#editDiv").hide();
        } else {
            jQuery("#addDiv").show();
            jQuery("#editDiv").show();
        }
    }

    submitForm(val) {
        var otherName = [];

        this.langData.forEach(e => {
            let x = {
                langId: e.langId,
                langCode: e.code,
                fieldName: jQuery(".addField" + e.langId).val(),
                addtitle: jQuery(".addtitle" + e.langId).val(),
                values: jQuery(".addValues" + e.langId).val()
            }
            otherName.push(x);
        });
        val._value.otherName = otherName;
        // console.log("=============", val._value)
        this._service.addFilter(val._value).subscribe(
            res => {
                //     console.log("==========>res", res)
                if (res) {
                    swal("Success!", "Added Successfully!", "success");
                    jQuery('#addFilter').modal('hide');
                    this.getVices(1)
                    this.filter.reset();
                    this.type = 1;
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        )
    }

    gotoEditData(data) {
         // console.log("======>", data)
         this.editFilter.controls['newtitledropdown'].setValue(data.title);
         this.editFilter.controls['newtype'].setValue(data.type);

         //[formControl]="editFilter.controls['newtype']"
        jQuery(".eField").val('');
        jQuery(".eValues").val('');
        this.Priority = data.Priority
        this.type = data.type;
        this.obj= data;
        this.isman = data.mandatory
        var lables = data.otherLabel;
        var value = data.otherOptions
        var otherTitle = data.otherTitle
        this.filterId = data._id;
        this.langData.forEach(ele => {
            for (var langCode in lables) {
                if (ele.code == langCode) {
                    jQuery(".editField" + ele.langId).val(lables[langCode]);
                }
            }
            for (var val in value) {
                if (ele.code == val) {
                    jQuery(".editValues" + ele.langId).val(value[val]);
                }
            }
            for (var ttl in otherTitle) {
                if (ele.code == ttl) {
                    jQuery(".editTitle" + ele.langId).val(otherTitle[ttl]);
                }
            }

        });
        if (this.type == 12) {
            jQuery("#editDiv").hide();
        } else {
            jQuery("#editDiv").show();
        }
    }

    editFilterFunc(val) {
        var editOtherName = [];

        this.langData.forEach(e => {
            let x = {
                langId: e.langId,
                langCode: e.code,
                fieldName: jQuery(".editField" + e.langId).val(),
                values: jQuery(".editValues" + e.langId).val(),
                editTitle: jQuery(".editTitle" + e.langId).val()

            }
            editOtherName.push(x);
            //console.log("=============", editOtherName)
        });
        val._value.otherName = editOtherName;
        val._value.filterId = this.filterId;
        val._value.Priority = this.Priority;
        val._value.newisMandatory =(val._value.newisMandatory)?val._value.newisMandatory:this.isman;
       // val._value.newisMandatory = jQuery('#mandVal').is(':checked');


        // console.log("valediteeeeeeeee.", val._value);
        this._service.editFilter(val._value).subscribe(
            res => {
                //console.log("==========>res", res)
                if (res) {

                    swal("Success!", "Edited Successfully!", "success");
                    jQuery('#editFilter').modal('hide');
                    this.editFilter.reset();
                    this.getVices(1)

                    this.type = 1;
                } else {
                    sweetAlert("Oops...", res.message, "error");
                }
            }
        )

    }

    gotoFilter() {
        var title = jQuery("#filter").val()
      //  console.log("================>", jQuery("#filter").val())
        if (jQuery("#filter").val() != 1) {
            this._service.getData(2, title).subscribe(
                res => {
                    //console.log("=============>getVices", res.data.searchPreferences[0])
                    if (res.data.searchPreferences[0]) {
                        this.data = res.data.searchPreferences[0];
                        this.msg = false;
                    } else {
                        this.msg = true;
                        this.data = [];
                    }
                }
            )
        }
    }

    gotoDeleteFilter(deleteid) {
        var id = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Filter!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    id.deleteFilter(deleteid);
                    swal({
                        title: 'Delete!',
                        text: 'Filter Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your Filter is safe :)", "error");
                }
            });
    }
    deleteFilter(deleteid) {
        var deleteData = deleteid
        // console.log("filterId", deleteid);
        this._service.deleteFilter(deleteid._id).subscribe(
            res => {
                this.getVices(1)

            }
        )
    }
    gotoUpArrow(index) {
        // console.log("up arrow", index);
        if (index > 0) {
            const tmp = this.data[index - 1];
            // console.log("upId", this.category[index - 1].catId)
            // console.log("currentId", this.category[index].catId)
            this.data[index - 1] = this.data[index];
            this.data[index] = tmp;
            this._service.gotoRowsUpate(this.data[index].Priority, this.data[index - 1].Priority, 2).subscribe(
                res => {
                    this.getVices(1)

                }
            )
        }
        // console.log(this.category)
    }
    gotoDownArrow(index) {
        if (index < this.data.length) {
            const tmp = this.data[index + 1];
            this.data[index + 1] = this.data[index];
            this.data[index] = tmp;
            // console.log("downId", this.category[index + 1].catId, this.category[index + 1].name)
            // console.log("currentId", this.category[index].catId, this.category[index].name)
            this._service.gotoRowsUpate(this.data[index].Priority, this.data[index + 1].Priority, 2).subscribe(
                res => {
                    this.getVices(1)

                }
            )
        }
    }
}
