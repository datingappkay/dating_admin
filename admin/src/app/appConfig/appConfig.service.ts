import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../app.constant';

@Injectable()
export class appConfigService {

    constructor(private http: Http, public _config: Configuration) {
    }


    addFilter(val) {
        let url = this._config.Server + 'question';
        return this.http.post(url, JSON.stringify(val), { headers: this._config.headers }).map(res => res.json());
    }
    getData(status, title) {
        let url = this._config.Server + 'question?status=' + status + "&title=" + title;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getLanguage() {
        let url = this._config.Server + "language/" + 1;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    editFilter(val) {
        let url = this._config.Server + 'questions';
        return this.http.put(url, JSON.stringify(val), { headers: this._config.headers }).map(res => res.json());
    }
    deleteFilter(Id) {
        let url = this._config.Server + 'questions';
        let body = {
            id: Id
        }
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    gotoRowsUpate(catId, otherId, type) {
        let url = this._config.Server + "reorderpreference";
        let body = {
            currId: catId,
            otherId: otherId,
            type: type
        };
        console.log("priorityswaop", body)
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }
}
