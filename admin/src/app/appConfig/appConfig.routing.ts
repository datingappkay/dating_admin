//importing all the essential modules
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//importing all the custom components
//============ importing Matches Componentt ================
import { appConfigComponent } from './components/appConfig.component';
 
 

export const appConfigRoutes: Routes = [ 
         {
       path: '',
       component: appConfigComponent,
         data:{
         title: ''
       }
      }
 
];
export const routing: ModuleWithProviders = RouterModule.forChild(appConfigRoutes); // not required


