import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 import { ModuleWithProviders } from '@angular/core';
 import { routing } from './MatchesUser.routing';

//============ importing Ng2PaginationModule,AgmCoreModule,MyDatePickerModule  ================
import {Ng2PaginationModule} from 'ng2-pagination'; //importing ng2-pagination
import { AgmCoreModule } from 'angular2-google-maps/core';
import { MyDatePickerModule } from 'mydatepicker';
import { NouisliderModule } from 'ng2-nouislider';
import {CKEditorModule} from 'ng2-ckeditor';

//============ importing UsersComponent  ================
 import {Matches} from './matches/matches.components';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    routing,
    Ng2PaginationModule, 
     AgmCoreModule.forRoot({
       apiKey:  'AIzaSyCEIFo_lhymH_7uUR375RmK7_2v5GG6jJI', // datum bee api key
         // apiKey:  'AIzaSyD-iTOQrT6uPa2tfz_zjGBr9z7MmJ_VJNE', // datum api key
        libraries: ['places']
     }),
     MyDatePickerModule,
     NouisliderModule,
     CKEditorModule,
   routing,
    
  ],
  declarations: [
    Matches
    
    // matchUsersPageComponents
   
  ]
})
 export class MatchesUserModule {    }
 