import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
import { Configuration } from '../app.constant';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
//import {HttpClient, HttpParams, HttpRequest, HttpEvent, Http, Response, Headers, RequestOptions, RequestMethod} from '@angular/common/http';
declare var $q: any;

declare var API_URL: string;
@Injectable()
export class MatchesUserService {

    constructor(private http: Http, public _config: Configuration) {


    }

    getAllMatchUsers(offset, limit) {
        let url = this._config.Server + 'userMatchFilters' + '?offset=' + offset + '&limit=' + limit;

        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getSearchFromMatchesDates(user) {
        let url = this._config.Server + 'userMatchFilters?dateFrom=' + user.dateFrom + '&dateTo=' + user.dateTo;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }


    getUserBytext(text) {
        let Url = this._config.Server + "userMatchFilters?text=" + text;
        // if (text == "") {
        //     Url = this._config.Server + "users";
        // }

        console.log(text)
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getProfileById(id) {

      //  console.log("profileeeeeeeeeeeeee=================>", id)
        let url = this._config.Server + 'profile/' + id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getProfileByContectNumber(contectNumber) {
      //  console.log("profileeeeeeeeeeeeee", contectNumber)
        let url = this._config.Server + 'profile/contectNumber/' + contectNumber;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getProfileByEmail(email) {
        let url = this._config.Server + 'profile/email/' + email;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
  

}
// this could also be a private method of the component class
function handleError(error: any) {
    // log error
    // could be something more sofisticated
    let errorMsg = error.message || `Yikes! There was was a problem with our hyperdrive device and we couldn't retrieve your data!`
    console.error(errorMsg);

    // throw an application level error
    return Observable.throw(errorMsg);
}
