import { Routes, RouterModule } from '@angular/router';

import{ compaignUserPageComponent} from './compaignUserPage/compaignUserPage.component';
import { ModuleWithProviders } from '@angular/core';
import { targetedUserComponent} from './targetedUser/targetedUser.component';
import { compaignModule } from './compaign.module';
import { newCompaignsComponent} from './component/newCompaigns.component';
import { viewsCompaignComponent} from './viewsCompaign/viewsCompaign.component';
import { clickedCompaignComponent} from './clickedCompaign/clickedCompaign.component';


export const compaignroutes: Routes = [
  {
    path: '',
    component: compaignUserPageComponent,
    data:{
      title: ''
    }
  
},
{
    path: ':component',
      component: newCompaignsComponent,
    data:{
      title: 'New Compaign'
    }
  
},
{
  path: ':targetedUser',
  component: targetedUserComponent,
  data:{
    title: 'targetedUser'
  }

},
{
  path: ':viewsCompaign',
  component: viewsCompaignComponent,
  data:{
    title: 'views'
  }

},
{
  path: ':clickedCompaign',
  component: clickedCompaignComponent,
  data:{
    title: 'clickedCompaign'
  }

}
];


export const routing: ModuleWithProviders = RouterModule.forChild(compaignroutes);