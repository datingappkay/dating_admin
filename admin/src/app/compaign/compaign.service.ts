import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
import { Configuration } from '../app.constant';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { MessagesService } from '../theme/components/messages/messages.service';

//import {HttpClient, HttpParams, HttpRequest, HttpEvent, Http, Response, Headers, RequestOptions, RequestMethod} from '@angular/common/http';
declare var $q: any;

declare var API_URL: string;
@Injectable()
export class compaignService {

    constructor(private http: Http, public _config: Configuration) {


    }
    public loaderStatus: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    displayLoader(value: boolean) {
        this.loaderStatus.next(value);
    }
    uploadImgInServer(imgData) {
        let url = this._config.Server + "UploadImgInServer";
        let body = {
            activeimage: imgData
        }

        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());

    }
    getAllcompaign() {
        let url = this._config.Server + 'campaingDetails';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    getCompaignPagination(offset, limit) {
        let url = this._config.Server + 'campaingDetails?offset=' + offset + '&limit=' + limit;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }


    getTargetedUsers(id) {
        let Url = this._config.Server + "campaignUsersById?campaingId=" + id + "&type=1";
        console.log("url", Url)
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getViewsUsers(id) {
        let Url = this._config.Server + "campaignUsersById?campaingId=" + id + "&type=2";
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    getClickedUsers(id) {
        let Url = this._config.Server + "campaignUsersById?campaingId=" + id + "&type=3";
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    // getProfileById(id) {
    //     let url = this._config.Server + 'profile/' + id;
    //     return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    // }

    CreateUser(user) {

        let url = this._config.Server + 'campaignUsers?longitude=' + user.lng + '&latitude=' + user.lat + '&distance=' + user.distance + '&distanceType=' + user.distanceType;
        // let body 
        // = {


        //   latitude: parseFloat(user.lat),
        //   longitude: parseFloat(user.lng),
        //      distance:user.distance,
        //     distanceType: user.dropDownId

        // }
        console.log("dtrdrdrdrdrdsrddr", url);
        //console.log("successfully submitted");
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());

    }

    sendPushDetail(user) {

        let url = this._config.Server + 'notification';
        let body
            = {

            campainTitle: user.compaignTitle,
            startDate: user.startDate,
            endDate: user.endDate,
            city: user.city,
            country: user.country,
            campainImageUrl: user.image,
            pushTitle: user.line2,
            message: user.message,
            url: user.url,
            usersId: user,


        }
        console.log("send push notification", body);
        //console.log("successfully submitted");
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());

    }
    sendNotification(message) {

        let url = this._config.Server + "campaingDetail";
        let body = {
            message: message.message,
            campainImageUrl: message.campainImageUrl,
            pushTitle: message.pushTitle,
            url: message.url,
            campainTitle: message.campainTitle,
            startDate: message.startDate,
            endDate: message.endDate,
            usersId: message.usersId,
            city: message.city,
            country: message.country

        }
        console.log("send push notificationffffffffffffffffffffffff", body);

        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());

    }
    SendNotificationToAll(userId, data) {
        let url = this._config.Server + "campaingNotifications";
        let body = {

            usersId: userId,
            url: data.url,
            campainImageUrl: data.campainImageUrl,
            message: data.message,
            pushTitle: data.pushTitle,
            campainTitle: data.campainTitle,

        }
        console.log("send push notification^^^^^^^^", body);

        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    //  sendNotification(content,message, userIds) {
    //         let options = new RequestOptions({
    //         method: RequestMethod.Post,
    //         headers: contentHeaders
    //     });
    //     let body = JSON.stringify({content:content, mess: message.message,type: message.type,image: message.image,numberOfUser: message.numberOfUser,line2:message.line2,url:message.url, userId: userIds });
    //      let Url = API_URL + "/notification";
    //     return this.http.post(Url, body, options)
    //         .map(res => res);
    // }
    getProfileById(_id) {
        let url = this._config.Server + 'profile/' + _id;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    deleteUser(UserId) {
        let url = this._config.Server + "campaing/" + UserId;
        // console.log("before", UserId)
        // let body = {
        //     usersId: UserId
        //         }
        // console.log("before", UserId)
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }
    getnewCompaignPage(offset, limit) {
        let Url = this._config.Server + 'campaignUsers?offset=' + offset + '&limit=' + limit;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());

    }
}
// this could also be a private method of the component class
function handleError(error: any) {
    // log error
    // could be something more sofisticated
    let errorMsg = error.message || `Yikes! There was was a problem with our hyperdrive device and we couldn't retrieve your data!`
    console.error(errorMsg);

    // throw an application level error
    return Observable.throw(errorMsg);
}
