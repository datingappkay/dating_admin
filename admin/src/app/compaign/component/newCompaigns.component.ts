import { Component, ViewEncapsulation, Input, ViewChild, Inject, OnInit, ElementRef, NgZone, ChangeDetectorRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppConfig } from "../../app.config";
import {
    Router
} from '@angular/router';

import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
//import { IMyDpOptions } from 'mydatepicker';
import { FileUploaderModule } from "ng4-file-upload";
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormControl,
    FormArray
} from '@angular/forms';
//import { SearchPipe } from "../../theme/pipes/search/search.pipe";
import {
    FileUploader, FileSelectDirective
} from 'ng2-file-upload';
//import {videoPlayer} from '@angular-video';
import {
    AgmCoreModule,
    MapsAPILoader
} from 'angular2-google-maps/core';
import {
    ImageCropperComponent,
    CropperSettings,
    Bounds
} from 'ng2-img-cropper';

//importing service


import { Subject } from 'rxjs/Subject'

import {
    AppState
} from "../../app.state";

// import "../../../../node_modules/jquery/dist/jquery-ui.css";
import { Subscription } from 'rxjs/Subscription';

import 'rxjs/add/observable/timer';

import "../../../../node_modules/jquery/dist/jquery-1.9.1.js";
import "../../../../node_modules/jquery/dist/jquery-ui.js";
import { validateConfig } from '@angular/router/src/config';
import { resolve } from 'path';
import { Pipe, PipeTransform } from '@angular/core';
import { MyDatePickerModule } from 'mydatepicker';
import { compaignService } from '../compaign.service';
import { UsersService } from '../../users/users.service';

declare var swal: any;
declare var sweetAlert: any;
declare var document: any

@Component({
    selector: 'newCompaignsComponent',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./newCompaigns.component.scss'],
    templateUrl: './newCompaigns.component.html',
    providers: [compaignService, UsersService]
})



export class newCompaignsComponent {
    toggleBool: boolean = true;
    private loader: boolean = false;
    private Spinner: boolean = false;
    myForm: FormGroup;
    public mapmodal: boolean;
    public users: any[];
    checkedTick: any;
    SelectType = 'KM';
    zoom: number = 8;
    // // initial center position for the map
    lat: number = 51.673858;
    lng: number = 7.815982;
    showId = false;
    public croppingBoxPush: boolean = false;
    public detailsBoxPush: boolean = false;
    private hasError: boolean = false;
    private hasErrorE: boolean = false;
    private data2: any;
    public fullImagePath: any;
    private notificationM: FormGroup;
    private newMsg: boolean = false;
    searchControl = new FormControl(); // search
    private pushStatus: boolean;
    cropperSettings2: CropperSettings;

    private pageNum: number = 1;
    //public p = 1;
    public flag: number = 0;
    // public Status: number;
    public count: any;
    public usersCountData: any;
    public likesData: any;
    public viewsData: any;
    public profileExist: any;
    public unLikesData: any;
    public matchesData: any;
    public image: any;
    private name: any;
    private id: any;
    public checkBtn: any;
    elementRef: ElementRef;
    // form1: FormGroup;

    myFormUpDateUser: FormGroup;
    addUser: FormGroup;
    private editUserForm: FormGroup; // edit
    private userDetailForm: FormGroup;

    public flage: string;
    private imgageurl: any;
    public taxesSearch = []
    public userLog: any[];

    email = new FormControl();
    bannedError = "";
    public preferences: any[];
    //public prefDiscovery: any[];
    public prefUser: any[];
    public preferencesUp: any[];
    public prefUpUser: any[];
    public prefsettings: any[];
    public prefsettingsUser: any[];
    public addForm: boolean = false;
    private values: any[] = []; // me
    private valuesUser: any[] = []; // me
    private imageurl: any[] = [];
    private videourl: any[] = [];
    public minAge: any;
    private nodata: boolean = false;
    private nosearchdata: boolean = false;
    private previewNotification: boolean = false;
    private hasMail: boolean = false;

    private userById: any; // for edit
    private usr: any;
    private errCode: number;
    private Strt: any;
    private Endd: any;
    private nouserresult: any;

    public mapmodalUp: boolean;
    public alertMsg: boolean;
    public msg: string;
    public activePageTitle: string = '';
    public Btn: string = '';
    public file_srcs: any[] = []; // me
    public file_srcsUp: any[] = []; // me
    public debug_size_before: any[] = []; // me
    public debug_size_after: any[] = []; // me
    public croppingBox: boolean = false;
    public croppingBoxUp: boolean = false;

    public searchEnabled = 0;
    public searchTerm = '';
    //for cropping images
    cropperSettings: CropperSettings;
    private data: any;
    private dataUp: any;

    private changeImage: boolean = false;
    private cropper: boolean = false;

    private uploaded: boolean = false;
    private user: any;

    private someRange: any = [0, 1000];
    private content: any;
    public slide: any;
    private m_names: any = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
    public myDatePickerOptions: any;
    public valuesExist: boolean = false;
    public valuesExistUser: boolean = false;
    private ids: any[] = [];
    public favPreference = [];
    private FbIdd: any;
    private ProfilePicc: any;
    private _idd: any;
    private userr: any;
    private Namee: any;
    public sliderBtn = [];
    private banStatus: any;

    deleteUserId = "";
    banndUserId = "";
    banndUserName = "";
    banndemail = "";
    emailAreadyTaken = false;
    numberAreadyTaken = false;
    public deleteUser = [];
    public namedata = [];
    public fullVideoPath = "";
    public myPlayer: any;
    public playerHTML: any;
    public pMsg: boolean = false;
    val: any
    public showloader: boolean = false;
    private subscription: Subscription;
    private timer: Observable<any>;
    public myglobalUrl: any;
    public pushTitle: any;
    public pushMsg: any;
    public rowsOnPage = 10;
    public p = 1;


    userDetails = {
        Name: "Rahul",
        PhoneNo: "+919672829202",
        Email: "Rahul@mobifyi.com",
        RegistrationDate: "Friday, January 5, 2018 5:39:37 AM",
        DateOfBirth: "17/08/1992",
        LastLogin: "Friday, January 5, 2018 5:39:37 AM",
        Age: "25",
        Gender: "Male",
        Height: "166 cm , 5'6\" feet",
        ProfileVideo: "https://s3.amazonaws.com/sync1to1/SYNC1TO1Video20180104114749AM.mp4",
        RegisteredFrom: "Bengaluru / india",
        ProfilePhoto: "https://s3.amazonaws.com/datumv3/Sync1To1Image20180104114737AM.png",
        OtherPhotos: [
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png",
            "https://s3.amazonaws.com/datumv3/Sync1To1Image20171225091921PM.png"
        ],
        OtherImages: [],
        Preferences: [

        ]
    }

    constructor(private _loader: MapsAPILoader, private router: Router, private _zone: NgZone, private formBuilder: FormBuilder,
        private _compaignService: compaignService, private _usersService: UsersService) {


        var today = new Date();



    }

    ngOnInit() {

        this.getPage(this.p)
        this.myglobalUrl = 'https://admin-files.muonhenho.com/datum_2.0-admin/campagin.png'

        this.myForm = this.formBuilder.group({
            options: ['', Validators.required],
            distance: ['', Validators.required],
            lat: ['', Validators.required],
            lng: ['', Validators.required],
            compaignTitle: ['', Validators.required],
            startDate: ['', Validators.required],
            endDate: ['', Validators.required],
            city: ['', Validators.required],
            country: ['', Validators.required]
        })



        this.notificationM = this.formBuilder.group({
            // "type": ['', Validators.required],
            "line2": ['', Validators.compose([Validators.required, lengthValidator])],
            "message": ['', Validators.compose([Validators.required, lengthValidatorr])],
            // "image": [''],
            "url": ['', Validators.compose([urlValidator])],
            // "numberOfUser": ['']

        })



    }


    public uploader: FileUploader = new FileUploader({ url: URL, itemAlias: 'newProfilePicture' });

    title: string;
    previewImage: any;
    tempImage: any;
    source: string;

    getToday(): string {
        return new Date().toISOString().split('T')[0]
    }
    getPage(p) {
        this._compaignService
            .getnewCompaignPage(p, this.rowsOnPage).subscribe((data) => {
                {

                    this.p = p;

                }
            });
    }
    setDate(): void {
        // Set today date using the patchValue function
        let date = new Date();
        this.myForm.patchValue({
            dob: {
                date: {
                    year: date.getFullYear(),
                    month: date.getMonth() + 1,
                    day: date.getDate()
                },


            }
        });


    }

    selectAllCheckBox(
    ) {
        this.deleteUser = [];
        this.users.forEach((obj) => {
            obj.checked = !obj.checked;
            if (obj.checked) {
                this.deleteUser.push({ _id: obj._id, deviceType: obj.deviceType })
                this.toggleBool = false;
            }
            else {
                this.toggleBool = true;
            }
            // this.gotocheck(obj.name, event);
        });
        //console.log("===>>", this.deleteUser)

    }

    gotocheck(id) {

        var index = this.deleteUser.findIndex(x => x._id == id._id);
        //console.log("index", index);
        if (index > -1) {
            this.deleteUser.splice(index, 1)
        } else {
            this.deleteUser.push({ _id: id._id, deviceType: id.deviceType })
        }
        //console.log("selected:-", this.deleteUser);
    }


    // this.deleteUser.push(id);



    clearDate(): void {
        // Clear the date using the patchValue function
        this.myForm.patchValue({ dob: null });
    }
    toggleId() {
        this.showId = true;
    }
    backToUsers() {
        this.router.navigate(['/pages/compaign']);
    }
    croppedPush(e, i) {
        //console.log(this.image) // for cropping
        // this.cropper = true;
    }
    initMap(myMap) {

        //myMap.triggerResize() // to resize map on popup

        this._loader.load().then(() => {
            var control = document.getElementById("cityTxt");
            var input = /** @type {!HTMLInputElement} */ (control);
            //var input = <HTMLInputElement>document.getElementById("cityTxt");
            var autocomplete = new google.maps.places.Autocomplete(input, { types: ['geocode'] });
            google.maps.event.addListener(autocomplete, 'place_changed', () => {
                this._zone.run(() => {
                    let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                    //set latitude and longitude
                    var address = place.formatted_address;
                    var res = place.address_components;
                    this.val = address
                    //console.log("place-place-place", place)
                    this.lat = place.geometry.location.lat();
                    this.lng = place.geometry.location.lng();
                    var add = place.formatted_address;
                    var value = add.split(",");
                    var count = value.length;
                    var country = value[count - 1];
                    var city = value[count - 3];
                    //console.log(city + country)
                    this.myForm.controls['lat'].setValue(this.lat)
                    this.myForm.controls['lng'].setValue(this.lng)
                    this.myForm.controls['city'].setValue(city)
                    this.myForm.controls['country'].setValue(country)
                    //console.log(this.myForm.value)


                });
            });

        }); // not in oninit
    }

    initCountryMap(myMap) {

        //myMap.triggerResize() // to resize map on popup

        this._loader.load().then(() => {
            var control = document.getElementById("countryTxt");
            var input = /** @type {!HTMLInputElement} */ (control);
            //var input = <HTMLInputElement>document.getElementById("cityTxt");
            var autocomplete = new google.maps.places.Autocomplete(input, { types: ['geocode'] });
            google.maps.event.addListener(autocomplete, 'place_changed', () => {
                this._zone.run(() => {
                    let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                    //set latitude and longitude

                    var address = place.formatted_address;
                    var res = place.address_components;

                    //  //console.log(address)
                    this.lat = place.geometry.location.lat();
                    this.lng = place.geometry.location.lng();
                    var add = place.formatted_address;
                    var value = add.split(",");
                    var count = value.length;
                    var country = value[count - 1];
                    var city = value[count - 3];
                    //    //console.log(city + country)
                    this.myForm.controls['lat'].setValue(this.lat)
                    this.myForm.controls['lng'].setValue(this.lng)
                    this.myForm.controls['city'].setValue(city)
                    this.myForm.controls['country'].setValue(country)
                    //console.log(this.myForm.value)


                });
            });

        }); // not in oninit
    }


    placeholder = "Enter City Name";
    radioChecked = function () {

        if (jQuery("input[name='type']:checked").val() == 'city') {
            this.placeholder = "Enter City Name";
            this.initMap();
            document.getElementById("city").checked = true;

            //console.log("Enter City Name")

        } else if (jQuery("input[name='type']:checked").val() == 'country') {
            this.placeholder = "Enter Country Name";
            this.initCountryMap();
            document.getElementById("country").checked = true;


            //console.log("Enter Country Name")

        }
        else {
            this.placeholder = "";
            //console.log("You are out of condition")
        }

        // this.placeholder="Enter City Name";

    }

    showPopup = function () {
        (jQuery('#dropDownId :selected').text())

        // jQuery("#dropDownId option[value='KM']").attr("selected", "selected");

    }
    loading: boolean = false;


    UploadImg() {
        this.croppingBox = true;
        jQuery('#croppingBox').modal('show')

        // called each time file input changes

    }
    confirmUploadPush() {
        this._compaignService.uploadImgInServer(this.image).subscribe(
            result => {
                this.fullImagePath = result.res

            })


    }

    setDefaultImg() {
        // var image=document.getElementById("input1").value;
        var image = (<HTMLInputElement>document.getElementById('input1')).value;
        if (image == null || image == '') {

            document.getElementById('input1').setAttribute('src', this.myglobalUrl);
        }
    }

    public url;

    readUrl(event: any) {
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();

            reader.onload = (event: any) => {
                this.image = event.target.result;
                //console.log("===============", this.image)
            }

            reader.readAsDataURL(event.target.files[0]);
        }
    }
    pushFormDetails() {
        this.detailsBoxPush = true;
        jQuery('#detailsBoxPush').modal('show')
    }
    saveFormPush({ valid }: { valid: boolean }) {
        this.newMsg = false
        if (valid) {
            //console.log(this.notificationM.value);
            this.hasError = false


            this.detailsBoxPush = false;
            jQuery('#detailsBoxPush').modal('hide')

        } else {
            this.hasError = true
        }

    }
    // public pushTitle: string;
    // public pushMsg: string;


    pushUsers(notificationM) {
        this.pMsg = true;
        //console.log("notificationM", notificationM._value);
        this.pushTitle = notificationM.value.line2;
        this.pushMsg = notificationM.value.message;
        jQuery('#detailsBoxPush').modal('hide')
        //this.pMsg=true;
    }
    closFormPush() {
        this.detailsBoxPush = false;
        jQuery('#detailsBoxPush').modal('hide')
    }



    onSendPush() {

        this.myForm.value.image = this.fullImagePath;
        this._compaignService
            .sendPushDetail(this.myForm.value)
            .subscribe((data) => {

                this.users = data.data;

                // console.log("Successfully added..................",this.users)
                //console.log("successfully submitted");


            });

    }

    public pushData = [];
    public pushData1 = [];
    public pushData2 = [];
    onSubmit() {
        this.showloader = true;

        this.timer = Observable.timer(500); // 5000 millisecond means 5 seconds



        this.myForm.value.distanceType = this.SelectType;
        this._compaignService
            .CreateUser(this.myForm.value)
            .subscribe((data) => {
                console.log("ur out", data)
                if (data.data.length > 0) {
                    this.users = data.data;
                    this.nosearchdata = false
                    this.namedata=data.data;
                    this.taxesSearch=[]
                    this.users.forEach(e => {
                        this.pushData.push(e._id);
                        console.log("ur in ifffff")
                    });
                } else {
                    this.users = [];
                    this.nosearchdata = true
                    console.log("ur in elseeeeeeeeeeeee")

                }



            });
        this.subscription = this.timer.subscribe(() => {
            // set showloader to false to hide loading div from view after 5 seconds
            this.showloader = false;
        });
    }

    gotoSearch(value) {
        //console.log("=-==",value)
        //this.namedata=[]
        var searchData = [];
        if (value) {
            // this.namedata = this.users;
            this.namedata.map(data => {
                if (data.firstName.toUpperCase().indexOf(value.toUpperCase()) > -1) 
                {
                   // this.users=[]
                  // console.log("==============>",data);
                   searchData.push(data);
                   // this.taxesSearch.push(data);
                   
                }
            });
           // console.log('searchData', searchData)
            this.users =  searchData;
            
        }else{
           this.users = this.namedata 
        }
        
        //this.users=this.namedata
       // console.log("jadu gar ka jadu sabK yeh saval hain karte ho tum kesew ", this.users)
    }






    sendNotification() {

        jQuery('#sendPush').modal('show')

        // if (valid) {

        var notifyiusersId = this.pushData;
        //console.log(this.notificationM.value);
        this.hasError = false

        this.notificationM.value.image = this.fullImagePath;


        //console.log(" notificatiion", this.notificationM.value)
        //console.log(" myform", this.myForm.value)
        var body = {
            campainTitle: this.myForm.value.compaignTitle,
            startDate: this.myForm.value.startDate.epoc,
            endDate: this.myForm.value.endDate.epoc,

            pushTitle: this.notificationM.value.line2,
            message: this.notificationM.value.message,
            url: this.notificationM.value.url,
            usersId: this.deleteUser,
            city: this.myForm.value.city,
            country: this.myForm.value.country,
            campainImageUrl: this.notificationM.value.image || this.myglobalUrl,
        }
        // //console.log("+++++++++++++++",body, this.deleteUser)
        this._compaignService.sendNotification(body)
            .subscribe((data) => {
                this._compaignService.SendNotificationToAll(this.deleteUser, body)
                    .subscribe((data) => {
                        // //console.log("body dataaaaaaaaaaaaaaaa", body)
                        if (data.code !== 200) {
                            //  this.pMsg = true
                            swal("Success!", "Notification Send Successfully!", "success");
                            this.deleteUser = [];
                            this.users = [];


                        }
                        else {
                            //     this.pMsg = false
                            this.hasError = true
                        }
                    })
            })
        jQuery('#sendPush').modal('hide')

    }

    closeModal() {
        jQuery('#sendPush').modal('toggle');
    }
    //public pMsg: boolean= true;
    onSendClick() {
        this._compaignService
            .CreateUser(this.myForm.value)
            .subscribe((data) => {

                this.users = data.data;

                //console.log("Successfully  added..................", data.data)
                //console.log("successfully submitted");


            });


    }
    public setTimer() {

        // set showloader to true to show loading div on view
        this.showloader = true;

        this.timer = Observable.timer(5000); // 5000 millisecond means 5 seconds
        this.subscription = this.timer.subscribe(() => {
            // set showloader to false to hide loading div from view after 5 seconds
            this.showloader = false;
        });
    }
    DremoveImage(): void { this.image = ''; }

}

export function lengthValidator(control: FormControl): {
    [key: string]: any
} {

    if (control.value.length >= 45) {
        // alert("success");``
        return {
            checkLength: true
        };
    }


}

export function lengthValidatorr(control: FormControl): {
    [key: string]: any
} {

    if (control.value.length >= 100) {
        // alert("success");
        return {
            checkLengthh: true
        };
    }

}
export function urlValidator(control: FormControl): {
    [key: string]: any
} {

    var Regexp = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;

    if (control.value && !Regexp.test(control.value)) {
        return {
            invalidUrl: true
        };
    }
}