 import { Routes, RouterModule }  from '@angular/router';

import { coinWalletsComponent } from './coinWallets/coinWallets.component';
import { ModuleWithProviders } from '@angular/core';

export const CoinWalletroutes: Routes = [ 
 {
       path: '',
       component: coinWalletsComponent,
       data:{
         title: 'Coin Wallet'
       }
     
 },
 {
 path: 'coinConfig',
 component: coinWalletsComponent,
 data:{
   title: 'Coin Wallet'
 }

}
];

export const routing: ModuleWithProviders = RouterModule.forChild(CoinWalletroutes);