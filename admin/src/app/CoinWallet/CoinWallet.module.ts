import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 import { ModuleWithProviders } from '@angular/core';
 import { routing } from './CoinWallet.routing';

//============ importing Ng2PaginationModule,AgmCoreModule,MyDatePickerModule  ================
import {Ng2PaginationModule} from 'ng2-pagination'; //importing ng2-pagination
//============ importing UsersComponent  ================
import { coinWalletsComponent } from './coinWallets/coinWallets.component';
import { MyDatePickerModule } from 'mydatepicker';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    routing,
    Ng2PaginationModule,
    MyDatePickerModule
  ],
  declarations: [
    coinWalletsComponent,
    
   ]
})
 export class CoinWalletModule { }
 
                                     