import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit, OnInit } from '@angular/core';
import { AppConfig } from "../../../app.config";
import { Router } from '@angular/router';

//=================== importing form components ==================
import { FormGroup, FormBuilder, Validators, AbstractControl, FormArray, FormControl } from '@angular/forms';

//=================== importing Profiles Service =============
import { ProfilesService } from '../../profile.service';
import { UsersService } from '../../../users/users.service';

@Component({
    selector: 'profileViews',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./profileViews.component.scss'],
    templateUrl: './profileViews.component.html',
    providers: [ProfilesService, UsersService]
})

export class ProfileViewsComponent implements OnInit, AfterViewInit {
    private TOP: any;
    public loader: boolean = false;
    public profViews: any;
    public profileExist: any;
    private noViewsdata: boolean = false;
    public viewsSummaryData: any;
    public config: any;
    public configFn: any;
    searchViews = new FormControl();

    constructor(private _profileService: ProfilesService, private _usersService: UsersService, private _appConfig: AppConfig, private router: Router, vcRef: ViewContainerRef, private fb: FormBuilder) {
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;

    }

    ngOnInit() {

        this.getViews()
        this.searchViews.valueChanges
            .debounceTime(400)
            .distinctUntilChanged()
            .flatMap(seachControl => this._profileService.searchViews(seachControl))
            .subscribe((data: any) => {
                //console.log(data)
                if (data.errCode == 201) {
                    this.noViewsdata = false
                    this.profViews = data.response.data
                }
                else if (data.errNum == 400) {
                    this._profileService.getViews()
                        .subscribe((res: any) => {
                            if (data) {
                                this.noViewsdata = false
                                this.profViews = res.response.data;
                            }
                            else {
                                this.noViewsdata = true
                            }
                        });
                }
                else if (data.errNum == 135) {
                    this.profViews = [];
                    this.noViewsdata = true
                }
            });
    }


    profile(fbid) {
        this.loader = true

        this._profileService
            .getProfileByFbId(fbid, 1)
            .subscribe((data) => {
                if (data.errCode == 201) {
                    console.log(data)
                    this.loader = false
                    this.profileExist = data.response.data;


                }
                this.loader = false
                error => console.log(error)
            });
    }
    profilePic(value) {
        if (value == null || value == "" || value == "string") {
            return "assets/img/app/noimage.png"

        }
        return value;
    }
    refresh() {
        this.getViews()
    }


    ngAfterViewInit() {

    }

    private getViews(): void {
        this._profileService
            .getViews()
            .subscribe((data: any) => {
                {
                    if (data.errCode == 201) {
                        this.profViews = data.response.data
                        this.noViewsdata = false
                    } else {
                        this.noViewsdata = true
                    } console.log(data)

                }

            })
    }


    getViewsData(id) {
        this.loader = true
        this._usersService
            .getViewsByFbId(id)
            .subscribe((data) => {

                if (data.errCode == 201) {
                    console.log(data)
                    this.loader = false
                    this.viewsSummaryData = data.response.data;
                }
                this.loader = false
                error => console.log(error)
            });



    }















}