import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
 import { ModuleWithProviders } from '@angular/core';
 import { routing } from './plan.routing';
 import { ModalModule } from 'angular2-modal';


//============ importing Ng2PaginationModule,AgmCoreModule,MyDatePickerModule  ================
import {Ng2PaginationModule} from 'ng2-pagination'; //importing ng2-pagination
//============ importing UsersComponent  ================
import { PlanComponent } from './components/plan.component' ;
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    routing,
    Ng2PaginationModule,

  ],
  declarations: [
    PlanComponent,
   ]
})
 export class PlanModule { }
 
                                     