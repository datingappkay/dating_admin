import {
    Component,
    ViewEncapsulation,
    Input,
    ViewChild,
    Inject,
    OnInit,
    ElementRef,
    NgZone,
    ChangeDetectorRef
} from '@angular/core';
import {
    BrowserModule
} from '@angular/platform-browser';
import {
    AppConfig
} from "../../app.config";
import {
    Router
} from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';

//importing service
import {
    PlanService
} from '../plan.service';

import {
    AppState
} from "../../app.state";


declare var swal: any;
declare var sweetAlert: any;

declare var $: any;
declare var google: any;
declare var jQuery: any;


@Component({
    selector: 'plan',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./plan.component.scss'],
    templateUrl: './plan.component.html',
    providers: [PlanService]
})

export class PlanComponent implements OnInit {


    public deletePlanData: any[];


    checked: boolean = false;
    disabled = false;
    status: any = [];
    passport: any = [];
    recentVisitors: any = [];
    whoSuperLikeMe: any = [];
    whoLikeMe: any = [];
    noAdds: any = [];
    readreceipt: any = [];
    hideDistance: any = [];
    hideAge: any = [];
    public data: any[];
    public rowsOnPage = 10;
    public p = 1;
    public forEdit: any
    public editPlanData = {};
    public planData: any = [];
    public addplan: FormGroup;
    public editPlan: FormGroup;
    oldpaln: any;
    oldcurrencySymbol: any;
    oldcost: any;
    oldcurrencyCode: any;
    olddurationInDays: any;
    oldlikeCount: any;
    oldrewindCount: any;
    oldstatusCode: any;
    oldsuperlikeCount: any;
    planAddId: any
    oldhideDistance: any;
    oldhideAge: any;
    olddescription: any;
    public users: any[];
    public deleteUser = [];
    public id: any;
    userDetails = {
        Name: "",
        durationInDays: "",
        durationInMonths: "",
        likeCount: "",
        superLikeCount: "",
        currencySymbol: "",
        currencyCode: "",
        Description: "",
        Cost: "",
        actualId: "",
        rewindCount: "",
        passport: "",
        recentVisitors: "",
        whoSuperLikeMe: "",
        whoLikeMe: "",
        noAdds: "",

        readreceipt: "",
        hideDistance: "",
        hideAge: "",
        StatusCode: "",
        Date: "",
        Status: "",

        // OtherImages: [],
        // Preferences: [

        // ]
    }



    constructor(@Inject(ElementRef) elementRef: ElementRef, private route: ActivatedRoute, fb: FormBuilder, public cdr: ChangeDetectorRef, private _planService: PlanService,
        private _state: AppState, private router: Router,
        private _zone: NgZone) {





        this.addplan = fb.group({
            'planName': ['', Validators.required],
            'durationInDays': ['', Validators.required],
            'durationInMonths': ['', Validators.required],
            'likeCount': ['', Validators.required],
            // 'superLikeCount': ['', Validators.required],
            'hideDistance': ['', Validators.required],
            'hideAge': ['', Validators.required],
            'cost': ['', Validators.required],
            'currencySymbol': ['', Validators.required],
            'currencyCode': ['', Validators.required],
            'status': ['', Validators.required],
            'statusCode': ['', Validators.required],
            'passport': ['', Validators.required],
            'recentVisitors': ['', Validators.required],
            'whoSuperLikeMe': ['', Validators.required],
            'whoLikeMe': ['', Validators.required],
            'noAdds': ['', Validators.required],
            'readreceipt': ['', Validators.required],
            'rewindCount': ['', Validators.required],
            'description': ['', Validators.required],
            'actualId': ['', Validators.required],
            'actualIdiOS': ['', Validators.required],


        });
        this.editPlan = fb.group({
            'newplanName': ['', Validators.required],
            'newdurationInDays': ['', Validators.required],
            'newdurationInMonths': ['', Validators.required],
            'newlikeCount': ['', Validators.required],
            // 'superLikeCount': ['', Validators.required],
            'newhideDistance': ['', Validators.required],
            'newhideAge': ['', Validators.required],
            'newcost': ['', Validators.required],
            'newcurrencySymbol': ['', Validators.required],
            'newcurrencyCode': ['', Validators.required],
            'newstatus': ['', Validators.required],
            'newstatusCode': ['', Validators.required],
            'newpassport': ['', Validators.required],
            'newrecentVisitors': ['', Validators.required],
            'newwhoSuperLikeMe': ['', Validators.required],
            'newwhoLikeMe': ['', Validators.required],
            'newnoAdds': ['', Validators.required],
            'newreadreceipt': ['', Validators.required],
            'newrewindCount': ['', Validators.required],
            'newdescription': ['', Validators.required],
            'newactualId': ['', Validators.required],
            'newactualIdiOS': ['', Validators.required],


        });





    }



    ngOnInit() {
        this.getPlanInfo();
        this.getDeletePlanInfo();
        this.getPage(this.p)
        this.getDeletePlanPages(this.p)
    }
    getPlanInfoById() {
        this.route.params.subscribe(params => {
            let _id = params['id'];
            console.log("id - ", _id)

            this._planService
                .getplansById(_id)
                .subscribe((data) => {
                    console.log("getPlansById data", data);
                    if (data.data) {
                        this.planData = data.data;
                    }
                    error => console.log(error)
                });
        });



    }
    getPage(p) {
        this._planService
            .getPlanPage(p, this.rowsOnPage).subscribe((data) => {
                {

                    this.p = p;

                }
            });
    }
    getDeletePlanPages(p) {
        this._planService
            .getDeletePlanPage(p, this.rowsOnPage).subscribe((data) => {
                {

                    this.p = p;

                }
            });
    }
    getPlanInfo() {
        this._planService.getplans().subscribe(
            result => {
                //   if (result.data && result.data.length > 0) {


                this.planData = result.data;
                console.log("dsdADGETTTTTTTTTTTTT==============>", this.planData)
                //  }else{
                //      console.error();


                //  }


            })
    }
    getDeletePlanInfo() {
        this._planService.getDeleteplans().subscribe(result => {
            this.deletePlanData = result.data;
            console.log("delete plan Info", this.deletePlanData)
        })
    }

    onChange(value) {


        if (value.checked === false) {

            this.checked = false;

            console.log("111111111111111111", 0);
        }
        if (value.checked === true) {

            this.checked = true;
            console.log("00000000000000000000000", 1);
        }



    }


    submitForm(value) {
        value._value.passport = jQuery('#togBtn1').is(':checked')
        value._value.passport = jQuery('#togBtn1').is(':checked')
        value._value.recentVisitors = $('#togBtn2').is(':checked')
        value._value.whoSuperLikeMe = $('#togBtn3').is(':checked')
        value._value.whoLikeMe = $('#togBtn4').is(':checked')
        value._value.noAdds = $('#togBtn5').is(':checked')
        value._value.readreceipt = $('#togBtn6').is(':checked')
        value._value.hideDistance = $('#togBtn7').is(':checked')
        value._value.hideAge = $('#togBtn8').is(':checked')

      //  console.log("val", value._value)
        this._planService.addplans(value._value).subscribe(
            result => {
                if (result.code !== 200) {
                    swal("Success!", "Plan Added Successfully!", "success");
                    jQuery('#addPlan').modal('hide');

                    this.ngOnInit();
                    this.getPlanInfo();

                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
                this.addplan.reset();
            }
        )
    }


    gotoSearchPlan(term) {
        this._planService.getUserBySearchtext(term).subscribe(
            (data) => {

                this.planData = data.data;

                console.log("yyyyyyyyyyy", data.data);
            });
    }
    gotoSearchSeletePlan(term) {
        this._planService.getUserBySearchtext(term).subscribe(
            (data) => {

                this.planData = data.data;

                console.log("yyyyyyyyyyy", data.data);
            });
    }






    selectAllCheckBox() {
        this.deleteUser = [];
        this.planData.forEach((obj) => {
            obj.checked = !obj.checked;
            if (obj.checked) {
                this.deleteUser.push(obj._id)

            }

            // this.gotocheck(obj.name, event);
        });
        console.log("===>>", this.deleteUser)

    }

    DeleteselectAllCheckBox(
    ) {
        this.users = [];
        this.deletePlanData.forEach((obj) => {
            obj.checked = !obj.checked;
            if (obj.checked) {
                this.users.push(obj._id)

            }

            // this.gotocheck(obj.name, event);
        });
        console.log("===>>", this.users)

    }

    gotocheck(id) {
        console.log("idddddddddssssssssss", id);
        this.forEdit = id

        var index = this.deleteUser.indexOf(id);
        if (index === -1) {
            this.deleteUser.push(id);

        }
        else {
            this.deleteUser.splice(index, 1);

        }
        console.log("selected:-", this.deleteUser);
    }



    gotoDeletecheck(id) {
        console.log("idddddddddssssssssss", id);


        var index = this.users.indexOf(id);
        if (index === -1) {
            this.users.push(id);

        }
        else {
            this.users.splice(index, 1);

        }
        console.log("selected:-", this.users);

    }



    DeletePlan() {

        //console.log("idddddddd", this.deleteUser);
        if (this.deleteUser.length == 0) {
            swal("Please Select Plan");
        } else {



            var index = -1;
            this._planService
                .deleteplans(this.deleteUser).subscribe((data) => {
                    this._planService.getplans().subscribe(
                        (data) => {
                            if (data.code !== 200) {

                                this.planData = data.data;

                                this.ngOnInit();
                                swal("Success!", "Plan Deleted!", "success");
                                this.deleteUser.splice(index, 1)
                                this.getPlanInfo();

                            }
                        })

                });
        }


    }

    userDetailsModal(user) {

        this._planService
            .getplansById(user._id)
            .subscribe((user) => {

                if (user.data[0]) {
                    user = user.data[0];
                    console.log("user ", user);
                    //  this.userDetails.OtherPhotos.length = 0;
                    // this.userDetails.OtherPhotos = [];
                    // var years = moment().diff(user.dob, 'years');
                    this.userDetails.Name = user.planName || "--";
                    this.userDetails.durationInDays = user.durationInDays || "--";
                    this.userDetails.durationInMonths = user.durationInMonths || "--";
                    this.userDetails.likeCount = user.likeCount;
                    this.userDetails.superLikeCount = user.superLikeCount;
                    this.userDetails.currencySymbol = user.currencySymbol || "--";
                    this.userDetails.currencyCode = user.currencyCode || 0;
                    this.userDetails.Description = user.description || "--";
                    this.userDetails.rewindCount = user.rewindCount || "--";
                    this.userDetails.Cost = user.cost || "--";

                    this.userDetails.passport = user.passport;
                    this.userDetails.recentVisitors = user.recentVisitors;
                    this.userDetails.whoSuperLikeMe = user.whoSuperLikeMe;
                    this.userDetails.whoLikeMe = user.whoLikeMe;
                    this.userDetails.noAdds = user.noAdds;



                    this.userDetails.actualId = user.actualId;

                    this.userDetails.readreceipt = user.readreceipt;
                    this.userDetails.hideDistance = user.hideDistance;
                    this.userDetails.hideAge = user.hideAge;
                    this.userDetails.Status = user.status || "--";
                    this.userDetails.StatusCode = user.statusCode || "--";
                    this.userDetails.Date = user.timestamp || "--";



                    $("#userDetails").modal("show");
                }
                error => console.log(error)
            });

    }



    editplan(id) {
        this.planAddId = id
        if (!id) {

            swal("Please Select Plan");
        } else {
            jQuery('#editPlan').modal('show');

            this._planService
                .getplansById([id])
                .subscribe((user) => {

                    this.editPlanData = user.data[0];

                })
        }
    }
    submiteditForm(editplandata) {
        editplandata._value.newpassport = jQuery('#togBtn1').is(':checked')
        editplandata._value.newrecentVisitors = $('#togBtn2').is(':checked')
        editplandata._value.newwhoSuperLikeMe = $('#togBtn3').is(':checked')
        editplandata._value.newwhoLikeMe = $('#togBtn4').is(':checked')
        editplandata._value.newnoAdds = $('#togBtn5').is(':checked')
        editplandata._value.newreadreceipt = $('#togBtn6').is(':checked')
        editplandata._value.newhideDistance = $('#togBtn7').is(':checked')
        editplandata._value.newhideAge = $('#togBtn8').is(':checked')
        editplandata._value._id = this.planAddId

      
       // console.log("======this.planAddId====", editplandata._value)
        this._planService.editPlan(editplandata._value).subscribe(
            result => {
                if (result.code !== 200) {
                    swal("Success!", "Plan Edited Modified!", "success");
                    jQuery('#editPlan').modal('hide');

                    this.ngOnInit();
                    this.getPlanInfo();

                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
                this.addplan.reset();
            }
        )
    }





}






