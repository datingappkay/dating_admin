import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';

declare var localStorage


@Injectable()
export class Configuration {
    Server: string = "https://admin-api.helloperfect.co/";

    authToken = localStorage.getItem('adminToken');
    headers = new Headers({ 'Content-Type': 'application/json' });
    constructor() {
        this.headers.append('authorization', localStorage.getItem('adminToken'));
        this.headers.append('lang', 'en');
    }
    setToken() {
        this.headers.set('authorization', localStorage.getItem('adminToken'));
        this.headers.set('lang', 'en');
    }
}