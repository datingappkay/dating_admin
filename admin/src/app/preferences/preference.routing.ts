//importing all the essential modules
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//importing all the custom components
//============ importing Report template generator Component ================
import { PreferenceGeneratorComponent } from './components/preferenceComponent/preference.component';
 
export const prefroutes: Routes = [ 
   {
       path: '',
       component: PreferenceGeneratorComponent,
       
      },
 
];
export const routing: ModuleWithProviders = RouterModule.forChild(prefroutes); // not required


