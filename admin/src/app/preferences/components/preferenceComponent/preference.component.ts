import { Component, ViewEncapsulation, ViewContainerRef, AfterContentInit, OnInit } from '@angular/core';
import { AppConfig } from "../../../app.config";
import { Router } from '@angular/router';

//=================== importing form components ==================
import { FormGroup, FormBuilder, Validators, AbstractControl, FormArray, FormControl } from '@angular/forms';
//=================== importing PreferenceService   =============
import { PreferenceService } from '../../preference.service';
declare var swal: any;
declare var $: any;
declare var sweetAlert: any;


@Component({
    selector: 'preference',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./preference.component.scss'],
    templateUrl: './preference.component.html',
    providers: [PreferenceService]
})

export class PreferenceGeneratorComponent implements OnInit {
    public rowsOnPage = 10;
    msg = true;
    filter: FormGroup;
    editFilter: FormGroup;
    isMandatory = true;
    type = 1;
    Priority: any;
    data = [];
    langData = [];
    obj = '';
    filterId: any;
    isMan:any;
    valCount = false;
    btnFlag = true;
    public config: any;
    public configFn: any;

    constructor(private _service: PreferenceService, private _appConfig: AppConfig, private router: Router, vcRef: ViewContainerRef, private fb: FormBuilder) {
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
        this.filter = fb.group({
            'type': ['', Validators.required],
            'unit': [''],
            'titileLable': ['', Validators.required],
            'values': ['1'],
            'valuesOther': ['1'],
            'titleNameOther': ['', Validators.required],
            'isMandatory': 'true',
        });
        this.editFilter = fb.group({
          
            'newtype': [''],
            'newunit': [''],
            'newtitileLable': [''],
            'newvaluesOther': ['1'],
            'newvalues': ['1'],
            'newisMandatory': '',
        });
    }

    ngOnInit() {
        jQuery("#addDiv").hide();
        this.getprefrence()
        this._service.getLanguage().subscribe(
            res => {
             //   console.log("res.data", res.result)
                if (res.result && res.result.length > 0) {
                    this.langData = res.result;
                } else {
                    this.langData = [];
                }
            }
        )
    }
    refresh(){
        this.getprefrence()
    }

    getprefrence() {
        this._service.getData().subscribe(
            res => {
             //   console.log("=============>getVices", res.data.Mypreference[0])
                if (res.data.Mypreference[0]) {
                    this.data = res.data.Mypreference[0];
                    this.msg = false;
                } else {
                    this.msg = true;
                    this.data = [];
                }
            }
        )
    }

    checkValues(s) {
       // console.log("===========>",s)
        if (s == 1) {
            jQuery("#addunit").hide();
            jQuery("#editunit").hide();
            jQuery("#addDiv").show();
            jQuery("#editDiv").show();
        }
        else if (s == 12) {
            jQuery("#addDiv").hide();
            jQuery("#editDiv").hide();

        } else {
            jQuery("#addDiv").show();
            jQuery("#editDiv").show();
            jQuery("#addunit").show();
            jQuery("#editunit").show();

        }
    }

    submitForm(val) {
        var otherName = [];

        this.langData.forEach(e => {
            let x = {
                langId: e.langId,
                langCode: e.code,
                addtitle: jQuery(".addtitle" + e.langId).val(),
                values: jQuery(".addValues" + e.langId).val()
            }
            otherName.push(x);
        });
        val._value.otherName = otherName;
       // console.log("=============", val._value)
        this._service.addFilter(val._value).subscribe(
            res => {
                //     console.log("==========>res", res)
                if (res) {
                    swal("Success!", "Added Successfully!", "success");
                    jQuery('#addFilter').modal('hide');
                    this.getprefrence()
                    this.filter.reset();
                    this.type = 1;
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        )
    }

    gotoEditData(data) {
       //    console.log("======>", data)
           this.editFilter.controls['newunit'].setValue(data.forAdminUnit);
        this.editFilter.controls['newtype'].setValue(data.TypeOfPreference);
        jQuery(".eField").val('');
        jQuery(".eValues").val('');
        this.Priority = data.Priority
        this.type = data.TypeOfPreference;
        this.obj = data;
        this.isMan= data.mandatory
        var lables = data.otherLabel;
        var value = data.otherOptionsValue
        var otherTitle = data.otherPreferenceTitle
        this.filterId = data._id;
        this.langData.forEach(ele => {
            for (var langCode in lables) {
                if (ele.code == langCode) {
                    jQuery(".editField" + ele.langId).val(lables[langCode]);
                }
            }
            for (var val in value) {
                if (ele.code == val) {
                    jQuery(".editValues" + ele.langId).val(value[val]);
                }
            }
            for (var ttl in otherTitle) {
                if (ele.code == ttl) {
                    jQuery(".editTitle" + ele.langId).val(otherTitle[ttl]);
                }
            }

        });
        if (this.type == 12) {
            jQuery("#editDiv").hide();
        } else {
            jQuery("#editDiv").show();
        }
    }

    editFilterFunc(val) {
        var editOtherName = [];
        
        this.langData.forEach(e => {
            let x = {
                langId: e.langId,
                langCode: e.code,
                values: jQuery(".editValues" + e.langId).val(),
                editTitle: jQuery(".editTitle" + e.langId).val()

            }
            editOtherName.push(x);
            //console.log("=============", editOtherName)
        });
        val._value.otherName = editOtherName;
        val._value.filterId = this.filterId;
        val._value.Priority = this.Priority
        val._value.newisMandatory =(val._value.newisMandatory)?val._value.newisMandatory:this.isMan

       //  console.log("valediteeeeeeeee.", val._value);
        this._service.editFilter(val._value).subscribe(
            res => {
                //console.log("==========>res", res)
                if (res) {

                    swal("Success!", "Edited Successfully!", "success");
                    jQuery('#editFilter').modal('hide');
                    this.editFilter.reset();
                    this.getprefrence()
                    this.type = 1;
                } else {
                    sweetAlert("Oops...", res.message, "error");
                }
            }
        )






    }

    gotoDeleteFilter(deleteid) {
        var id = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Search Preferences!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    id.deleteFilter(deleteid);
                    swal({
                        title: 'Delete!',
                        text: 'Search Preferences Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your Search Preferences is safe :)", "error");
                }
            });
    }
    deleteFilter(deleteid) {
        var deleteData = deleteid
        // console.log("filterId", deleteid);
        this._service.deleteFilter(deleteid).subscribe(
            res => {
                this.getprefrence()

            }
        )
    }
    gotoUpArrow(index) {
        // console.log("up arrow", index);
        if (index > 0) {
            const tmp = this.data[index - 1];
            // console.log("upId", this.category[index - 1].catId)
            // console.log("currentId", this.category[index].catId)
            this.data[index - 1] = this.data[index];
            this.data[index] = tmp;
            this._service.gotoRowsUpate(this.data[index].Priority, this.data[index - 1].Priority, 1).subscribe(
                res => {
                    // console.log("res", res);
                    this.ngOnInit();
                }
            )
        }
        // console.log(this.category)
    }
    gotoDownArrow(index) {
        if (index < this.data.length) {
            const tmp = this.data[index + 1];
            this.data[index + 1] = this.data[index];
            this.data[index] = tmp;
            // console.log("downId", this.category[index + 1].catId, this.category[index + 1].name)
            // console.log("currentId", this.category[index].catId, this.category[index].name)
            this._service.gotoRowsUpate(this.data[index].Priority, this.data[index + 1].Priority, 1).subscribe(
                res => {
                    // console.log("res", res);
                    this.ngOnInit();
                }
            )
        }
    }
}



