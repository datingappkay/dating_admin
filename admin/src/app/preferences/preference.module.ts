//================= importing all the required modules ==============
import { NgModule, ModuleWithProviders } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


//============ importing the router module ==============
import { RouterModule } from '@angular/router';
//============ importing Ng2PaginationModule, Generator Component ================
import {
  Ng2PaginationModule, PaginationService, PaginationControlsDirective,
  PaginationControlsComponent, PaginatePipe
} from 'ng2-pagination';

//============ importing Preference Generator Component ================
import { PreferenceGeneratorComponent } from './components/preferenceComponent/preference.component';

//============ importing AgmCoreModule, TypeaheadModule, TypeaheadModule ================
import { AgmCoreModule } from "angular2-google-maps/core";
// import {SelectModule} from 'angular2-select';
import { TypeaheadModule } from 'ng2-bootstrap/typeahead';



@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    Ng2PaginationModule,
    // SelectModule,
    TypeaheadModule
  ],
  declarations: [
    PreferenceGeneratorComponent,
  ],

  providers: [PaginationService],
  exports: [
  ]


})
//============= exporting customer module ==========================
export class PreferenceModule {

}