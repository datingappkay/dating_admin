import { Routes, RouterModule } from '@angular/router';

import { TermsandconditionsComponent } from './termsandconditions/termsandconditions.component';

import { ModuleWithProviders } from '@angular/core';

export const termsandconditionsroutes: Routes = [
  {
    path: '', component: TermsandconditionsComponent, data: { title: '' },
   
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(termsandconditionsroutes);