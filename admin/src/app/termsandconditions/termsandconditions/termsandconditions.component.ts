import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit, ViewChild } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';
import { TermsandConditionsService } from '../termsandconditions.service';

import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { ImageCropperComponent, CropperSettings, Bounds } from 'ng2-img-cropper';
declare var swal: any;
declare var sweetAlert: any;
@Component({
    selector: 'termsandconditions',
    encapsulation: ViewEncapsulation.None,
    // styleUrls: ['./policy.component.scss'],
    templateUrl: './termsandconditions.component.html',
    providers: [TermsandConditionsService]
})


export class TermsandconditionsComponent {
    ckeditorContent: any;
    pageForm: FormGroup;
    fileName: any;
    type = 1;
    data1: any;
    dataURI: any;
    public_id: any;
    cropperSettings: CropperSettings;
    croppedWidth: number;
    croppedHeight: number;
    cloudinaryImage: any;
    private answer: any = '';
    imgagefile: any;
    adminURL = 'https://config.helloperfect.co/TermsAndConditions.html';
    private images: any = [];
    private uploaded: boolean = false;
    public config = {
        uiColor: '#F0F3F4',
        height: '600',
    };

    @ViewChild('cropper', undefined) cropper: ImageCropperComponent;
    imageSrc: any;
    constructor(private _appConfig: AppConfig, private router: Router,
        vcRef: ViewContainerRef, fb: FormBuilder, private _TermsandConditionsService: TermsandConditionsService) {
        this.pageForm = fb.group({
            'fileName': "",
        });
        this.cropperSettings = new CropperSettings();
        this.cropperSettings.noFileInput = true;
        this.data1 = {};
        this.ckeditorContent = `TermsAndConditions....`;

        this._TermsandConditionsService.getTermsAndConditionsFile().subscribe(result => {
            console.log(" TermsAndConditions result is : ", result.data)
            if (result.data) {
                this.ckeditorContent = result.data[0].configData
            }

        });

    }
    backToPrivicy() {
        this.router.navigate(['/pages/users']);
    }
    ngOnInit() {
        // this._adminconfigservice.getFileContent().subscribe(
        //     result => {
        //         this.ckeditorContent = result.data[0].configData;
        //         // console.log("result", this.ckeditorContent);
        //     }
        // )
        // this._adminconfigservice.getImages().subscribe(
        //     result => {
        //         this.imageSrc = result.data;
        //         // console.log("result", this.imageSrc);
        //     }
        // )
        // this._adminconfigservice.getPageURL().subscribe(
        //     result => {
        //         this.adminURL = result.data;
        //         // console.log("result", this.adminURL);
        //     }
        // )
    }


    // cropped(bounds: Bounds) {
    //     this.croppedHeight = bounds.bottom - bounds.top;
    //     this.croppedWidth = bounds.right - bounds.left;
    //     // console.log("height", this.croppedHeight);
    //     // console.log("width", this.croppedWidth);
    // }
    // addImage(img) {
    //     // console.log("answer",this.ckeditorContent);
    //     this.ckeditorContent = this.ckeditorContent + "<img src=" + img + ">"
    //     // console.log("image",this.ckeditorContent)
    // }
    // fileChangeListener($event) {
    //     var image: any = new Image();
    //     var file: File = $event.target.files[0];
    //     var myReader: FileReader = new FileReader();
    //     var that = this;
    //     myReader.onloadend = function (loadEvent: any) {

    //         image.src = loadEvent.target.result;
    //         that.cropper.setImage(image);
    //     };
    //     myReader.readAsDataURL(file);
    // }
    // confirmUpload() {
    //     let dataURI = this.data1.image;
    //     // let imgres = dataURI.split(',')[1];
    //     // console.log("immmm",imgres);
    //     // console.log("img", dataURI);
    //     var blob = new Blob([dataURI], { type: 'image/png' });
    //     var fileName = (Math.random().toString(36).substring(7) + ".png");
    //     this.imgagefile = new File([blob], fileName)
    //     // console.log("imgurl", this.imgagefile);

    //     this._adminconfigservice.gotoimageUpload(dataURI).subscribe(
    //         result => {
    //             // console.log("result", result);
    //             this.ngOnInit();
    //         }
    //     )
    // }
    gotoSaveFile() {
        console.log("source", this.ckeditorContent);

        this._TermsandConditionsService.updateTermsAndConditionsFile({ data: this.ckeditorContent }).subscribe(result => {
            this.ngOnInit();
            if (result.code != 200) {
                swal("Success!", "File Saved Successfully!", "success");
            } else {
                sweetAlert("Oops...", "Something went wrong!", "error");
            }
        });
    }

}