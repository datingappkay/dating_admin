import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { contentHeaders } from '../loginComponent/headers';//Importing headers from header file
import { Configuration } from '../app.constant';
declare var API_URL: string;
@Injectable()
export class TermsandConditionsService {

    constructor(private http: Http, public _config: Configuration) {
    }



    updateTermsAndConditionsFile(data) {
        let body = {
            configType: 1,
            configData: data,
            title:"Terms And Conditions"

        }
        console.log("termsssssssbody", body);

        let url = this._config.Server + 'termsAndConditions';
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }

    getTermsAndConditionsFile() {
        //  let url = this._config.Server + 'termsAndConditions';
        let url = this._config.Server + "termsAndConditions?configType=1";

        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

}


// this could also be a private method of the component class
function handleError(error: any) {
    // log error
    // could be something more sofisticated
    let errorMsg = error.message || `Yikes! There was was a problem with our hyperdrive device and we couldn't retrieve your data!`
    console.error(errorMsg);

    // throw an application level error
    return Observable.throw(errorMsg);
}
