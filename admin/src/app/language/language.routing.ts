//importing all the essential modules
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//importing all the custom components
//============ importing Matches Componentt ================
import { languageComponent } from './components/language.component';
 
 

export const languageRoutes: Routes = [ 
         {
       path: '',
       component: languageComponent,
         data:{
         title: ''
       }
      }
 
];
export const routing: ModuleWithProviders = RouterModule.forChild(languageRoutes); // not required


