import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../app.constant';

@Injectable()
export class languageService {

    constructor(private http: Http, public _config: Configuration) {
    }


    addLanguge(data) {
        let url = this._config.Server + "language";
        let body = { languageName: data.langeugeName, langeugeCode: data.langeugeCode, isMandatory: data.isMandatory };
        console.log("============>body", body)
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }

    getLanguges(status) {
        let Url = this._config.Server + "language/" + status;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    deleteLanguge(_id) {
        let Url = this._config.Server + "language/" + _id;
        return this.http.delete(Url, { headers: this._config.headers })
            .map(res => res.json());
    }

    addReason(data) {
        let url = this._config.Server + "reportReasons";
        return this.http.post(url, data, { headers: this._config.headers }).map(res => res.json());
    }
    editLanguage(data, id) {
        let url = this._config.Server + "language";
        let body = { languageName: data.newlangeugeName, status: data.status, langeugeCode: data.newlangeugeCode, isRTL: data.newisMandatory, langId: id };
        console.log("============>body", body)
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    getReasons() {
        let Url = this._config.Server + "reportReasons";
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }

    getReportReasons(_id) {
        let Url = this._config.Server + "reportReasons/" + _id;
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    deleteUser(UserId,status) {
        let url = this._config.Server + "language?status=" + status + "&id=" + UserId
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }

}
