import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, OnInit } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';

//importing service

//=================== importing form components ==================
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


//=================== importing Matches Service   =============
import { languageService } from '../language.service';
import { UsersService } from '../../users/users.service';
import { ProfilesService } from '../../profiles/profile.service';
declare var swal: any;
declare var $: any;


@Component({
    selector: 'language',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./language.component.scss'],
    templateUrl: './language.component.html',
    providers: [languageService, UsersService, ProfilesService]
})

export class languageComponent implements OnInit, AfterViewInit {
    deleteUserId: any;
    oldlanguageName: any;
    oldlanguageCode: any
    oldRtl: any;
    langId: any;
    isMandatory = true;
    status: any;

    addLanguage: FormGroup;
    editLanguage: FormGroup;

    loader = true;
    data: any = [];

    public chatDetails: any;
    public config: any;
    public configFn: any;

    constructor(private _Service: languageService, private _appConfig: AppConfig, private router: Router, vcRef: ViewContainerRef, private fb: FormBuilder) {
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
        this.addLanguage = fb.group({
            'langeugeName': ['', Validators.required],
            'langeugeCode': ['', Validators.required],
            'isMandatory': '',

        });
        this.editLanguage = fb.group({
            'newlangeugeName': ['', Validators.required],
            'newlangeugeCode': ['', Validators.required],
            'newisMandatory': '',
        });


    }

    ngOnInit() {
        this.activelanguage(1)

    }

    activelanguage(status) {
        this._Service.getLanguges(status)
            .subscribe((dataA) => {
                //console.log("resssssssssssss", dataA)
                this.data = dataA.result;
            });
    }
    deActivelanguage(status) {
        this._Service.getLanguges(status)
            .subscribe((dataA) => {
                // console.log("resssssssssssss", dataA)
                this.data = dataA.result;
            });
    }
    AddLanguage() {
        $("#addLanguage").modal("show");
    }
    submitForm(value) {

        this._Service.addLanguge(value._value).subscribe(
            (result) => {
                swal("Success!", "Language Added Successfully!", "success");
                this.ngOnInit();
                jQuery('#addLanguage').modal('hide');
                this.addLanguage.reset();
            })
    }


    showModalDelete(_id, status) {
        this.deleteUserId = _id;
        var post = this;
        this.status = status
        console.log("------------>", status)
        // $("#DeleteUser").modal("show");
        if (status == 0) {
            swal({
                title: "Are you sure want to Deactivate This language?",
                text: "You will be able to recover this Language!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    // this.isConfirm = isConfirm
                    // console.log("====this.isConfirm====", this.isConfirm)
                    if (isConfirm) {
                        post.deletepost(_id);
                        swal({
                            title: 'Delete!',
                            text: 'Language Deleted Successfully!',
                            type: 'success'
                        });

                    } else {
                        swal("Cancelled", "Your Language is safe :)", "error");
                    }
                });
        }
        else if (status == 1) {

            swal("Success!", "Language Recover Successfully!", "success");
            post.deletepost(_id);
        }
        else {
            swal({
                title: "Are you sure want to permanently Delete This language?",
                text: "You will be not able to recover this Language!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    // this.isConfirm = isConfirm
                    // console.log("====this.isConfirm====", this.isConfirm)
                    if (isConfirm) {
                        post.deletepost(_id);
                        swal({
                            title: 'Delete!',
                            text: 'Language Deleted Successfully!',
                            type: 'success'
                        });

                    } else {
                        swal("Cancelled", "Your Language is safe :)", "error");
                    }
                });
        }
    }

    deletepost(_id) {
        this._Service
            .deleteUser(this.deleteUserId, this.status)
            .subscribe((result) => {
                if (result.code == 200) {
                    console.log("data deleted");
                } else {
                    console.log("data NOT deleted");
                }
                jQuery('#DeleteUser').modal('hide');
                if (this.status == 0) {
                    this.ngOnInit();
                } else {
                    this.deActivelanguage(0)
                }

                console.log("data statusssssssssssss", this.status);

            })
    }

    editLanguages(language) {
        this.oldlanguageName = language.name;
        this.oldlanguageCode = language.code;
        this.oldRtl = language.isRtl;
        this.langId = language._id,
            this.status = language.status

    }

    updateLanguage(language) {
        language._value.status = this.status
        this._Service.editLanguage(language._value, this.langId).subscribe(
            result => {
                swal("Success!", "Language Edited Successfully!", "success");

                this.ngOnInit()
                jQuery('#editLanguage').modal('hide');

            }
        )
    }

}
