import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from './app.constant';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import { Body } from '@angular/http/src/body';
//import {HttpClient, HttpParams, HttpRequest, HttpEvent, Http, Response, Headers, RequestOptions, RequestMethod} from '@angular/common/http';
declare var API_URL: string;
@Injectable()
export class AppService {
    constructor(private http: Http, public _config: Configuration) {
    }
    tokenVerification() {
        let Url = this._config.Server + "tokenVerification";
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    changePassword(val) {
        let Url = this._config.Server + "password";
        let body = {
            currentPassword:val.oldPassword,
            newPassword:val.password,
            confirmPassword:val.confirmPassword
        }
        return this.http.put(Url, body, { headers: this._config.headers })
            .map(res => res.json());
    }
}



