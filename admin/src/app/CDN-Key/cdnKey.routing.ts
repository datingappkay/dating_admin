//importing all the essential modules
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//importing all the custom components
//============ importing Matches Componentt ================
import { cdnKeyComponent } from './components/cdnKey.component';
 
 

export const cdnKeyRoutes: Routes = [ 
         {
       path: '',
       component: cdnKeyComponent,
         data:{
         title: ''
       }
      }
 
];
export const routing: ModuleWithProviders = RouterModule.forChild(cdnKeyRoutes); // not required


