import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../app.constant';

@Injectable()
export class cdnKeyService {

    constructor(private http: Http, public _config: Configuration) {
    }


    addLanguge(data) {
        let url = this._config.Server + "cdnKeys";
        let body = JSON.stringify(data);
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }

    getLanguges() {
        let Url = this._config.Server + "cdnKeys";
        return this.http.get(Url, { headers: this._config.headers })
            .map(res => res.json());
    }
    deleteLanguge(_id) {
        let Url = this._config.Server + "language/" + _id;
        return this.http.delete(Url, { headers: this._config.headers })
            .map(res => res.json());
    }


    editLanguage(data) {
        let url = this._config.Server + "cdnKeys";
        let body = JSON.stringify(data)
        //console.log("============>",body)
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }


    deleteUser(UserId) {
        let url = this._config.Server + "cdnKeys?&id=" + UserId
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }

}
