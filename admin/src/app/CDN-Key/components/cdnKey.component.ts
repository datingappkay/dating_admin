import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, OnInit } from '@angular/core';
import { AppConfig } from "../../app.config";
import { Router } from '@angular/router';

//importing service

//=================== importing form components ==================
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


//=================== importing Matches Service   =============
import { cdnKeyService } from '../cdnKey.service';
import { UsersService } from '../../users/users.service';
import { ProfilesService } from '../../profiles/profile.service';
declare var swal: any;
declare var $: any;


@Component({
    selector: 'cdnKey',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./cdnKey.component.scss'],
    templateUrl: './cdnKey.component.html',
    providers: [cdnKeyService, UsersService, ProfilesService]
})

export class cdnKeyComponent implements OnInit, AfterViewInit {
    id: any;
    isMandatory = true;
    status: any;
    obj = [];
    addLanguage: FormGroup;
    editCDN: FormGroup;
    loader = true;
    data: any = [];

    public chatDetails: any;
    public config: any;
    public configFn: any;

    constructor(private _Service: cdnKeyService, private _appConfig: AppConfig, private router: Router, vcRef: ViewContainerRef, private fb: FormBuilder) {
        this.config = this._appConfig.config;
        this.configFn = this._appConfig;
        this.addLanguage = fb.group({
            'cloudName': ['', Validators.required],
            'apiKey': ['', Validators.required],
            'apiSecret': ['', Validators.required],
            'uploadPreset': ['', Validators.required],


        });
        this.editCDN = fb.group({
            'newcloudName': [''],
            'newapiKey': [''],
            'newapiSecret': [''],
            'newuploadPreset': [''],
        });


    }

    ngOnInit() {
        this.activelanguage()

    }

    activelanguage() {
        this._Service.getLanguges()
            .subscribe((dataA) => {
                console.log("resssssssssssss", dataA)
                this.data = dataA.data;
            });
    }

    AddLanguage() {
        $("#addLanguage").modal("show");
    }
    submitForm(value) {

        this._Service.addLanguge(value._value).subscribe(
            (result) => {
                swal("Success!", "Success!", "success");
                this.ngOnInit();
                jQuery('#addLanguage').modal('hide');
                this.addLanguage.reset();
            })
    }


    showModalDelete(_id) {
        this.id = _id;
        var post = this;
        swal({
            title: "Are you sure want to Deleted CDN Details?",
            text: "You will be able to recover CDN Details!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                // this.isConfirm = isConfirm
                // console.log("====this.isConfirm====", this.isConfirm)
                if (isConfirm) {
                    post.deletepost(_id);
                    swal({
                        title: 'Delete!',
                        text: 'CDN Details Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your CDN Details is safe :)", "error");
                }
            });

    }

    deletepost(_id) {
        this._Service
            .deleteUser(this.id)
            .subscribe((result) => {
                this.ngOnInit();
            })
    }

    editLanguages(dt) {
        this.obj = dt
        this.id = dt._id

    }

    updateLanguage(data) {
        data._value.id = this.id
        this._Service.editLanguage(data._value).subscribe(
            result => {
                swal("Success!", "Success!", "success");
                this.ngOnInit()
                jQuery('#editLanguage').modal('hide');

            }
        )
    }

}
