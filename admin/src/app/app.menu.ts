export const menuItems = [

  // {
  //   title: 'Dashboard',
  //   routerLink: 'userdashboard',
  //   icon: 'ion-android-home',
  //   selected: false,
  //   expanded: false,
  //   order: 99,
  //   subMenu: [
  //     {
  //       title: 'User Data',
  //       routerLink: 'userdashboard',
  //     },
  //     {
  //       title: 'User Activites',
  //       routerLink: 'userActivitesDashboard',
  //     },
  //     {
  //       title: 'Coin Data',
  //       routerLink: 'userCoinsDashboard',
  //     },
  //     {
  //       title: 'Device Analytics',
  //       routerLink: 'userDeviceDashboard',
  //     },
  //     {
  //       title: 'App Earnings',
  //       routerLink: 'appEarningsDashboard',
  //     },
  //     // {
  //     //   title: 'Ratings',
  //     //   routerLink: 'userMatchDashboard',
  //     // },
  //   ]
  // },
  {
    title: 'Users',
    routerLink: 'users',
    icon: 'ion-person-stalker',
    selected: false,
    expanded: false,
    order: 100
    // subMenu: [
    //   {
    //     title: 'Active Users',
    //     routerLink: 'users',
    //   },
    //   {
    //     title: 'Deactive Users',
    //     routerLink: 'deactiveUsers',
    //   },
    //     {
    //     title: 'Banned Users',
    //     routerLink: 'bannedUser',
    //   },
    //   {
    //     title: 'Reports',
    //     routerLink: 'reportedUser',
    //   },
    //   {
    //     title: 'Likes',
    //     routerLink: 'likedUserPage',
    //   },
    //   {
    //     title: 'Dis-Likes',
    //     routerLink: 'disLikedUserPage',
    //   },
    //   {
    //     title: 'Recent Visitors',
    //     routerLink: 'recentVisitorsPage',
    //   },
    //   {
    //     title: 'Matches',
    //     routerLink: 'matchPage',
    //   },
    //   {
    //     title: 'SuperLikes',
    //     routerLink: 'superLikesPage',
    //   },
    // ]
  },
   
  {
    title: 'User Activites',
    routerLink: 'UserLikes',
    icon: 'fa fa-user-secret',
    selected: false,
    expanded: false,
    order: 101,

  },
  {
    title: 'Matches',
    routerLink: 'MatchesUser',
    icon: 'ion-android-people',
    selected: false,
    expanded: false,
    order: 102
  },

  // {
  //   title: 'Ad Campaign',
  //   routerLink: 'compaign',
  //   icon: 'ion-speakerphone',
  //   selected: false,
  //   expanded: false,
  //   order: 103

  // },
  {
    title: 'In-App Purchase Plans',
    routerLink: 'plan',
    icon: 'fa fa-plus-circle',
    selected: false,
    expanded: false,
    order: 104

  },
  {
    title: 'Subscription Logs',
    routerLink: 'subscription',
    icon: 'fa fa-youtube-play',
    selected: false,
    expanded: false,
    order: 105

  },
  // {
  //   title: 'Coin',
  //   routerLink: 'coin',
  //   icon: 'fa fa-usd',
  //   selected: false,
  //   expanded: false,
  //   order: 106

  // },
  // {
  //   title: 'CoinConfiguration',
  //   routerLink: 'CoinWallet',
  //   icon: 'fa fa-get-pocket',
  //   selected: false,
  //   expanded: false,
  //   order: 107

  // },
  // {
  //   title: 'AppWallet',
  //   routerLink: 'AppWallet',
  //   icon: 'fa fa-google-wallet',
  //   selected: false,
  //   expanded: false,
  //   order: 108

  // },

  // {
  //   title: 'App Rating',
  //   routerLink: 'appRate',
  //   icon: 'ion-ios-star-half',
  //   selected: false,
  //   expanded: false,
  //   order: 109
  // },
  // {
  //   title: 'Dates',
  //   routerLink: 'activeDates',
  //   icon: 'fa fa-clock-o',
  //   selected: false,
  //   expanded: false,
  //   order: 110
  // },
  // {
  //   title: 'Report Reasons',
  //   routerLink: 'MatchesComponent',
  //   icon: 'ion-clipboard',
  //   selected: false,
  //   expanded: false,
  //   order: 111
  // },
  // {
  //   title: 'Preferences',
  //   routerLink: 'preferences',
  //   icon: 'ion-wrench',
  //   selected: false,
  //   expanded: false,
  //   order: 112
  // },
  {
    title: 'SMS Logs',
    routerLink: 'otpLogs',
    icon: 'fa fa-commenting',
    selected: false,
    expanded: false,
    order: 113
  },
  // {
  //   title: 'App-Version',
  //   routerLink: 'appversion',
  //   icon: 'fa fa-info-circle',
  //   selected: false,
  //   expanded: false,
  //   order: 114
  // },
  // {
  //   title: 'Languages',
  //   routerLink: 'languageComponent',
  //   icon: 'fa fa-language',
  //   selected: false,
  //   expanded: false,
  //   order: 115
  // },
  // {
  //   title: 'App Config',
  //   routerLink: 'appConfig',
  //   icon: 'fa fa-sun-o',
  //   selected: false,
  //   expanded: false,
  //   order: 115
  // },
  {
    title: 'About',
    routerLink: 'termsAndConditions',
    icon: 'fa fa-info-circle',
    selected: false,
    expanded: false,
    order: 116,
    subMenu: [
      {
        title: 'Terms&Conditions',
        routerLink: 'termsAndConditions',
      },
      {
        title: 'Privacy Policy',
        routerLink: 'policy',
      },
      {
        title: 'Guidelines',
        routerLink: 'communityGuidelines',
      },
      {
        title: 'SafetyTips',
        routerLink: 'safetyTips',
      },
      {
        title: 'Licenses',
        routerLink: 'licenses',
      },
    ]
  },


  {
    title: 'App Configuration',
    routerLink: 'appConfig',
    icon: 'fa fa-sun-o',
    selected: false,
    expanded: false,
    order: 116,
    subMenu: [
      {
        title: 'User Preferences',
        routerLink: 'appConfig',
      },
      {
        title: 'Search Preferences',
        routerLink: 'preferences',
      },
      {
        title: 'Report Reasons',
        routerLink: 'MatchesComponent',
      },
      {
        title: 'App-Version',
        routerLink: 'appversion',
      },
      // {
      //   title: 'Languages',
      //   routerLink: 'languageComponent',
      // },
      {
        title: 'CDN-Key',
        routerLink: 'cdnKey',
      },
    ]
  },

];